package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class BookingAccept {

    @SerializedName("booking_id")
    var BookingID : Long? = null

    @SerializedName("user_id")
    var UserID : Long? = null

    @SerializedName("status")
    var Status : Boolean? = null

    @SerializedName("message")
    var ResponseMessage : String? = null
}