package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class FcmUser{

    @SerializedName("name")
    var username: String? = ""

    @SerializedName("registration_id")
    var FcmKey: String = ""

    @SerializedName("user")
    var userID: Long = 0

    @SerializedName("active")
    var active: Boolean = false

    override fun toString(): String {
        return "FcmUser(username=$username, FcmKey='$FcmKey', userID=$userID, active=$active)"
    }

}
