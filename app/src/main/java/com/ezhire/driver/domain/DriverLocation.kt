package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class DriverLocation {

    @SerializedName("user_id")
    var UserID : Long? = null

    @SerializedName("lat")
    var Latitude : Double? = null

    @SerializedName("lng")
    var Longitude : Double? = null

    @SerializedName("latlng")
    var Latlng : String? = null


}