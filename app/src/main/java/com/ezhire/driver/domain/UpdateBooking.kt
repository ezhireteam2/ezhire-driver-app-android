package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class UpdateBooking {

    var booking_id: Long? = null
    var deliver_date_string: String? = null
    var deliver_time_string: String? = null
    var in_odometer: String? = "0"
    var in_fuel_level: String? = "0"

//    @SerializedName("booking_driver_status")
//    var bookingDriverStatus : BookingDriverStatus? = null

}