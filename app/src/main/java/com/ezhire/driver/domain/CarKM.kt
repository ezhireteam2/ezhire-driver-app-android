package com.ezhire.driver.domain

class CarKM {

    var driver_id: Long? = null
    var booking_id: Long? = null
    var shift_start_km: Double? = null
    var shift_end_km: Double? = null
    var status : Boolean? = null
}