package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class CallResponse {

    @SerializedName("id")
    var ID:String = ""

    @SerializedName("status")
    var Status:String = ""

    @SerializedName("card_token")
    var CardToken:String = ""

    @SerializedName("3d_url")
    var URL:String = ""

    @SerializedName("message")
    var Message : String = ""

    var isDocumentAvailable: Boolean = false

    var isPaymentCardAdded : Int = 0

    var isAdditionalInfoAvailable : Int = 0

    var amount : Double? = null

    override fun toString(): String {
        return "CallResponse(ID='$ID', Status='$Status', CardToken='$CardToken', URL='$URL', isDocumentAvailable=$isDocumentAvailable, isPaymentCardAdded=$isPaymentCardAdded)"
    }

}