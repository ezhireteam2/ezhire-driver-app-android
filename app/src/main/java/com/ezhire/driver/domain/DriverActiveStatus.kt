package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class DriverActiveStatus{

    @SerializedName("user_id")
    var UserID : Long? = null

    @SerializedName("active")
    var Active : Int? = null

}
