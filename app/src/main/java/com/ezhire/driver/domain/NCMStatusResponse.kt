package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class NCMStatusResponse {

    val status : Boolean = false
    val message : String? = null

    @SerializedName("data")
    val bookingDriverStatus : ArrayList<BookingDriverStatus>? = null

}