package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName


class BookingDriverStatus {

    @SerializedName("booking_id")
    var bookingID : Long? = null

    @SerializedName("driver_id")
    var DriverID : Long? = null

    @SerializedName("vendor_id")
    var VendorID : Int? = null

    @SerializedName("car_id")
    var CarID : Int? = null

    @SerializedName("booking_status")
    var BookingStatus : Int? = null

    @SerializedName("car_status")
    var CarStatus : Int? = null

    @SerializedName("driver_status")
    var DriverStatus : Int? = null

    @SerializedName("odometer")
    var Odomoter : Long? = null

    @SerializedName("lat")
    var Latitude : String = ""

    @SerializedName("lng")
    var Longitude : String = ""

    @SerializedName("lat_lng")
    var LatLng : String = ""

    @SerializedName("action_date")
    var Action_Date_String : String? = null

    @SerializedName("action_time")
    var Action_Time_String : String? = null

    @SerializedName("source")
    val Source = "ANDROID"

    @SerializedName("fuel_level")
    var FuelLevel : String? = null

    @SerializedName("plate_number")
    var CarPlateNumber : String? = null

    @SerializedName("is_ncm")
    var IsNCM : Boolean = false


}