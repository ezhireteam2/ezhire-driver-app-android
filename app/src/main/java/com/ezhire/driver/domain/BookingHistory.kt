package com.ezhire.driver.domain

import java.io.Serializable

class BookingHistory : Serializable{

    var return_time_string : String = ""
    var status : Int = 0
    var deliver_date : String = ""
    var currency : String = ""
    var amount : Double = 0.0
    var deliver_time_string : String = ""
    var return_date : String = ""
    var id : Long = 0

    var driver_id : Long = 0
    var rental_type_id : Int = 0
}
