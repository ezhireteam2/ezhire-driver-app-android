package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class BookingStatusCode{

    @SerializedName("booking_id")
    var BookingID : Long? = null

    @SerializedName("status")
    var StatusCode : Int? = null

    @SerializedName("booking_driver_status")
    var driverStatus : BookingDriverStatus? = null

    var amount : Double? = null

    var date : String? = ""

    var source : String? = "ANDROID"

    var comments : String? = ""

    var booking_amount: Double = 0.0

}
