package com.ezhire.driver.domain

import android.os.Parcelable
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable


class Booking : Serializable {

    @SerializedName("id")
    var BookingID:Long = 0

    @SerializedName("airport_id")
    var AirportID : Int = 0

    @SerializedName("airport_return_id")
    var AirportReturnID : Int = 0

    @SerializedName("delivery_location")
    var DeliveryLocationAddress:String = ""

    @SerializedName("deliver_date")
    var DeliveryDate:String = ""

    @SerializedName("from_date")
    var FromDate :String = ""

    @SerializedName("return_date")
    var ReturnDate:String = ""

    @SerializedName("to_date")
    var ToDate :String = ""

    @SerializedName("deliver_time")
    var DeliveryTime:String = ""

    @SerializedName("return_time")
    var ReturnTime:String = ""

    @SerializedName("deliver_date_string")
    var DeliveryDateString:String = ""

    @SerializedName("deliver_time_string")
    var DeliveryTimeString:String = ""

    @SerializedName("deliver_at")
    var DeliveryLatLong:String = ""

    @SerializedName("delivery_location_lat")
    var DeliveryLat: Double = 0.0

    @SerializedName("delivery_location_lng")
    var DeliveryLong:Double = 0.0

    @SerializedName("collection_location")
    var ReturnLocationAddress:String? = ""

    @SerializedName("return_date_string")
    var ReturnDateString:String? = ""

    @SerializedName("return_time_string")
    var ReturnTimeString:String? = ""

    @SerializedName("return_location_lat")
    var ReturnLat:Double = 0.0

    @SerializedName("return_location_lng")
    var ReturnLong:Double = 0.0

    @SerializedName("return_at")
    var ReturnLatLong:String = ""

    @SerializedName("city_id")
    var CityID : Int = 0

    @SerializedName("return_city_id")
    var ReturnCityID : Int = 0

    @SerializedName("owner_id")
    var UserID : Long = 0

    @SerializedName("vendor_id")
    var VendorID : Int = 0

    @SerializedName("status")
    var Status : Int = 0

    @SerializedName("car_available_id")
    var CarAvailableID : Int = 0

    @SerializedName("car_id")
    var AssignedCarID : Int = 0

    @SerializedName("cat_id")
    var CategoryID : Int = 0

    @SerializedName("days")
    var NoOfDays: Double = 0.0

    @SerializedName("self_pickup_id")
    var SelfPickupID : Int = 0

    @SerializedName("self_pickup_status")
    var SelfPickupStatus : Int = 0

    @SerializedName("self_return_id")
    var SelfReturnID : Int = 0

    @SerializedName("self_return_status")
    var SelfReturnStatus : Int = 0

    @SerializedName("amount")
    var TotalAmount : Double = 0.0

    @SerializedName("daily_rate")
    var DailyRate : Double = 0.0

    @SerializedName("deliver_rate")
    var DeliverRate : Double = 0.0

    @SerializedName("collection_charges")
    var CollectionCharges : Double = 0.0

    @SerializedName("airport_charges")
    var AirportCharges : Double = 0.0

    @SerializedName("vat_rate")
    var VATRate : Double = 0.0

    @SerializedName("insurance")
    var InsuranceAmount : Double = 0.0

    @SerializedName("pai")
    var PAI : Double = 0.0

    @SerializedName("gps")
    var gps : Double = 0.0

    @SerializedName("babe_seater")
    var babe_seater : Double = 0.0

    @SerializedName("boster_seat")
    var boster_seat : Double = 0.0

    @SerializedName("extra_driver")
    var extra_driver : Double = 0.0

    @SerializedName("dr")
    var DR : Double = 0.0

    @SerializedName("wr")
    var WR : Double = 0.0

    @SerializedName("mr")
    var MR : Double = 0.0

    @SerializedName("drg")
    var DRG : Double = 0.0

    @SerializedName("wrg")
    var WRG : Double = 0.0

    @SerializedName("mrg")
    var MRG : Double = 0.0

    @SerializedName("car_name")
    var CarName : String = ""

    @SerializedName("car_image")
    var CarImage : String = ""

    @SerializedName("car_color")
    var CarColor : String = ""

    @SerializedName("car_number_plate")
    var CarNumberPlate : String = ""

    @SerializedName("DIR")
    var DIR : Double = 0.0

    @SerializedName("WIR")
    var WIR : Double = 0.0

    @SerializedName("MIR")
    var MIR : Double = 0.0

    @SerializedName("driver_name")
    var DriverName : String = ""

    @SerializedName("driver_number")
    var DriverNumber : String = ""

    @SerializedName("driver_image")
    var DriverImage : String = ""

    @SerializedName("rental_end_feedback")
    var RentalEndFeedback : Int = 0

    @SerializedName("delivery_feedback_time")
    var EstimatedTimeOfArrival : String = ""

    @SerializedName("Delivery_feedback")
    var DeliveryFeedBack : Int = 1

    @SerializedName("extended_amount")
    var ExtendedAmountTotal : Double = 0.0

    @SerializedName("rent_extended_amount")
    var RentExtendedAmount : Double = 0.0

    @SerializedName("gps_extended_amount")
    var GPSExtendedAmount : Double = 0.0

    @SerializedName("babyseat_extended_amount")
    var BabySeatExtendedAmount : Double = 0.0

    @SerializedName("bosterseat_extended_amount")
    var BosterSeatExtendedAmount : Double = 0.0

    @SerializedName("driver_extended_amount")
    var DriverExtendedAmount : Double = 0.0

    @SerializedName("insurance_extended_amount")
    var InsuranceExtendedAmount : Double = 0.0

    @SerializedName("pai_extended_amount")
    var PAIExtendedAmount : Double = 0.0

    @SerializedName("discount")
    var DiscountAmount : Double = 0.0

    @SerializedName("extended_discount")
    var ExtendedDiscount : Double = 0.0

    @SerializedName("extended_days")
    var ExtendedDays : Int = 0

    @SerializedName("payment_cc")
    var PaymentCC : Double = 0.0

    @SerializedName("confirmed_attribution")
    var ConfirmedAttribution : Int = 0

    @SerializedName("agreement_number")
    var AgreementNumber : String = ""

    var self_pickup_timings : String = ""
    var self_return_timings : String = ""
    var self_pickup_vendor_contact : String? = ""
    var self_return_vendor_contact : String = ""

    @SerializedName("browser_name")
    var OperationSystem : String = "ANDROID"

    @SerializedName("Source")
    var Source : String = "ANDROID"

    @SerializedName("os_version")
    var OSVersion : String? = ""

    @SerializedName("app_version")
    var AppVersion : String? = ""

    @SerializedName("PROMO_CODE_ID")
    var PromoCodeID : Int = 0

    @SerializedName("Promo_Code")
    var PromoCode : String? = ""

    @SerializedName("deposit_card_id")
    var DepositCardID : Long = 0

    @SerializedName("total_charge")
    var TotalCharge : Double = 0.0

    @SerializedName("wallet_debit_amount")
    var WalletDebitAmount : Double = 0.0

    var insurance_active : Int = 0
    var pai_active : Int = 0
    var add_driver_active : Int = 0
    var gps_active : Int = 0
    var baby_seat_active : Int = 0
    var boster_seat_active : Int = 0

    var fort_id: String = ""

    var is_checkout = 0

    var charge_customer_id = 0

    var collection_reminder = 0

    var user_comments: String = ""

    var extra_field1 : String = "0"

    @SerializedName("email_outstanding")
    var EmailOutStandingAmount = 0.0

    @SerializedName("millage_id")
    var MillageID : Long = 0

    @SerializedName("contract_id")
    var ContractID : Long = 0

    @SerializedName("rental_type_id")
    var RentalTypeID : Int = 0

    @SerializedName("payment_type_id")
    var PaymentTypeID : Int = 0

    @SerializedName("monthly_optimization_enabled")
    var OptimizationEnabled : Int = 0

    @SerializedName("booking_hour")
    var BookingHours : Int = 0

    @SerializedName("hrg")
    var HourlyRate : Double = 0.0

    @SerializedName("overtime_rate")
    var OverTimeRate : Double = 0.0

    var driver_customer_feedback : Int = 0

    @SerializedName("currency")
    var chargeType = ChargeType()

    @SerializedName("charges")
    var chargesList: ArrayList<AddOns>? = ArrayList()

    var IS_COLLECTION = false

    fun clone(): Booking {
        val stringProject = Gson().toJson(this, Booking::class.java)
        return Gson().fromJson<Booking>(stringProject, Booking::class.java)
    }


}

class AddOns(

    var isSelected: Boolean = false,
    var AddonIcon: String = "",
    var showSwitchView: Boolean = false,
    var carDays: Boolean = false

)  : Serializable{

    @SerializedName("id")
    var ID: Int = 0

    @SerializedName("type_name")
    var AddonTitle: String = ""

    @SerializedName("desc")
    var AddonDesc: String = ""

    @SerializedName("dr")
    var DailyRate: Double = 0.0

    @SerializedName("wr")
    var WeeklyRate: Double = 0.0

    @SerializedName("mr")
    var MonthlyRate: Double = 0.0

    @SerializedName("frequency")
    var Frequency: Double = 0.0

    var AddOnRate: Double = 0.0

    @SerializedName("total_charge")
    var TotalCharge: Double = 0.0

    var TotalChargeOld: Double = 0.0

    @SerializedName("charge_type_id")
    var ChargeTypeID: Int = 0

    @SerializedName("comments")
    var Comments :String = ""

    @SerializedName("on_type")
    var OnType : Int = 0

    @SerializedName("on_amount")
    var OnAmount: Double = 0.0

    @SerializedName("charge")
    var Charge: Double = 0.0

    @SerializedName("from_date")
    var FromDate : String = ""

    @SerializedName("to_date")
    var ToDate : String = ""

    @SerializedName("order1")
    var order1 : Int = 1

    @SerializedName("is_paid")
    var IsPaid : Int = 0

    @SerializedName("refund_to")
    var RefundTo : String = ""

    var ExtendedAmount :Double = 0.0

    @SerializedName("Source")
    var Source : String = "ANDROID"

    @SerializedName("active")
    var active : Int = 1

    @SerializedName("days_extended")
    var DaysExtended: Int = 0

    var showAddonInSummary = false

    var currency: String = ""


    fun clone(): AddOns {
        val stringProject = Gson().toJson(this, AddOns::class.java)
        return Gson().fromJson<AddOns>(stringProject, AddOns::class.java)
    }

    override fun toString(): String {
        return "AddOns(isSelected=$isSelected, AddonIcon='$AddonIcon', showSwitchView=$showSwitchView, carDays=$carDays, ID=$ID, AddonTitle='$AddonTitle', AddonDesc='$AddonDesc', DailyRate=$DailyRate, WeeklyRate=$WeeklyRate, MonthlyRate=$MonthlyRate, Frequency=$Frequency, AddOnRate=$AddOnRate, TotalCharge=$TotalCharge, TotalChargeOld=$TotalChargeOld, ChargeTypeID=$ChargeTypeID, Comments='$Comments', OnType=$OnType, OnAmount=$OnAmount, Charge=$Charge, FromDate='$FromDate', ToDate='$ToDate', order1=$order1, IsPaid=$IsPaid, RefundTo='$RefundTo', ExtendedAmount=$ExtendedAmount, Source='$Source', active=$active, DaysExtended=$DaysExtended, showAddonInSummary=$showAddonInSummary, currency='$currency')"
    }


}

class ChargeType : Serializable {

    @SerializedName("id")
    var ID: Int = 0

    @SerializedName("city_id")
    var CityID: Int = 0

    @SerializedName("country_id")
    var CountryID: Int = 0

    @SerializedName("damount")
    var DeliveryAmount: Double = 0.0

    @SerializedName("camount")
    var CollectionAmount: Double = 0.0

    @SerializedName("curr")
    var Currency: String = ""

    @SerializedName("tax")
    var TaxRate: Int = 0

    @SerializedName("payment_currency")
    var PaymentCurrency: String? = ""

    var rental_type_id: Int =  0

    @SerializedName("tax_label")
    var TaxLabel: String = ""

    var operational_hour: Int =  10

    @SerializedName("airport_charges")
    var AirportCharge : Double = 0.0

    @SerializedName("terms_condition")
    var TermsAndConditions: String = ""

    @SerializedName("insurance_info")
    var InsuranceInfo: String = "Comprehensive insurance is included in the rental, in case of an accident, if you provide us with Green Police Report there won't be any charges, however if the accident is caused by your mistake and you get a red police report then there will be excess insurance charges, which depends upon the type of the car and the extend of the damage, usually it ranges from AED 200 to AED 1500. Select Full Insurance (Optional) if you want the full coverage. PAI(Optional) is Personal Accident Insurance that insures drivers of a rental car and their passengers for accidental medical costs."


    override fun toString(): String {
        return "ChargeType(ID=$ID, CityID=$CityID, CountryID=$CountryID, DeliveryAmount=$DeliveryAmount, CollectionAmount=$CollectionAmount, Currency='$Currency', TaxRate=$TaxRate, PaymentCurrency=$PaymentCurrency)"
    }


}