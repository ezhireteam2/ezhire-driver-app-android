package com.ezhire.driver.domain

class Document{

    var doc_id : Int = 0
    var document_id : Int = 0
    var title: String = ""
    var documents: String = ""
    var user_id : String = ""
    var isDocumentAvailable : Boolean  = false
    var isUploaded : Boolean  = false
    var documentPath : String = ""
    var is_required : Boolean  = false
    var update_fuser : Int = 1

    override fun toString(): String {
        return "Document(doc_id=$doc_id, name='$title', documents='$documents', user_id='$user_id', isDocumentAvailable=$isDocumentAvailable, isUploaded=$isUploaded, documentPath='$documentPath')"
    }


}