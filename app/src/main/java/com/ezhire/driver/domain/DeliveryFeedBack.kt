package com.ezhire.driver.domain

class DeliveryFeedBack{

    var del_col_ontime: Int = 0
    var good_condition: Int = 0
    var delivery_date_string: String = ""
    var delivery_time_string: String = ""
    var rate: Int = 0
    var comments: String = ""
    var feedback_type: Int = 0
    var Booking_id: Long = 0
    var user_id : Long = 0
}