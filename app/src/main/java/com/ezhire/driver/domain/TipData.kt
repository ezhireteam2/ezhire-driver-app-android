package com.ezhire.driver.domain

class TipData {

    var MonthYear : String? = null
    var data : ArrayList<Tips>? = null

}


class  Tips{

    var currency : String? = null
    var booking_id : Long? = null
    var amount : Double? = null
    var driver_id : Long? = null
    var date_string : String? = null
}