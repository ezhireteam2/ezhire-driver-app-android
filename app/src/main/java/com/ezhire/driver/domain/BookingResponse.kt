package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class BookingResponse {

    val status : Boolean = false
    val message : String? = null

    @SerializedName("data")
    val bookingList : ArrayList<BookingList>? = null

    @SerializedName("driver_status")
    val driverStatus : BookingDriverStatus? = null
}