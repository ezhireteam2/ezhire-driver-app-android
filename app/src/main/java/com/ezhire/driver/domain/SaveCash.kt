package com.ezhire.driver.domain

class SaveCash {

    var booking_id : Long? = null
    var amount : Double? = null
    var booking_amount : Double? = null
    var date : String? = null
    var source : String? = "ANDROID"
    var comments : String? = "cash collected by driver"
}