package com.ezhire.driver.domain

class ConnectionModel(val type: Int, val isConnected: Boolean)