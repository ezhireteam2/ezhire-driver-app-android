package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("id")
    var ID: Long = 0

    @SerializedName("user_id")
    var UserId: Long = 0

    @SerializedName("username")
    var UserName: String = ""

    @SerializedName("password")
    var Password: String = ""

    @SerializedName("role_type")
    var RoleType: Int = 0

    @SerializedName("extra_field1")
    var ExtraField1: String? = ""

    @SerializedName("extra_field2")
    var ExtraField2: String? = ""

    @SerializedName("first_name")
    var FirstName: String = ""

    @SerializedName("last_name")
    var LastName: String? = ""

    @SerializedName("mobile")
    var MobileNo: String? = ""

    @SerializedName("country_code")
    var CountryCode:String? = ""

    @SerializedName("country")
    var Country:String? = ""

    @SerializedName("email")
    var Email:String =""

    @SerializedName("code_number")
    var VerificationCode:String =""

    @SerializedName("nationality")
    var Nationality:String? =""

    @SerializedName("renting_in")
    var RentingIn:Int = 0

    var RentingInCountryCode:String = ""

    @SerializedName("is_resident")
    var ResidenceOf:String? =""

    @SerializedName("dl_country")
    var DrivingLicenseIssueFrom:String? =""

    @SerializedName("dl_issue_date")
    var DrivingLicenseIssueDate:String? =""

    @SerializedName("date_of_birth")
    var DateOfBirth:String? = ""

    @SerializedName("fcm_key")
    var FcmKey: String? = ""

    @SerializedName("telephone")
    var Telephone : String? = ""

    @SerializedName("user_source1")
    var Source : String? = "Android"

    @SerializedName("os_version")
    var OSVersion : String? = ""

    @SerializedName("app_version")
    var AppVersion : String? = ""

    @SerializedName("firebase_token")
    var AuthToken : String? = ""

    @SerializedName("currency")
    var Currency: String? = ""

    @SerializedName("payment_currency")
    var PaymentCurrency: String? = ""

    override fun toString(): String {
        return "User(ID=$ID, UserId=$UserId, UserName='$UserName', Password='$Password', RoleType=$RoleType, ExtraField1=$ExtraField1, ExtraField2=$ExtraField2, FirstName=$FirstName, LastName=$LastName, MobileNo=$MobileNo, CountryCode=$CountryCode, Country=$Country, Email='$Email', VerificationCode='$VerificationCode', Nationality=$Nationality, RentingIn=$RentingIn, ResidenceOf=$ResidenceOf, DrivingLicenseIssueFrom=$DrivingLicenseIssueFrom, DrivingLicenseIssueDate=$DrivingLicenseIssueDate, DateOfBirth=$DateOfBirth, FcmKey=$FcmKey, Telephone=$Telephone, Source=$Source, OSVersion=$OSVersion, AppVersion=$AppVersion, AuthToken=$AuthToken, Currency=$Currency, PaymentCurrency=$PaymentCurrency)"
    }


}