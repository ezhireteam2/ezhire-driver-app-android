package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName

class AppVersion {

    @SerializedName("version")
    var AppVersion:String = ""
}
