package com.ezhire.driver.domain

class ShiftInfo {

    var booking_id : Long? = null
        var driver_id: Long? = null
        var shift_time: String? = null
    var shift_date: String? = null
    var fuel_level: String? = null
    var car_odometer: String? = null
    var shift_type: Int? = null
    var cash_collected : Double? = null
    var status : Boolean? = null

    var shift_start_time : String? = null
    var shift_end_time : String? = null

    var start_car_odometer : String? = null
    var end_car_odometer : String? = null

    var extra_hrs : Int? = null
    var total_hrs : Int? = null

    var amount : Double? = null


}