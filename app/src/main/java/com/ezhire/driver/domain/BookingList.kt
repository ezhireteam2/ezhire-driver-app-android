package com.ezhire.driver.domain

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class BookingList : Serializable {

    @SerializedName("booking_status")
    var Status: Int = 0

    @SerializedName("booking_id")
    var BookingID: Long = 0

    @SerializedName("driver_id")
    var DriverID: Long = 0

    @SerializedName("rental_type_id")
    var RentalTypeID : Int = 0

    @SerializedName("payment_type_id")
    var PaymentTypeID : Int = 0

    @SerializedName("delivery_location")
    var DeliveryLocationAddress:String = ""

    @SerializedName("collection_location")
    var ReturnLocationAddress:String? = ""

    @SerializedName("deliver_date_string")
    var DeliveryDateString: String = ""

    @SerializedName("deliver_time_string")
    var DeliveryTimeString: String = ""

    @SerializedName("return_date_string")
    var ReturnDateString: String? = ""

    @SerializedName("return_time_string")
    var ReturnTimeString: String? = ""


}