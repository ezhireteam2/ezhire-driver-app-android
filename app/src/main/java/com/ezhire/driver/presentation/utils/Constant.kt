package com.ezhire.driver.presentation.utils


object Constant {

    const val PREFERENCES_FILE_NAME = "Ezhire"
    const val DATABASE = "Ezhire"
    const val NETWORK_ERROR = "Service field \n Try Again !"
    const val NO_INTERNET = "No Network Connectivity"

    const val SUCCESS = "1"
    const val FAILURE = "0"
    const val INFO = "2"


    const val BOOKING_ENDED = 9
    const val NEW_BOOKING = 1
    const val VENDOR_ASSIGNED = 2
    const val BOOKING_CONFIRMED = 3
    const val ON_THE_WAY = 10
    const val DRIVER_ARIVED = 11
    const val RENTAL_STARTED = 31
    const val RENTAL_END = 9
    const val COLLECT = 4
    const val DRIVER_ASSIGNED_COLLECTION = 17
    const val ON_THE_WAY_COLLECTION = 18
    const val DRIVER_ARRIVED_COLLECTION = 19
    const val CAR_COLLECTED = 20
    const val ON_THE_WAY_BACK = 13
    const val ARRIVED_DISPATCH_CENTER = 14
    const val DRIVER_AVAILABLE = 16
    const val ON_THE_WAY_BACK_COLLECTION = 21

    const val NCM_OUT = 22
    const val NCM_IN = 23


    const val DRIVER_ACTIVE = 1
    const val DRIVER_INACTIVE = 0

    const val PAYMENT_CC = 6
    const val COLLECTION_CHARGE = 3
    const val RENT = 4
    const val DELIVERY_CHARGE = 11
    const val DISCOUNT = 14
    const val FULL_INSURANCE = 15
    const val BABY_SEATER = 16
    const val GPS = 17
    const val BOSTER_SEAT = 18
    const val PAI = 19
    const val VAT = 20
    const val ADD_DRIVER = 21
    const val EXCEED_TIME_CHARGE = 23
    const val WALLET_PAYMENT = 24
    const val INTER_CITY_CHARGE = 25
    const val EXTRA_HOUR = 27
    const val AIRPORT_CHARGE = 29
    const val TOTAL_CHARGE = -1

    const val SELF_DRIVE = 1
    const val WITH_DRIVE = 2
    const val KM_DRIVE = 3



}