package com.ezhire.driver.presentation.main.ui.history

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.databinding.FragmentBookingHistoryBinding
import com.ezhire.driver.domain.BookingHistory
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.main.ui.tips.TipsViewModel
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import org.koin.android.ext.android.inject


class BookingHistoryFragment : BaseFragment()  {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
        const val LOCATION_PERMISSION_RC = 501
        const val DEFAULT_ZOOM  = 15.0f
        var API_CALL = true

    }

    private val viewModel by inject<BookingHistoryViewModel>()
    private lateinit var binding: FragmentBookingHistoryBinding

    private lateinit var bookingHistoryList : ArrayList<BookingHistory>

    override fun onDestroy() {
        super.onDestroy()
        //Locus.stopLocationUpdates()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentBookingHistoryBinding.inflate(layoutInflater)


        binding.bookingHistoryRV.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false)
        binding.bookingHistoryRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


       binding.navMenu.setOnClickListener {
            this.requireActivity().onBackPressed()
       }

        getBookingHistory()


        return binding.root
    }


    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun getBookingHistory(){

        viewModel.getBookingHistory(AppData.getUserID()).observe(this.requireActivity(),{
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()

                    Handler(Looper.getMainLooper()).postDelayed({
                        hideProgressIndicator()
                    }, 1500)

                    val data = it.data as ArrayList<BookingHistory>

                    bookingHistoryList = data as ArrayList<BookingHistory>

                    if(bookingHistoryList.size > 0){

                        binding.bookingHistoryRV.visibility = View.VISIBLE
                        binding.noRecordFoundTV.visibility = View.INVISIBLE
                        binding.bookingHistoryRV.adapter = BookingHistoryAdapter(this.requireContext(),bookingHistoryList,itemClicked)

                    }else{
                        binding.bookingHistoryRV.visibility = View.INVISIBLE
                        binding.noRecordFoundTV.visibility = View.VISIBLE
                    }


                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        })
    }

    internal var itemClicked: IItemClicked =
        object : IItemClicked {
            override fun itemClicked(postion: Int) {

                val intent = Intent(context, BookingHistoryDetailAC::class.java)
                intent.putExtra("BOOKING",bookingHistoryList!![postion])
                startActivity(intent)

            }
        }



}




