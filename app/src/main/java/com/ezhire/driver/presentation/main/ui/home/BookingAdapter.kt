package com.ezhire.driver.presentation.main.ui.home

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ezhire.driver.R
import com.ezhire.driver.domain.BookingHistory
import com.ezhire.driver.domain.BookingList
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import java.util.*


class BookingAdapter(
    private val context: Context,
    itemList: ArrayList<BookingList>,
    private val itemClicked: IItemClicked
) : androidx.recyclerview.widget.RecyclerView.Adapter<BookingAdapter.ViewHolder>() {

    private var itemList = ArrayList<BookingList>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_booking, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BookingAdapter.ViewHolder, position: Int) {

        val booking = itemList[position]

        holder.locationTitleTV.text = "Booking # "+booking.BookingID
        holder.locationAddressTV.text = "Delivery at: ${booking.DeliveryLocationAddress}"
        holder.distanceTV.text = booking.DeliveryDateString + "  " + Helper.formatTime24HrToAmPm(booking.DeliveryTimeString)



        val statusArray = arrayOf(Constant.DRIVER_ASSIGNED_COLLECTION,Constant.CAR_COLLECTED,Constant.COLLECT,Constant.DRIVER_ARRIVED_COLLECTION,Constant.ON_THE_WAY_COLLECTION)
        val collectionBooking = statusArray.any { it == booking.Status }

        if(collectionBooking){
            holder.locationAddressTV.text = "Collection from: ${booking.ReturnLocationAddress}"
            holder.distanceTV.text = booking.ReturnDateString + "  " + Helper.formatTime24HrToAmPm(booking.ReturnTimeString)
        }else{
            holder.locationTitleTV.setTextColor(context.getColor(R.color.green_shade))
        }

        holder.itemView.setOnClickListener{

            itemClicked.itemClicked(position)


        }

    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var locationTitleTV: TextView
        var locationAddressTV: TextView
        var distanceTV: TextView

        init {
            locationTitleTV = itemView.findViewById(R.id.locationTitleTV)
            locationAddressTV = itemView.findViewById(R.id.locationAddressTV)
            distanceTV = itemView.findViewById(R.id.distanceTV)
        }
    }
}
