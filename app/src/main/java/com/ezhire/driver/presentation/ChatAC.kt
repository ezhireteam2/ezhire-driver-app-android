package com.ezhire.driver.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ezhire.driver.R
import com.ezhire.driver.presentation.base.BaseActivity
import com.google.firebase.analytics.FirebaseAnalytics
import kotlinx.android.synthetic.main.activity_chat.*

class ChatAC : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)



        closeChat.setOnClickListener {
            finish()
        }
    }



    override fun onDestroy() {
        //chatfragment = null

        super.onDestroy()
        System.gc()
    }
}
