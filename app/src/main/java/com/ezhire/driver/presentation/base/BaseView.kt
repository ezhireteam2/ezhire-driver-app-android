package com.ezhire.driver.presentation.base

import android.os.Bundle
import androidx.annotation.IdRes

interface BaseView {

    fun navigateToFragment(@IdRes id: Int, args: Bundle? = null)
    fun isInternetAvailable() : Boolean
    fun hideKeyboard()
    fun callDialog(type: String)
    fun openDrawer()


}