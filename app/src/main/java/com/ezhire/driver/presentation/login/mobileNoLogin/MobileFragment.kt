package com.ezhire.driver.presentation.login.mobileNoLogin

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ezhire.driver.R
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.data.Response
import com.ezhire.driver.databinding.MobileFragmentBinding
import com.ezhire.driver.domain.FcmUser
import com.ezhire.driver.domain.User
import com.ezhire.driver.presentation.main.MainActivity
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.Helper.showBasicAlert
import kotlinx.android.synthetic.main.mobile_fragment.*
import org.koin.android.ext.android.inject
import java.util.*
import java.util.concurrent.TimeUnit

class MobileFragment : BaseFragment() {


    private val viewModel by inject<MobileViewModel>()
    private lateinit var binding: MobileFragmentBinding
    private var seconds: Int = 60
    lateinit var storedVerificationId: String
    lateinit  var phoneNumber: String
    lateinit  var token : String
    lateinit var resendToken: PhoneAuthProvider.ForceResendingToken
    var handler1 = Handler()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MobileFragmentBinding.inflate(inflater)


        binding.fab.setOnClickListener(::onClickEvent)

        binding.timeTV.setOnClickListener {
            seconds = 60
            generateOTP()
        }

        binding.backBtnIV.setOnClickListener {
            seconds = 60
            binding.mobileNoCodeVerificationLayout.visibility = View.GONE
            binding.mobileNoLayout.visibility = View.VISIBLE
            hideProgress()
        }

        textChangeListner()


        return  binding.root

    }


    private fun textChangeListner(){
        binding.mobileNoET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (p0!!.startsWith("0")) {
                    binding.mobileNoET.setText(
                        binding.mobileNoET.text.toString().replaceFirst(
                            "0",
                            ""
                        )
                    )
                    binding.mobileNoET.setSelection(binding.mobileNoET.text.toString().length)
                }

                if (p0.toString().isNotEmpty()) {
                    binding.progressMain.visibility = View.VISIBLE
                } else {
                    binding.progressMain.visibility = View.GONE
                }
            }
        })


        binding.pinET.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable?) {

                if (s.toString().length == 0){
                    binding.errorTV.visibility = View.GONE
                }
                if (s.toString().length == 6) {
                    verifyMobileNumber()
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })



    }

    private fun onClickEvent(view: View) {
        showProgress()
        phoneNumber = binding.ccp.selectedCountryCodeWithPlus +binding.mobileNoET.text.toString().trim()

        val options = PhoneAuthOptions.newBuilder()
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(seconds.toLong(), TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity())                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }



    fun generateOTP(){
        binding.errorTV.visibility = View.GONE
        val options = PhoneAuthOptions.newBuilder()
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(seconds.toLong(), TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity())                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }


    val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onVerificationCompleted(credential: PhoneAuthCredential) {
            // This callback will be invoked in two situations:
            // 1 - Instant verification. In some cases the phone number can be instantly
            //     verified without needing to send or enter a verification code.
            // 2 - Auto-retrieval. On some devices Google Play services can automatically
            //     detect the incoming verification SMS and perform verification without
            //     user action.

            signInWithPhoneAuthCredential(credential)
        }

        override fun onVerificationFailed(e: FirebaseException) {
            hideProgress()
            showBasicAlert(
                requireContext(),
                getString(R.string.alert),
                e.localizedMessage,
                Constant.FAILURE, null
            )
        }

        override fun onCodeSent(
            verificationId: String,
            token: PhoneAuthProvider.ForceResendingToken
        ) {
            // The SMS verification code has been sent to the provided phone number, we
            // now need to ask the user to enter the code and then construct a credential
            // by combining the code with a verification ID.

            // Save verification ID and resending token so we can use them later
            storedVerificationId = verificationId
            //resendToken = token
            resendToken = token
            binding.mobileNoTV.text = phoneNumber
            binding.mobileNoCodeVerificationLayout.visibility  = View.VISIBLE
            binding.pinET.requestFocus()
            binding.progressMain.visibility  = View.GONE
            binding.mobileNoLayout.visibility  = View.GONE
            startTimer()


       }
    }

    private fun startTimer() {


        val timer1 = Timer(true)
        val timerTask1 = object : TimerTask() {
            override fun run() {
                handler1.post {
                    //Log.e("timer1", "after 1 second")
                    seconds = seconds - 1


                    if (seconds == 0) {
                        timeTV?.text = "RESEND CODE"
                        timeTV?.clearAnimation()
                        timer1.cancel()
                        seconds = 60
                        handler1.removeCallbacksAndMessages(null)
                    } else {
                        timeTV?.text = "$seconds seconds left for new code request"
                    }

                }
            }
        }
        timer1.schedule(timerTask1, 0, 1000) // 1000 = 1 second

    }


    fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        showProgressIndicator()
        FirebaseAuth.getInstance().signInWithCredential(credential)
            .addOnCompleteListener(
                requireActivity()
            ) { task ->
                if (task.isSuccessful) {
                    getToken()
                } else {
                    hideProgressIndicator()
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        binding.errorTV.visibility = View.VISIBLE
                        binding.errorTV.text = "Invalid code entered."
                    }

                }
            }
    }





    fun verifyMobileNumber() {
        try{
            val code = binding.pinET.text.toString()
            val credential = PhoneAuthProvider.getCredential(storedVerificationId, code)
            hideKeyboard()
            signInWithPhoneAuthCredential(credential)
        }catch (e: Exception){
            Log.e("Ex", e.toString())
        }

    }


    private fun getToken() {

        val mUser: FirebaseUser? = FirebaseAuth.getInstance().currentUser
        mUser!!.getIdToken(true)
            .addOnCompleteListener { p0 ->
                if (p0.isSuccessful) {
                    token = p0.result?.token.toString()
                    checkUserExist(token)
                }

            }
    }

    private fun checkUserExist(token: String) {
        viewModel.userExist(token).observe(viewLifecycleOwner, {
            when (it) {
                is Response.Loading -> ""
                is Response.Success<*> -> {

                    val response = it.data as List<User>

                    val runnable = Runnable {
                        binding.pinET.setText("")
                        binding.mobileNoLayout.visibility = View.VISIBLE
                        binding.mobileNoCodeVerificationLayout.visibility = View.GONE
                    }

                    if (response.isNullOrEmpty()) {
                        hideProgressIndicator()
                        showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            "Your are not registered to any vendor please contact to vendor",
                            Constant.FAILURE,runnable
                        )
                    } else {


                        if(response[0].RoleType == 4){
                            setUserInPref(response[0])

                        }else{
                            hideProgressIndicator()
                            showBasicAlert(
                                this.requireContext(),
                                getString(R.string.alert),
                                "Your are not registered driver, please contact eZhire for further assistance.",
                                Constant.FAILURE,runnable
                            )

                        }


                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    showBasicAlert(
                        requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE,null
                    )
                }

            }

        })


    }

    private fun setUserInPref(user: User) {

        AppData.setLogin(true)
        AppData.setAppToken(user.ExtraField2)
        AppData.setEmail(user.UserName)
        user.MobileNo?.let { AppData.setPhone(it) }
        AppData.setName(user.FirstName + " " + user.LastName)
        AppData.setUserID(user.ID)

        val request = FcmUser()
        request.active = true
        request.userID = user.ID
        request.username = user.UserName
        request.FcmKey = AppData.getFCMToken()?:""


        if(AppData.getFCMToken().isNullOrBlank()){
            hideProgressIndicator()
            Helper.showBasicAlert(requireContext(),"Info","Notification Token not found",Constant.INFO,null)
        }else{
            saveFCMTokenToServer(request)
        }

    }

    private fun saveFCMTokenToServer(param : FcmUser){

        viewModel.registerFCMTokenToServer(param).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> {
                    //showProgressIndicator()
                }
                is Response.Success<*> -> {
                    hideProgressIndicator()

                    startActivity(Intent(requireContext(), MainActivity::class.java))
                    activity?.finish()
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    showBasicAlert(
                        requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        }
    }



    private fun showProgress() {
        try {

            binding.fab.alpha = 0.5f
            binding.fab.isEnabled = false
            binding.progressBar.visibility = View.VISIBLE

        } catch (e: Exception) {
            Log.e("Ex", e.toString())
        }
    }

    private fun hideProgress() {

        try {

            binding.fab.alpha = 1f
            binding.fab.isEnabled = true
            binding.progressBar.visibility = View.INVISIBLE

        } catch (e: Exception) {
            Log.e("Ex", e.toString())
        }

    }






}