package com.ezhire.driver.presentation.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle

class Extension {

    inline fun <T> Context.openActivity(it: Class<T>, extras: Bundle.() -> Unit = {}) {
        val intent = Intent(this, it)
        intent.putExtras(Bundle().apply(extras))
        startActivity(intent)
    }


    inline fun <reified T : Activity> Context.startActivity(block: Intent.() -> Unit = {}) {
        startActivity(Intent(this, T::class.java).apply(block))
    }


}