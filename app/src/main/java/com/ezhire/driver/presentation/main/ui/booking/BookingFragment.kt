package com.ezhire.driver.presentation.main.ui.booking

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.animation.LinearInterpolator
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.setPadding
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.birjuvachhani.locus.Locus
import com.directions.route.*
import com.ezhire.driver.R
import com.ezhire.driver.data.MyForegroundService
import com.ezhire.driver.data.Response
import com.ezhire.driver.data.network.HttpEndPoints
import com.ezhire.driver.data.polyLineData
import com.ezhire.driver.data.service.firebase.Config
import com.ezhire.driver.databinding.FragmentBookingBinding
import com.ezhire.driver.domain.*
import com.ezhire.driver.presentation.ChargesDetailAC
import com.ezhire.driver.presentation.ChatAC
import com.ezhire.driver.presentation.MapsActivity
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.main.ui.document.DocumentAC
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.google.maps.android.SphericalUtil
import com.maps.route.extensions.getColor
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import io.socket.client.IO
import io.socket.client.Manager
import io.socket.client.Socket
import io.socket.emitter.Emitter
import io.socket.engineio.client.Transport
import kotlinx.android.synthetic.main.dialoge_booking_details.view.*
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.math.RoundingMode
import java.text.DateFormat
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ceil


class BookingFragment : BaseFragment(), OnMapReadyCallback, DatePickerDialog.OnDateSetListener,
    RoutingListener, GoogleMap.CancelableCallback {

    private var lastKnownLocation: Location? = null
    private val viewModel by inject<BookingViewModel>()
    private lateinit var binding: FragmentBookingBinding

    lateinit var bookingStatusAdapter: BookingStatusAdapter
    var bookingStatusList = ArrayList<BookingStatus>()

    lateinit var shiftStatusAdapter: ShiftStatusAdapter
    var shiftStatusList = ArrayList<ShiftInfo>()

    lateinit var booking: Booking

    var locationPermissionGranted: Boolean = false

    val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 999

    var map: GoogleMap? = null

    var mFusedLocationProviderClient: FusedLocationProviderClient? = null

    private lateinit var mSocket: Socket

    var driverLoc: Location? = null

    var amountToCollect = 0.0

    var firstTimeLoading = false

    var timeSave = ""

    private lateinit var mLocalBroadcastManager: LocalBroadcastManager

    var currentLocation: Location? = null

    private var fuelLevel = arrayOf("1/8", "2/8", "3/8", "4/8", "5/8", "6/8", "7/8", "8/8")

    companion object {
        var MARKETPLACE_BOOKING = false
        var SHIFT_STARTED = false
        var COLLECTION = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this.requireContext())

        val intentFilter = IntentFilter()
        intentFilter.addAction(Config.LOCATION_UPDATE)
        intentFilter.addAction(Config.PUSH_NOTIFICATION)
        mLocalBroadcastManager.registerReceiver(
            pushNotificationBroadCastReciver, intentFilter
        )

    }

    private val pushNotificationBroadCastReciver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if (intent.getBooleanExtra("TIP", false)) {

                navigateToFragment(R.id.action_nav_booking_to_nav_tips)
                //Toast.makeText(requireContext(),"1 Tip nyas",Toast.LENGTH_SHORT).show()
            } else {
                val newLocation: Location? = intent.getParcelableExtra<Location>("location")
                Log.e("NewLocation", "${newLocation?.latitude},${newLocation?.longitude}")

                newLocation?.let {
                    drawPath(it)
                    currentLocation = it
                }
            }


        }
    }

    override fun onDestroy() {
        mLocalBroadcastManager.unregisterReceiver(pushNotificationBroadCastReciver)
        super.onDestroy()
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentBookingBinding.inflate(layoutInflater)


        binding.navMenu.setOnClickListener {


            if(booking.Status == Constant.BOOKING_CONFIRMED || booking.Status == Constant.DRIVER_ASSIGNED_COLLECTION){
                navigateToFragment(R.id.action_nav_booking_to_nav_home)
            }else{
                openDrawer()
            }
        }

        binding.chatIV.setOnClickListener {
            startActivity(Intent(this.requireContext(), ChatAC::class.java))
        }

        binding.bookingDetailBtn.setOnClickListener {
            showBookingDetails()
        }

        binding.addCashBtn.setOnClickListener {
            showCashCollectionDialog()
        }

        binding.fullScreenView.setOnClickListener {
            val intent = Intent(this.requireContext(), MapsActivity::class.java)
            intent.putExtra("BOOKING", booking)
            startActivity(intent)
        }

        binding.acceptBtn.setOnClickListener {

            if (MARKETPLACE_BOOKING) {
                acceptBooking()
            } else {

                if (booking.RentalTypeID == 2 && (booking.Status == Constant.DRIVER_ARIVED || booking.Status == Constant.RENTAL_STARTED)) {

                    if (booking.Status == Constant.DRIVER_ARIVED) {
                        showShiftDialog(true)
                    } else {
                        if (SHIFT_STARTED) {
                            showShiftDialog(true)
                        } else {

                            //Cash
                            if (booking.PaymentTypeID == 2) {
                                showCashCollectionBeforeRentalEndDialog(amountToCollect)
                            } else {
                                updateBookingStatus(0.0, 0.0, 0, "")
                            }

                        }
                    }

                } else {

                    if (booking.RentalTypeID == 1 && booking.Status == Constant.DRIVER_ARIVED) {

                        showRentalStartDialog()
                    } else {

                        if (booking.Status == Constant.DRIVER_ARRIVED_COLLECTION || booking.Status == Constant.BOOKING_CONFIRMED) {
                            showOdometerDialog()
                        } else {
                            updateBookingStatus(0.0, 0.0, 0, "")
                        }

                        //updateBookingStatus(0.0, 0.0)
                    }
                }
            }
        }

        binding.rejectBtn.setOnClickListener {

            if (booking.Status == Constant.RENTAL_STARTED) {
                showShiftDialog(false)
            } else {
                rejectBooking()
            }
        }

        binding.bookingStatusRV.layoutManager = LinearLayoutManager(
            this.requireContext(),
            RecyclerView.VERTICAL,
            false
        )
        binding.bookingStatusRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 0))

        //initView()

        arguments?.getBoolean("marketplace").let {
            MARKETPLACE_BOOKING = it ?: false
        }

        arguments?.getSerializable("booking")?.let {

            booking = it as Booking

            if (booking.Status == Constant.RENTAL_STARTED) {
                getLastShift()
            }
            fillBookingDetails(it)
            startForgroundService()
        }

        getAmountToCollect()

        val statusArray = arrayOf(
            Constant.DRIVER_ASSIGNED_COLLECTION,
            Constant.CAR_COLLECTED,
            Constant.COLLECT,
            Constant.DRIVER_ARRIVED_COLLECTION,
            Constant.ON_THE_WAY_COLLECTION
        )
        COLLECTION = statusArray.any { it == booking.Status }

        AppData.booking.IS_COLLECTION = true

        val mapFragment = childFragmentManager
            .findFragmentById(R.id.mapView2) as SupportMapFragment?
        mapFragment!!.getMapAsync(this)

        binding.navigateBtn.setOnClickListener {
            openMap()
        }


        binding.contactVendor.setOnClickListener {
            try {
                val intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + booking.DriverNumber.toString()))
                startActivity(intent)
            } catch (e: Exception) {
                Log.e("Exception", e.toString())
            }
        }

        mFusedLocationProviderClient =
            LocationServices.getFusedLocationProviderClient(this.requireActivity())


        return binding.root
    }

    private fun startForgroundService() {
        val myServiceIntent = Intent(this.requireContext(), MyForegroundService::class.java)
        myServiceIntent.action = "START"
        myServiceIntent.putExtra("DATA", "eZhire is sending location updates")
        ContextCompat.startForegroundService(this.requireContext(), myServiceIntent)
    }

    private fun stopForgroundService() {
        Log.e("Stop Forground", "Stop Service")

        val isServiceRunning = isServiceRunningInForeground(
            this.requireContext(),
            MyForegroundService::class.java
        )
        Log.e("Service Status", "Status: ${isServiceRunning}")

        if (isServiceRunning) {
            val stopServiceIntent = Intent(this.requireContext(), MyForegroundService::class.java)
            stopServiceIntent.action = "STOP"
            ContextCompat.startForegroundService(this.requireContext(), stopServiceIntent)
        }


        polyLineData.clearData(this.requireContext())
        AppData.put(0.0, "DISTANCE")

        AppData.preferences.edit().putBoolean("FIRST_POINT", true).apply()
        AppData.preferences.edit().putBoolean("SECOND_POINT", true).apply()
        AppData.preferences.edit().putBoolean("LAST_POINT", true).apply()
    }

    private fun isServiceRunningInForeground(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                if (service.foreground) {

                    return true
                }
            }
        }
        return false
    }

    private fun showShiftDetails() {

        val alert =
            Dialog(this.requireContext(), android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(true)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_shift_detail, null)


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener {
            alert.dismiss()
        }
        val shiftRV = view.findViewById<RecyclerView>(R.id.alltipsRV)

        shiftRV.layoutManager = LinearLayoutManager(
            this.requireContext(),
            RecyclerView.VERTICAL,
            false
        )
        shiftRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


        shiftStatusAdapter =
            ShiftStatusAdapter(
                this.requireContext(), shiftStatusList
            )
        shiftRV.adapter = shiftStatusAdapter

        alert?.setContentView(view)
        alert?.window?.setWindowAnimations(R.style.DialogAnimation)
        alert?.show()

        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    private fun acceptBooking() {

        val requestParam = BookingAccept()
        requestParam.BookingID = booking.BookingID
        requestParam.UserID = AppData.getUserID()

        viewModel.acceptBooking(requestParam).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val response = it.data as BookingAccept
                    if (response.Status == true) {
                        checkBooking()
                    } else {
                        val runnable = Runnable {
                            stopForgroundService()
                            navigateToFragment(R.id.action_nav_booking_to_nav_home)

                        }

                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Info",
                            "${response.ResponseMessage}",
                            Constant.INFO,
                            runnable
                        )
                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }

    }

    private fun rejectBookingAPI() {

        val requestParam = BookingAccept()
        requestParam.BookingID = booking.BookingID
        requestParam.UserID = AppData.getUserID()

        viewModel.rejectBooking(requestParam).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val response = it.data as CallResponse
                    if (response.Status.contains("success", true)) {
                        stopForgroundService()
                        navigateToFragment(R.id.action_nav_booking_to_nav_home)
                    } else {
                        val runnable = Runnable {
                            stopForgroundService()
                            navigateToFragment(R.id.action_nav_booking_to_nav_home)

                        }

                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Info",
                            "${response.Status}",
                            Constant.INFO,
                            runnable
                        )
                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }

    }

    private fun setBookingStatusAdapter() {
        bookingStatusAdapter =
            BookingStatusAdapter(
                this.requireContext(), bookingStatusList
            )
        binding.bookingStatusRV.adapter = bookingStatusAdapter

    }


    private fun rejectBooking() {

        rejectBookingAPI()
        //stopForgroundService()
        //navigateToFragment(R.id.action_nav_booking_to_nav_home)

    }

    private fun updateBookingStatus(
        amountCollected: Double?,
        bookingTotalAmount: Double,
        odometerReading: Long,
        fuelLevel: String
    ) {

        val requestParam = BookingStatusCode()
        requestParam.BookingID = booking.BookingID
        requestParam.amount = amountCollected

        val bookingDriverStatus = BookingDriverStatus()
        bookingDriverStatus.bookingID = booking.BookingID
        bookingDriverStatus.DriverID = AppData.getUserID()
        bookingDriverStatus.CarID = booking.AssignedCarID
        bookingDriverStatus.VendorID = booking.VendorID
        bookingDriverStatus.Odomoter = odometerReading
        bookingDriverStatus.FuelLevel = fuelLevel

        val date = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(Date())
        val time = SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Date())

        Log.e("Date & Time", "${date} ${time}")
        bookingDriverStatus.Action_Date_String = date
        bookingDriverStatus.Action_Time_String = time

        bookingDriverStatus.Latitude = currentLocation?.latitude.toString()
        bookingDriverStatus.Longitude = currentLocation?.longitude.toString()
        bookingDriverStatus.LatLng = "${currentLocation?.latitude},${currentLocation?.longitude}"


        requestParam.driverStatus = bookingDriverStatus

        if (amountCollected ?: 0.0 > 1) {

            val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
            val cal = Calendar.getInstance()

            val date = dateFormat.format(cal.time)

            requestParam.date = "${date}"
            requestParam.source = "ANDROID"
            requestParam.comments = "cash collected by driver"
            requestParam.booking_amount = bookingTotalAmount
        }

        //requestParam.StatusCode = statusCode

        when (booking.Status) {

            Constant.BOOKING_CONFIRMED -> {

                requestParam.StatusCode = Constant.ON_THE_WAY

                val stopServiceIntent = Intent(
                    this.requireContext(),
                    MyForegroundService::class.java
                )
                stopServiceIntent.action = "ON-THE-WAY"
                ContextCompat.startForegroundService(this.requireContext(), stopServiceIntent)

                AppData.setNotifyCustomer(true)
            }

            Constant.ON_THE_WAY -> {
                requestParam.StatusCode = Constant.DRIVER_ARIVED
                AppData.setNotifyCustomer(true)
            }

            Constant.DRIVER_ARIVED -> {
                requestParam.StatusCode = Constant.RENTAL_STARTED
            }

            Constant.RENTAL_STARTED -> {
                AppData.setNotifyCustomer(true)
                requestParam.StatusCode = Constant.RENTAL_END
            }

            Constant.DRIVER_ASSIGNED_COLLECTION -> requestParam.StatusCode =
                Constant.ON_THE_WAY_COLLECTION

            Constant.ON_THE_WAY_COLLECTION -> requestParam.StatusCode =
                Constant.DRIVER_ARRIVED_COLLECTION

            Constant.DRIVER_ARRIVED_COLLECTION -> requestParam.StatusCode = Constant.RENTAL_END

            Constant.CAR_COLLECTED -> {
                AppData.setNotifyCustomer(true)
                requestParam.StatusCode = Constant.ON_THE_WAY_BACK
            }

            Constant.ON_THE_WAY_BACK -> requestParam.StatusCode = Constant.ARRIVED_DISPATCH_CENTER

        }

        bookingDriverStatus.DriverStatus = requestParam.StatusCode
        bookingDriverStatus.CarStatus = requestParam.StatusCode
        bookingDriverStatus.BookingStatus = requestParam.StatusCode
        bookingDriverStatus.CarID = booking.AssignedCarID

        viewModel.updateBookingStatus(requestParam).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()

                    val runnable = Runnable {
                        getBookingByBookingID()
                    }

                    val response = it.data as BookingResponse

                    if (response.status) {

                        if (this::alertDialoge.isInitialized && alertDialoge.isShowing) {
                            alertDialoge.dismiss()
                        }

                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Success",
                            "" + response.message,
                            Constant.SUCCESS,
                            runnable
                        )
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Info",
                            "" + response.message,
                            Constant.INFO,
                            runnable
                        )
                    }

                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        }


    }

    private fun checkBooking() {

        if (booking.Status == Constant.RENTAL_STARTED) {
            getLastShift()
        }

        viewModel.getBooking(AppData.getUserID()).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val response = it.data as Booking
                    if (response.BookingID > 0 && response.Status != 9) {
                        val bundle = Bundle()
                        bundle.putSerializable("key", response)
                        //navigateToFragment(R.id.nav_booking, bundle)
                        Log.e("Booking", "${response.BookingID}")
                        MARKETPLACE_BOOKING = false
                        booking = response
                        fillBookingDetails(booking)

                    } else {
                        stopForgroundService()
                        navigateToFragment(R.id.action_nav_booking_to_nav_feedback)
                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        }

    }

    @SuppressLint("SetTextI18n")
    private fun fillBookingDetails(booking: Booking) {

        AppData.preferences.edit().putString("ROOM_ID", booking.BookingID.toString()).apply()
        binding.bookingIDTV.text = "Booking ID: ${booking.BookingID}"

        if (booking.PaymentTypeID == 2 && booking.Status > Constant.ON_THE_WAY) {
            binding.addCashBtn.visibility = View.GONE
        } else {
            binding.addCashBtn.visibility = View.GONE
        }


        if (booking.Status == 1 && MARKETPLACE_BOOKING) {
            binding.contactVendor.visibility = View.INVISIBLE
        } else {
            binding.contactVendor.visibility = View.VISIBLE
        }

        val deliveryDateFormatted =
            Helper.dateFormatter(booking.DeliveryDateString, "dd/MM/yyyy", "dd MMM yy")
        val returnDateFormated =
            Helper.dateFormatter(booking.ReturnDateString, "dd/MM/yyyy", "dd MMM yy")

        bookingStatusList = ArrayList()

        binding.navMenu.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_menu))
        binding.navMenu.setPadding(20)

        when (booking.Status) {

            Constant.BOOKING_CONFIRMED -> {
                binding.acceptBtn.text = "On the way"
                binding.rejectBtn.visibility = View.GONE

                bookingStatusList.add(BookingStatus(0, "Driver and Car Assigned", true))

                binding.navMenu.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_back_arrow))
                binding.navMenu.setPadding(2)

            }

            Constant.ON_THE_WAY -> {
                binding.acceptBtn.text = "Arrived"

                //binding.acceptBtn.alpha = 0.5f
                //binding.acceptBtn.isEnabled = false

                binding.rejectBtn.visibility = View.GONE

                bookingStatusList.add(BookingStatus(1, "Car on the way", true))

            }

            Constant.DRIVER_ARIVED -> {
                binding.acceptBtn.text = "Start Rental"
                binding.rejectBtn.visibility = View.GONE

                bookingStatusList.add(BookingStatus(2, "Driver Arrived", true))

            }

            Constant.RENTAL_STARTED -> {

                map?.clear()
                stopForgroundService()
                AppData.clearBookingData()


                binding.acceptBtn.text = "End Rental"

                if (booking.NoOfDays > 1) {
                    binding.rejectBtn.text = "Shift End"

                    if (SHIFT_STARTED) {
                        binding.rejectBtn.text = "Shift End"
                    } else {
                        binding.rejectBtn.text = "Shift Start"
                    }

                    binding.rejectBtn.visibility = View.VISIBLE
                } else {
                    binding.rejectBtn.visibility = View.GONE
                }


            }

            Constant.DRIVER_ASSIGNED_COLLECTION -> {
                binding.acceptBtn.text = "On the way (Collection)"
                binding.rejectBtn.visibility = View.GONE

                bookingStatusList.add(BookingStatus(1, "Assigned for Collection", true))

                binding.navMenu.setImageDrawable(ContextCompat.getDrawable(requireContext(),R.drawable.ic_back_arrow))
                binding.navMenu.setPadding(2)
            }

            Constant.ON_THE_WAY_COLLECTION -> {
                binding.acceptBtn.text = "Arrived (Collection)"
                //binding.acceptBtn.alpha = 0.5f
                //binding.acceptBtn.isEnabled = false
                binding.rejectBtn.visibility = View.GONE

                bookingStatusList.add(BookingStatus(1, "On the way for car collection", true))

            }

            Constant.DRIVER_ARRIVED_COLLECTION -> {
                binding.acceptBtn.text = "Car Collected"
                binding.rejectBtn.visibility = View.GONE
                bookingStatusList.add(BookingStatus(1, "Arrived at collection location", true))

            }

            Constant.CAR_COLLECTED -> {
                binding.acceptBtn.text = "On the way (Dispatch Center)"
                binding.rejectBtn.visibility = View.GONE
            }

            Constant.ON_THE_WAY_BACK -> {
                binding.acceptBtn.text = "Arrived (Dispatch Center)"
                binding.rejectBtn.visibility = View.GONE
            }

        }

        if (MARKETPLACE_BOOKING) {
            binding.acceptBtn.text = "Accept"
            binding.rejectBtn.text = "Reject"
            binding.acceptBtn.visibility = View.VISIBLE
            binding.rejectBtn.visibility = View.VISIBLE

            bookingStatusList.add(BookingStatus(0, "New Booking", true))
        }

        if (booking.Status == Constant.RENTAL_STARTED) {
            bookingStatusList.add(
                BookingStatus(
                    3,
                    "Rental started on " + deliveryDateFormatted + " at " + Helper.formatTime24HrToAmPm(
                        booking.DeliveryTimeString
                    ),
                    true
                )
            )

            bookingStatusList.add(
                BookingStatus(
                    4,
                    "Rental will end on " + returnDateFormated + " at " + Helper.formatTime24HrToAmPm(
                        booking.ReturnTimeString
                    ),
                    false
                )
            )
        }

        setBookingStatusAdapter()
    }

    override fun onMapReady(p0: GoogleMap?) {

        if (p0 != null) {
            this.map = p0
        }

        //this.map.isMyLocationEnabled = true
        //this.map.isTrafficEnabled = true

        if (booking.Status != Constant.RENTAL_STARTED) {

            val routeData = isPathSavedInDevice();
            if (routeData.size > 0) {
                drawExistingPath(routeData)
            } else {
                getCurrentLocationAndDrawPath()
            }

        } else {
            getCurrentLocationAndDrawPath()
        }
    }

    private fun getCurrentLocationAndDrawPath() {

        Locus.getCurrentLocation(this.requireContext()) { it ->
            it.location?.let {

                currentLocation = it
                Log.e("Current loc", "${it.latitude}," + it.longitude)
                val source = LatLng(it.latitude, it.longitude)
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 18f))
                drawPath(it)
            }
            it.error?.let {
                Log.e("Current loc", "err: ${it.message} " + it.localizedMessage)
            }
        }

    }


    private fun drawPath(currentLoc: Location) {


        val source = LatLng(currentLoc.latitude, currentLoc.longitude)
        driverLoc = currentLoc
        var destination = LatLng(booking.DeliveryLat, booking.DeliveryLong)

        if (COLLECTION) {
            destination = LatLng(booking.ReturnLat, booking.ReturnLong)
        }

        if (booking.BookingID > 0 && booking.Status != Constant.RENTAL_STARTED) {

            val startPoint = currentLoc

            val endPoint = Location("locationA")
            endPoint.latitude = destination.latitude
            endPoint.longitude = destination.longitude


            if (line != null && line?.points?.size ?: 0 > 0) {
                val tolerance = 50.0 // meters
                val isLocationOnPath =
                    PolyUtil.isLocationOnPath(source, line?.points, true, tolerance)

                if (isLocationOnPath) {

                    val distance = endPoint.distanceTo(startPoint).toDouble()
                    val redrawPath = distanceFormula((distance.toLong()))

                    if (distance < 500 && (booking.Status == Constant.ON_THE_WAY || booking.Status == Constant.ON_THE_WAY_COLLECTION)) {
                        binding.acceptBtn.alpha = 1f
                        binding.acceptBtn.isEnabled = true
                    } else if (distance > 500 && (booking.Status == Constant.ON_THE_WAY || booking.Status == Constant.ON_THE_WAY_COLLECTION)) {
                        //binding.acceptBtn.alpha = 0.5f
                        //binding.acceptBtn.isEnabled = false
                    }

                    Log.e("ReDraw", "Redraw -> ${redrawPath}")

                    currentLoc?.let {

                        map?.animateCamera(
                            CameraUpdateFactory.newCameraPosition(
                                CameraPosition(
                                    source,
                                    map?.cameraPosition?.zoom ?: 0f,
                                    map?.cameraPosition?.tilt ?: 0f,
                                    currentLoc.bearing
                                )
                            ),
                            2000, this
                        )
                        animateMarker(currentLoc, driverMarker)

                        //map.animateCamera(CameraUpdateFactory.newLatLng(source))


                        val df = DecimalFormat("#.####")
                        df.roundingMode = RoundingMode.CEILING

                        var index = 0
                        for ((position, item) in line?.points!!.withIndex()) {
                            val pathLat = df.format(item.latitude)
                            val pathLong = df.format(item.longitude)
                            val currentLaent = df.format(currentLoc.latitude)
                            val currentLong = df.format(currentLoc.longitude)
                            if (currentLaent == pathLat && currentLong == pathLong) {
                                Log.e("Same", "Point same")
                                index = position
                                break  //Breaking the loop when the index found
                            }
                        }
                        //Creating new path from the current location to the destination
                        val listPoints: List<LatLng> =
                            line?.points!!.subList(index, (line?.points?.size!!))

                        val distance = SphericalUtil.computeLength(listPoints)




                        binding.selfPickupTV.text =
                            "${Helper.RoundByString((distance / 1000), 2, 2)}KM away "

                        if (timeSave.length > 1) {
                            binding.selfPickupTV.text = "${
                                Helper.RoundByString(
                                    (distance / 1000),
                                    2,
                                    2
                                )
                            }KM away | ${timeSave}"
                        }

                        val options =
                            PolylineOptions().width(5f).color(getColor(R.color.green_shade))
                                .geodesic(
                                    true
                                )
                        val iterator = listPoints.iterator()
                        while (iterator.hasNext()) {
                            val data = iterator.next()

                            options.add(data)
                        }


                        line?.remove()
                        line = map?.addPolyline(options)

                        if (redrawPath) {
                            line?.remove()

                            //line?.points?.clear()
                            val emptyList = ArrayList<LatLng>()
                            line?.points = emptyList
                            line == null
                        }

                        val data = polyLineData(options)
                        polyLineData.writeData(this.requireContext(), data)

                    }
                } else {

                    Log.e("Google", "Routing Call")
                    val routing = Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(
                            this
                        ).waypoints(
                            source,
                            destination
                        ).key(getString(R.string.google_api_key)).build()

                    routing.execute()
                }
            } else {
                Log.e("Google", "Routing Call")
                firstTimeLoading = true
                val routing =
                    Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING).withListener(
                        this
                    ).waypoints(
                        source,
                        destination
                    ).key(getString(R.string.google_api_key)).build()

                routing.execute()
            }


        } else {

            binding.imageView7.visibility = View.GONE
            binding.selfPickupTV.visibility = View.GONE
            binding.navigateBtn.visibility = View.GONE
            binding.contactVendor.visibility = View.GONE
            binding.divider3.visibility = View.GONE
            binding.fullScreenView.visibility = View.GONE

            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 15f))

            if (driverMarker != null) {
                //driverMarker?.position = driverLoc

                driverLoc?.let { animateMarker(it, driverMarker) }


            } else {
                val icon: BitmapDescriptor =
                    BitmapDescriptorFactory.fromResource(R.drawable.car_marker)

                val lat = LatLng(driverLoc!!.latitude, driverLoc!!.longitude)
                driverMarker = map?.addMarker(MarkerOptions().position(lat).icon(icon))

            }

        }


    }

    override fun onFinish() {

    }

    override fun onCancel() {

    }

    private fun distanceFormula(currentDistance: Long): Boolean {


        val initalTotalDistance = AppData.get<Double>("DISTANCE") //M
        var redraw = false

        if (initalTotalDistance != null && initalTotalDistance > 0) {

            var distanceBy3 = initalTotalDistance.div(3)

            if ((distanceBy3 % 1) == 0.0) {
                distanceBy3 -= -1.0;
            } else {
                distanceBy3 = ceil(distanceBy3)
            }

            val point1: Long = (distanceBy3.times(2)).toLong()
            val point2 = distanceBy3.toLong()
            Log.e("Value", "${currentDistance} ${point1}")


            val firstPointVisit = AppData.preferences.getBoolean("FIRST_POINT", true)
            if (firstPointVisit && (currentDistance <= point1)) {
                Log.e("Point 1", "${currentDistance} ${point1}")
                AppData.preferences.edit().putBoolean("FIRST_POINT", false).apply()
                redraw = true
            }

            val SecondPointVisit = AppData.preferences.getBoolean("SECOND_POINT", true)
            if (SecondPointVisit && (currentDistance <= point2)) {
                Log.e("Point 2", "${currentDistance} ${point2}")
                AppData.preferences.edit().putBoolean("SECOND_POINT", false).apply()
                redraw = true
            }

            val LastPointVisit = AppData.preferences.getBoolean("LAST_POINT", true)
            if (LastPointVisit && (currentDistance <= 400)) {
                Log.e("LastPointVisit", "${currentDistance} 400}")
                AppData.preferences.edit().putBoolean("LAST_POINT", false).apply()
                redraw = true
            }

        }

        return redraw

    }

    private fun openMap() {

        var uri = String.format(
            Locale.ENGLISH,
            "http://maps.google.com/maps?daddr=%f,%f (%s)",
            booking.DeliveryLat,
            booking.DeliveryLong,
            "" + booking.DeliveryLocationAddress
        )

        if (COLLECTION) {
            uri = String.format(
                Locale.ENGLISH,
                "http://maps.google.com/maps?daddr=%f,%f (%s)",
                booking.ReturnLat,
                booking.ReturnLong,
                "" + booking.ReturnLocationAddress
            )
        }

        val gmmIntentUri =
            Uri.parse(uri)
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        if (mapIntent.resolveActivity(this.requireActivity().packageManager) != null) {
            startActivity(mapIntent)
        }
    }


    private fun getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(
                this.requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            locationPermissionGranted = true
            getDeviceLocation()
        } else {
            ActivityCompat.requestPermissions(
                this.requireActivity(), arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                }
            }
        }
        updateLocationUI()
    }

    @SuppressLint("MissingPermission")
    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                map?.isMyLocationEnabled = true
                map?.uiSettings?.isMyLocationButtonEnabled = true
            } else {
                map?.isMyLocationEnabled = false
                map?.uiSettings?.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (locationPermissionGranted) {
                val locationResult = mFusedLocationProviderClient?.lastLocation

                locationResult?.addOnCompleteListener {
                    if (it.isSuccessful) {
                        lastKnownLocation = it.result
                        if (lastKnownLocation != null) {
                            map?.moveCamera(
                                CameraUpdateFactory.newLatLngZoom(
                                    LatLng(
                                        lastKnownLocation!!.latitude,
                                        lastKnownLocation!!.longitude
                                    ), 15f
                                )
                            )

                            if (booking.BookingID > 0) {

                                var location = LatLng(booking.DeliveryLat, booking.DeliveryLong)
                                if (COLLECTION) {
                                    location = LatLng(booking.ReturnLat, booking.ReturnLong)
                                }

                                map?.addMarker(
                                    MarkerOptions().position(location)
                                        .title(booking.DeliveryLocationAddress)
                                )
                                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15f))


                            }
                        }
                    } else {
                        Log.d("TAG", "Current location is null. Using defaults.")
                        Toast.makeText(
                            this.requireContext(),
                            "last location reterival failed",
                            Toast.LENGTH_LONG
                        ).show()
                    }
                }

            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }


    private fun sendLocationToSocket(location: Location) {

        val messageJSONO = JSONObject()
        messageJSONO.put("room", "" + booking.BookingID)
        messageJSONO.put("location", "${location.latitude},${location.longitude}")

//        mSocket?.let {
//            if(mSocket != null && mSocket.connected()){
//                mSocket.emit("driver_location", messageJSONO)
//                Log.e("Location Sent", messageJSONO.toString())
//            }else{
//                Log.e("Socket","Connection:"+mSocket.connected())
//            }
//        }


    }

    var onUpdateChat = Emitter.Listener {

        Log.e("data", "" + it.get(0))
    }

    private fun getLocationFromSocket() {

        mSocket.on("driver_location", onUpdateChat)
    }

    private var alert: Dialog? = null

    private fun showShiftDialog(updateBookingStatus: Boolean) {

        if (alert != null && alert?.isShowing == true) {
            alert?.dismiss()
        }

        alert =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(false)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_start_shift, null)


        val dialogHeadingTV = view.findViewById<TextView>(R.id.shiftTitleTV)
        val dateET = view.findViewById<TextView>(R.id.dateET)
        val timeET = view.findViewById<TextView>(R.id.TimeET)
        val fuelET = view.findViewById<Spinner>(R.id.fuelLevelET)
        val odometerET = view.findViewById<TextView>(R.id.odometerET)
        val cashET = view.findViewById<TextView>(R.id.cashET)
        val cashCollectView = view.findViewById<LinearLayout>(R.id.cashCollectView)

        val aa: ArrayAdapter<*> =
            ArrayAdapter<Any?>(
                this.requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                fuelLevel
            )

        fuelET.adapter = aa

        val timeFormat: DateFormat = SimpleDateFormat("HH:mm")
        val timecal = Calendar.getInstance()
        System.out.println(timeFormat.format(timecal.time))

        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val cal = Calendar.getInstance()
        System.out.println(dateFormat.format(cal.time))

        var date = dateFormat.format(cal.time)
        var time = timeFormat.format(timecal.time)

        dateET.setText(date)
        timeET.setText(time)

        val callback =
            DatePickerDialog.OnDateSetListener { datePickerDialog: DatePickerDialog, year: Int, month: Int, day: Int ->

                var dayStr = ""
                var monthStr = ""

                dayStr = if (day < 10) {
                    "0${day}"
                } else {
                    "" + day
                }

                val selectedMonth = (month + 1)
                monthStr = if (selectedMonth < 10) {
                    "0${selectedMonth}"
                } else {
                    "${selectedMonth}"
                }


                date = "${dayStr}/${monthStr}/${year}"
                dateET.setText(date)

            }

        val timecallback =
            TimePickerDialog.OnTimeSetListener { datePickerDialog: TimePickerDialog, hourOfDay: Int, minute: Int, seconds: Int ->

                var hourString = ""
                if (hourOfDay < 10) {
                    hourString += "0${hourOfDay}"
                } else {
                    hourString += "${hourOfDay}"
                }

                var minuteString = ""
                if (minute < 10) {
                    minuteString += "0${minute}"
                } else {
                    minuteString += "${minute}"
                }

                time = "${hourString}:${minuteString}"
                timeET.setText(time)
            }

        dateET.setOnClickListener {
            val now = Calendar.getInstance()
            val dpd: DatePickerDialog = DatePickerDialog.newInstance(
                callback,
                now[Calendar.YEAR],  // Initial year selection
                now[Calendar.MONTH],  // Initial month selection
                now[Calendar.DAY_OF_MONTH] // Inital day selection
            )

            dpd.minDate = Calendar.getInstance()
            // If you're calling this from a support Fragment
            // If you're calling this from a support Fragment
            dpd.show(this.parentFragmentManager, "Datepickerdialog")

        }



        timeET.setOnClickListener {
            val now = Calendar.getInstance()
            val dpd: TimePickerDialog = TimePickerDialog.newInstance(
                timecallback,
                now[Calendar.HOUR_OF_DAY],  // Initial year selection
                now[Calendar.MINUTE],  // Initial month selection
                false // Inital day selection
            )

            dpd.setMinTime(
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                now.get(Calendar.SECOND)
            )
            // If you're calling this from a support Fragment
            // If you're calling this from a support Fragment
            dpd.show(this.parentFragmentManager, "Timepickerdialog")
        }

        cashCollectView.visibility = View.GONE
        if (!SHIFT_STARTED) {
            dialogHeadingTV.text = "Shift Start Information"
        } else {
            dialogHeadingTV.text = "Shift End Information"
        }


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener { view1: View? ->

            val request = ShiftInfo()
            request.booking_id = booking.BookingID
            request.driver_id = AppData.getUserID()
            request.fuel_level = fuelET.selectedItem as String
            request.car_odometer = odometerET.text.toString()

//            if (fuelET.text.toString().isBlank()) {
//                request.fuel_level = "0"
//            }

            if (odometerET.text.toString().isBlank()) {
                request.car_odometer = "0"
            }


            if (cashET.text.toString().isBlank()) {
                request.cash_collected = 0.0
            } else {
                request.cash_collected = cashET.text.toString().toDouble()
            }

            request.shift_date = date
            request.shift_time = time

            if (!SHIFT_STARTED) {
                request.shift_type = 1
            } else {
                request.shift_type = 2
            }

            saveShiftInfo(request, updateBookingStatus)

        }

        val cancelBtn = view.findViewById<Button>(R.id.cancel_btn)
        cancelBtn.setOnClickListener { alert?.dismiss() }

        alert?.setContentView(view)
        alert?.show()

        alert?.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
    }

    private fun showRentalStartDialog() {


        val alert =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alert.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert.setCancelable(false)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_start_shift, null)


        val dialogHeadingTV = view.findViewById<TextView>(R.id.shiftTitleTV)
        val dateET = view.findViewById<TextView>(R.id.dateET)
        val timeET = view.findViewById<TextView>(R.id.TimeET)
        val fuelET = view.findViewById<Spinner>(R.id.fuelLevelET)
        val odometerET = view.findViewById<TextView>(R.id.odometerET)
        val cashET = view.findViewById<TextView>(R.id.cashET)
        val cashCollectView = view.findViewById<LinearLayout>(R.id.cashCollectView)

        val aa: ArrayAdapter<*> =
            ArrayAdapter<Any?>(this.requireContext(), android.R.layout.simple_spinner_dropdown_item, fuelLevel)

        fuelET.adapter = aa

        val timeFormat: DateFormat = SimpleDateFormat("HH:mm")
        val timecal = Calendar.getInstance()
        System.out.println(timeFormat.format(timecal.time))

        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val cal = Calendar.getInstance()
        System.out.println(dateFormat.format(cal.time))

        var date = booking.DeliveryDateString
        var time = booking.DeliveryTimeString

        dateET.text = booking.DeliveryDateString
        timeET.text = booking.DeliveryTimeString

        val callback =
            DatePickerDialog.OnDateSetListener { datePickerDialog: DatePickerDialog, year: Int, month: Int, day: Int ->

                var dayStr = ""
                var monthStr = ""
                dayStr = if (day < 10) {
                    "0${day}"
                } else {
                    "${day}"
                }

                val selectedMonth = (month + 1)
                monthStr = if (selectedMonth < 10) {
                    "0${selectedMonth}"
                } else {
                    "${selectedMonth}"
                }


                date = "${dayStr}/${monthStr}/${year}"
                dateET.text = date

            }

        val timecallback =
            TimePickerDialog.OnTimeSetListener { datePickerDialog: TimePickerDialog, hourOfDay: Int, minute: Int, seconds: Int ->

                var hourString = ""
                if (hourOfDay < 10) {
                    hourString += "0${hourOfDay}"
                } else {
                    hourString += "${hourOfDay}"
                }

                var minuteString = ""
                if (minute < 10) {
                    minuteString += "0${minute}"
                } else {
                    minuteString += "${minute}"
                }

                time = "${hourString}:${minuteString}"
                timeET.text = time

                //time = "${hourOfDay}:${minute}"
                //timeET.setText(time)
            }

        dateET.setOnClickListener {
            val now = Calendar.getInstance()
            val dpd: DatePickerDialog = DatePickerDialog.newInstance(
                callback,
                now[Calendar.YEAR],  // Initial year selection
                now[Calendar.MONTH],  // Initial month selection
                now[Calendar.DAY_OF_MONTH] // Inital day selection
            )

            dpd.minDate = Calendar.getInstance()
            dpd.show(this.parentFragmentManager, "Datepickerdialog")

        }

        timeET.setOnClickListener {
            val now = Calendar.getInstance()
            val dpd: TimePickerDialog = TimePickerDialog.newInstance(
                timecallback,
                now[Calendar.HOUR_OF_DAY],  // Initial year selection
                now[Calendar.MINUTE],  // Initial month selection
                false // Inital day selection
            )
            dpd.setMinTime(
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                now.get(Calendar.SECOND)
            )

            dpd.show(this.parentFragmentManager, "Timepickerdialog")
        }

        cashCollectView.visibility = View.GONE
        dialogHeadingTV.text = "Booking Information"


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener { view1: View? ->

            val request = UpdateBooking()
            request.booking_id = booking.BookingID

            val fuelLevelET : String = fuelET.selectedItem as String

            request.in_fuel_level = fuelLevelET.toString()
            request.in_odometer = odometerET.text.toString()

            request.deliver_date_string = date
            request.deliver_time_string = time

            if (odometerET.text.toString().isBlank()) {

                if (odometerET.text.toString().isBlank()) {
                    odometerET.error = "Enter Odometer"
                }
            } else {
                saveRentalStartInfo(request, alert)
            }


        }

        val cancelBtn = view.findViewById<Button>(R.id.cancel_btn)
        cancelBtn.setOnClickListener { alert.dismiss() }

        alert.setContentView(view)
        alert.show()

        alert.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
    }

    private fun saveShiftInfo(request: ShiftInfo, updateBookingStatus: Boolean) {

        viewModel.saveCarKM(request).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    if (this.alert != null && this.alert?.isShowing == true) {
                        this.alert?.dismiss()
                    }

                    val response = it.data as ShiftInfo
                    if (response.status == true) {

                        amountToCollect = response.amount ?: 0.0
                        if (SHIFT_STARTED && booking.PaymentTypeID == 2 && updateBookingStatus) {
                            showCashCollectionBeforeRentalEndDialog(amountToCollect)
                            getLastShift()

                        } else {
                            getLastShift()
                            if (updateBookingStatus) {
                                updateBookingStatus(
                                    amountToCollect,
                                    amountToCollect,
                                    request.car_odometer?.toLong() ?: 0L,
                                    ""
                                )
                            } else {
                                val runnable = Runnable {
                                    checkBooking()
                                }
                                Helper.showBasicAlert(
                                    this.requireContext(),
                                    getString(R.string.sucess),
                                    "Shift Info Save",
                                    Constant.SUCCESS,
                                    runnable
                                )
                            }

                        }

                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            "Something went wrong",
                            Constant.FAILURE, null
                        )
                    }
                    Log.e("Res", " " + response.status.toString())
//                    if (response.status?.contains("success") == true) {
//                        updateBookingStatus()
//
//                    } else {
//                        navigateToFragment(R.id.action_nav_booking_to_nav_home)
//                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }
    }

    private fun saveRentalStartInfo(request: UpdateBooking, alert: Dialog?) {

        val odometer = request.in_odometer ?: "0"
        val fuelLevel = request.in_fuel_level ?: "0"


        viewModel.updateBooking(request).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    if (alert?.isShowing == true) {
                        alert.dismiss()
                    }

                    val response = it.data as CallResponse
                    if (response.Status.contentEquals("success")) {
                        updateBookingStatus(0.0, 0.0, odometer.toLong(), fuelLevel)
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            "Something went wrong",
                            Constant.FAILURE, null
                        )
                    }
                    Log.e("Res", " " + response.Status)
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }
    }

    private fun saveCash(request: SaveCash, alert: Dialog?) {

        viewModel.addCash(request).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    if (alert?.isShowing == true) {
                        alert.dismiss()
                    }

                    val response = it.data as CallResponse
                    if (response.Status.contentEquals("success")) {
                        //updateBookingStatus()

                        val runnable = Runnable {
                            getBookingByBookingID()
                        }
                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.sucess),
                            "Cash collected.",
                            Constant.SUCCESS, runnable
                        )
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            "Something went wrong",
                            Constant.FAILURE, null
                        )
                    }
                    Log.e("Res", " " + response.Status)
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }
    }

    private fun showCashCollectionDialog() {

        val alert =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(true)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_cash, null)

        val cashET = view.findViewById<TextView>(R.id.cashET)

        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val cal = Calendar.getInstance()
        System.out.println(dateFormat.format(cal.time))

        var date = dateFormat.format(cal.time)


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener { view1: View? ->


            if (cashET.text.toString().isBlank()) {
                cashET.error = "enter amount"
                //Toast.makeText(this.requireContext(),"Enter Valid Amount",Toast.LENGTH_SHORT).show()
            } else {
                val amount: Double = cashET.text?.toString()?.toDouble() ?: 0.0
                if (amount < 1) {
                    cashET.error = "enter more amount"
                    //Toast.makeText(this.requireContext(),"Enter more amount",Toast.LENGTH_SHORT).show()

                } else {

                    val request = SaveCash()
                    request.booking_id = booking.BookingID
                    request.date = date
                    request.amount = amount

                    saveCash(request, alert)

                }
            }


        }

        val cancelBtn = view.findViewById<Button>(R.id.cancel_btn)
        cancelBtn.setOnClickListener { alert?.dismiss() }

        alert?.setContentView(view)
        alert?.show()

        alert?.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
    }

    lateinit var alertDialoge: Dialog

    private fun showOdometerDialog() {

        alertDialoge =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alertDialoge.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alertDialoge.setCancelable(true)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_odometer, null)

        val cashET = view.findViewById<TextView>(R.id.cashET)

        val fuelSpinner = view.findViewById<Spinner>(R.id.fuelLevelET)

        val aa: ArrayAdapter<*> =
            ArrayAdapter<Any?>(
                this.requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                fuelLevel
            )

        fuelSpinner.adapter = aa

        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener { view1: View? ->


            //val fuelLevel = fuelSpinner.selectedItemPosition + 1

            val fuelLevel : String = fuelSpinner.selectedItem as String

            if (cashET.text.toString().isBlank()) {
                cashET.error = "enter odometer"
            } else {
                val odometerReading: Long = cashET.text?.toString()?.toLong() ?: 0
                if (odometerReading < 1) {
                    cashET.error = "enter right odometer reading"

                } else {
                    //All Good
                    //alert.dismiss()
                    updateBookingStatus(0.0, 0.0, odometerReading, fuelLevel.toString())

                }
            }


        }

        val cancelBtn = view.findViewById<Button>(R.id.cancel_btn)
        cancelBtn.setOnClickListener { alertDialoge?.dismiss() }

        alertDialoge.setContentView(view)
        alertDialoge.show()

        alertDialoge.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
    }

    private fun showCashCollectionBeforeRentalEndDialog(amount: Double) {

        val alert =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(false)

        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_cash_collect, null)

        val cashET = view.findViewById<TextView>(R.id.cashET)
        val cashTV = view.findViewById<TextView>(R.id.cashTV)

        view.findViewById<ImageView>(R.id.closeBtn).setOnClickListener {
            alert.dismiss()
        }

        cashTV.text = "${booking.chargeType.Currency} ${Helper.RoundByString(amount, 2, 2)}"

        cashET.text = "${Helper.RoundByString(amount, 2, 2)}"

        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val cal = Calendar.getInstance()
        System.out.println(dateFormat.format(cal.time))

        var date = dateFormat.format(cal.time)


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener { view1: View? ->


            if (cashET.text.toString().isBlank()) {
                cashET.error = "enter amount"
                //Toast.makeText(this.requireContext(),"Enter Valid Amount",Toast.LENGTH_SHORT).show()
            } else {
                val amountCollected: Double = cashET.text?.toString()?.toDouble() ?: 0.0

                if (amountCollected < amountToCollect) {
                    cashET.error = "amount should be equal or greater than ${
                        Helper.RoundByString(
                            amountToCollect,
                            2,
                            2
                        )
                    }"
                } else {
                    alert.dismiss()
                    updateBookingStatus(amountCollected, amount, 0, "")
                }

//                    val request = SaveCash()
//                    request.booking_id = booking.BookingID
//                    request.date = date
//                    request.amount = amountCollected
//                    request.booking_amount = amount


                //saveCash(request, alert)


            }


        }

        val cancelBtn = view.findViewById<Button>(R.id.cancel_btn)
        cancelBtn.setOnClickListener { alert?.dismiss() }

        alert?.setContentView(view)
        alert?.show()

        alert?.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )
    }

    private fun showBookingDetails() {

        val alert =
            Dialog(this.requireContext(), R.style.Dialog_Ninty_Percent)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(true)

        val view: View = LayoutInflater.from(context).inflate(
            R.layout.dialoge_booking_details,
            null
        )

        view.driverName.text = booking.DriverName
        view.driverNumber.text = booking.DriverNumber

        view.documentsTV.setOnClickListener {
            startActivity(Intent(this.requireContext(), DocumentAC::class.java))
        }

        view.numberPlateTV.text = "Car Number Plate: " + booking.CarNumberPlate

        if (booking.Status == 1 && MARKETPLACE_BOOKING) {
            view.driverName.text = "***********"
            view.driverNumber.text = "*********"
            //binding.contactVendor.visibility = View.INVISIBLE
        }

        if (booking.Status == Constant.RENTAL_STARTED && booking.RentalTypeID == Constant.WITH_DRIVE) {
            view.shiftDetail.visibility = View.VISIBLE
        }

        view.driverNumber.setOnClickListener {
            try {
                val intent =
                    Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + booking.DriverNumber.toString()))
                startActivity(intent)
            } catch (e: Exception) {
                Log.e("Exception", e.toString())
            }
        }

        if (booking.RentalTypeID == 2) {
            view.returnCardView.visibility = View.GONE
        }

        view.shiftView.setOnClickListener {
            view.shiftDetail.performClick()
        }



        view.shiftDetail.setOnClickListener {
            if (booking.Status == Constant.RENTAL_STARTED) {
                showShiftDetails()
            }
        }

        view.deliveryAddressTV.text = booking.DeliveryLocationAddress
        view.returnAddressTV.text = booking.ReturnLocationAddress

        //view.selfPickupTV.text = "Please wait..."

        val deliveryDateFormated =
            Helper.dateFormatter(booking.DeliveryDateString, "dd/MM/yyyy", "dd MMM yy")
        val returnDateFormated =
            Helper.dateFormatter(booking.ReturnDateString, "dd/MM/yyyy", "dd MMM yy")

        view.deliveryDateTimeTV.text =
            deliveryDateFormated + " " + Helper.formatTime24HrToAmPm(booking.DeliveryTimeString)

        view.returnDateTimeTV.text =
            returnDateFormated + " " + Helper.formatTime24HrToAmPm(booking.ReturnTimeString)

        if (booking.Status == Constant.DRIVER_ASSIGNED_COLLECTION || booking.Status == Constant.ON_THE_WAY_COLLECTION || booking.Status == Constant.DRIVER_ARRIVED_COLLECTION) {
            view.deliveryAddressTV.text = booking.ReturnLocationAddress
            view.deliveryDateTimeTV.text =
                returnDateFormated + " " + Helper.formatTime24HrToAmPm(booking.ReturnTimeString)

        }
        try {
            view.currencyTV.text = booking.chargeType.Currency
            view.bookingTotalAmountTV.text =
                "" + booking.chargesList?.filter { it.ChargeTypeID == -1 }
                    ?.get(0)?.TotalCharge
        } catch (e: Exception) {

        }

        view.bookingChargesView.setOnClickListener {
            view.viewDetailsTV.performClick()
        }

        view.viewDetailsTV.setOnClickListener {
            val intent = Intent(this.requireActivity(), ChargesDetailAC::class.java)
            intent.putExtra("BOOKING", booking)
            intent.putExtra("TEST", "12")
            startActivity(intent)
        }

        val okBtn = view.findViewById<View>(R.id.nav_menu)
        okBtn.setOnClickListener { view1: View? ->

            alert.dismiss()


        }

        alert?.setContentView(view)
        alert?.show()

        alert?.window?.setBackgroundDrawable(
            ColorDrawable(
                Color.TRANSPARENT
            )
        )

    }

    private fun getLastShift() {

        getAllShiftDetails()

        viewModel.getShiftInfo(AppData.getUserID(), booking_id = booking.BookingID)
            .observe(this.requireActivity(),
                {
                    when (it) {
                        is Response.Loading -> {

                        }
                        is Response.Success<*> -> {

                            val response = it.data as ShiftInfo
                            if (response.shift_type ?: 0 > 0) {

                                SHIFT_STARTED = response.shift_type != 2

                                if (SHIFT_STARTED) {
                                    binding.rejectBtn.text = "Shift End"
                                } else {
                                    binding.rejectBtn.text = "Shift Start"
                                }


                            } else {
                                Helper.showBasicAlert(
                                    this.requireContext(),
                                    getString(R.string.alert),
                                    "Something went wrong",
                                    Constant.FAILURE, null
                                )
                            }
                            Log.e("Res", " " + response.status.toString())
                        }
                        is Response.Error -> {
                            //hideProgressIndicator()
                            Helper.showBasicAlert(
                                this.requireContext(),
                                getString(R.string.alert),
                                it.exception,
                                Constant.FAILURE, null
                            )
                        }

                    }
                })
    }

    private fun notifyCustomer() {
        val driverLocation = DriverLocation()
        driverLocation.UserID = booking.UserID
        viewModel.notifyCustomer(driverLocation).observe(this.requireActivity(), {

        })
    }

    private fun getAmountToCollect() {


        viewModel.getAmountToCollect(booking_id = booking.BookingID).observe(this.requireActivity(),
            {
                when (it) {
                    is Response.Loading -> {

                    }
                    is Response.Success<*> -> {

                        val response = it.data as CallResponse
                        amountToCollect = response.amount ?: 0.0
                    }
                    is Response.Error -> {
                        //hideProgressIndicator()
                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            it.exception,
                            Constant.FAILURE, null
                        )
                    }

                }
            })
    }

    private fun getAllShiftDetails() {

        viewModel.getAllShiftInfo(AppData.getUserID(), booking_id = booking.BookingID)
            .observe(this.requireActivity(),
                {
                    when (it) {
                        is Response.Loading -> {

                        }
                        is Response.Success<*> -> {

                            val data = it.data as ArrayList<ShiftInfo>
                            shiftStatusList = data

                        }
                        is Response.Error -> {
                            //hideProgressIndicator()
                            Helper.showBasicAlert(
                                this.requireContext(),
                                getString(R.string.alert),
                                it.exception,
                                Constant.FAILURE, null
                            )
                        }

                    }
                })
    }


    override fun onDateSet(view: DatePickerDialog?, year: Int, monthOfYear: Int, dayOfMonth: Int) {

    }

    override fun onRoutingFailure(p0: RouteException?) {
        Log.e("onRoutingFailure", "" + p0?.message.toString())
    }

    override fun onRoutingStart() {
        Log.e("OnRoutingStart", "OnRoutingStart")
    }

    var driverMarker: Marker? = null
    var deliveryLocationMarker: Marker? = null
    private var line: Polyline? = null

    override fun onRoutingSuccess(list: java.util.ArrayList<Route>, p1: Int) {


        if (isAdded) {

            try {
                //Get all points and plot the polyLine route.
                //Log.e("NewDestination", "Request Receive")
                val listPoints: List<LatLng> = list.get(0).getPoints()
                val options =
                    PolylineOptions().width(5f).color(getColor(R.color.com_facebook_blue)).geodesic(
                        true
                    )
                val iterator = listPoints.iterator()
                while (iterator.hasNext()) {
                    val data = iterator.next()

                    options.add(data)
                }

                val driverLatLng = LatLng(driverLoc!!.latitude, driverLoc!!.longitude)

                val driverPositionByPath = list.get(0).points[0]

                map?.run {

                    if (driverMarker != null) {
                        //driverMarker?.position = driverLoc
                        val loc = Location("")
                        loc.latitude = driverPositionByPath.latitude
                        loc.longitude = driverPositionByPath.longitude
                        loc.bearing = (driverLoc?.bearing ?: 0.0) as Float
                        driverLoc?.let { animateMarker(it, driverMarker) }

                    } else {
//
                        val icon: BitmapDescriptor =
                            BitmapDescriptorFactory.fromResource(R.drawable.car_marker)
                        driverMarker = map?.addMarker(
                            MarkerOptions().position(driverPositionByPath).icon(
                                icon
                            ).anchor(0.5f, 0.5f)
                        )
                    }

                    var destination = LatLng(
                        booking.DeliveryLat,
                        booking.DeliveryLong
                    )

                    if (COLLECTION) {
                        destination = LatLng(
                            booking.ReturnLat,
                            booking.ReturnLong
                        )
                    }

                    if (deliveryLocationMarker != null) {
                        deliveryLocationMarker?.position = destination
                    } else {
                        deliveryLocationMarker = map?.addMarker(
                            MarkerOptions().position(destination).icon(
                                Helper.vectorToBitmap(
                                    ContextCompat.getDrawable(
                                        requireContext(),
                                        R.drawable.ic_location_pin
                                    ), resources.getColor(R.color.dark_shade)
                                )
                            )
                        )
                    }

                }


                if (line != null) {
                    line?.remove();
                }

                line = map?.addPolyline(options)

                binding.selfPickupTV.text =
                    list[0].distanceText + " away | " + list[0].durationText

                AppData.put(list[0], "ROUTE_INFO")
                timeSave = list[0].durationText

                var destination = LatLng(
                    booking.DeliveryLat,
                    booking.DeliveryLong
                )

                if (COLLECTION) {
                    destination = LatLng(
                        booking.ReturnLat,
                        booking.ReturnLong
                    )
                }

                if (firstTimeLoading) {

                    val endPoint = Location("locationA")
                    endPoint.latitude = destination.latitude
                    endPoint.longitude = destination.latitude

                    val loc = Location("")
                    loc.latitude = driverPositionByPath.latitude
                    loc.longitude = driverPositionByPath.longitude

                    val distance = endPoint.distanceTo(loc).toDouble()

                    AppData.put(distance, "DISTANCE")

                }
//                if (list[0].distanceValue < 400) {
//                    notifyCustomer()
//                }


                val builder = LatLngBounds.Builder()
                //val latLng = LatLng(booking.DeliveryLat, booking.DeliveryLong)
                builder.include(destination)
                builder.include(LatLng(driverLoc!!.latitude, driverLoc!!.longitude))
                val bounds = builder.build()
                //map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50))


                if (firstTimeLoading) {
                    map?.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLatLng, 15f))
                } else {
                    map?.animateCamera(CameraUpdateFactory.newLatLng(driverLatLng))
                }

                val data = polyLineData(options)
                polyLineData.writeData(this.requireContext(), data)

            } catch (e: java.lang.Exception) {
                Toast.makeText(
                    this.requireContext(),
                    "EXCEPTION: Cannot parse routing response",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

    }

    override fun onRoutingCancelled() {

    }


    private fun connectSocket() {
        try {
            val opts: IO.Options = IO.Options()
            //opts.port = 80
            mSocket = IO.socket(HttpEndPoints.WEBSOCKET_SERVER_URL)
            mSocket.connect()

            mSocket.io().on(Manager.EVENT_TRANSPORT) { args ->
                val transport: Transport = args[0] as Transport
                transport.on(Transport.EVENT_ERROR, Emitter.Listener { args ->
                    val e = args[0] as Exception
                    Log.e("Socket", "Transport error $e")
                    e.printStackTrace()
                    e.cause!!.printStackTrace()
                })
            }

            //mSocket.on(Socket.EVENT_CONNECT, onConnect)

        } catch (e: Exception) {
            Log.e("Socket", " " + e.toString())
        }


    }

    override fun onStart() {
        super.onStart()
    }

    private fun isPathSavedInDevice(): ArrayList<PolylineOptions> {
        Log.e("Time+1", "" + System.currentTimeMillis())
        return (polyLineData.getData(requireContext()))
    }

    private fun drawExistingPath(routeList: ArrayList<PolylineOptions>) {

        Log.e("Time+2", "" + System.currentTimeMillis())
        Log.e("Exisiting Path", "TRUE")
        //val polyLine = routeList
        if (routeList.size > 0) {

            val driverLocation = routeList.get(0).points.get(0)


            for (item in routeList) {
                item.color(ContextCompat.getColor(this.requireContext(), R.color.green_shade))
            }
            //polyLine.get(0).color(ContextCompat.getColor(this.requireContext(),R.color.green_shade))

            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(driverLocation, 18f))

            map?.run {

                if (driverMarker != null) {


                    driverLoc?.let { animateMarker(it, driverMarker) }

                } else {
//
                    val icon: BitmapDescriptor =
                        BitmapDescriptorFactory.fromResource(R.drawable.car_marker)
                    driverMarker = map?.addMarker(
                        MarkerOptions().position(driverLocation).icon(
                            icon
                        ).anchor(0.5f, 0.5f)
                    )
                }

                var latLng = LatLng(
                    booking.DeliveryLat,
                    booking.DeliveryLong
                )

                if (COLLECTION) {
                    latLng = LatLng(
                        booking.ReturnLat,
                        booking.ReturnLong
                    )
                }

                if (deliveryLocationMarker != null) {

                    deliveryLocationMarker?.position = latLng
                } else {
                    deliveryLocationMarker = map?.addMarker(
                        MarkerOptions().position(latLng).icon(
                            Helper.vectorToBitmap(
                                ContextCompat.getDrawable(
                                    requireContext(),
                                    R.drawable.ic_location_pin
                                ), resources.getColor(R.color.dark_shade)
                            )
                        )
                    )
                }

            }


            if (line != null) {
                line?.remove();
            }

            line = map?.addPolyline(routeList.get(0))

            val routeInfo: Route? = AppData.get<Route>("ROUTE_INFO")
            if (routeInfo != null) {
                timeSave = routeInfo.durationText
                binding.selfPickupTV.text =
                    routeInfo.durationText + " | " + routeInfo.distanceText + " away"
            }
            Log.e("Time+3", "" + System.currentTimeMillis())

        }
    }

    private fun getBookingByBookingID() {

        viewModel.getBookingByBookingID(AppData.booking.BookingID).observe(this.requireActivity(), {
            when (it) {
                is Response.Loading -> {
                    showProgressIndicator()
                }
                is Response.Success<*> -> {

                    hideProgressIndicator()
                    booking = it.data as Booking
                    if (booking.BookingID > 0 && booking.Status != 9) {

                        fillBookingDetails(booking)
                    } else {
                        AppData.clearBookingData()
                        stopForgroundService()
                        navigateToFragment(R.id.action_nav_booking_to_nav_home)

                    }
                }

                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        })
    }


}

fun animateMarker(destination: Location, marker: Marker?) {
    if (marker != null) {
        val startPosition = marker.position
        val endPosition = LatLng(destination.latitude, destination.longitude)
        val startRotation = marker.rotation
        val latLngInterpolator: LatLngInterpolator = LinearFixed()
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = 1000 // duration 1 second
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { animation ->
            try {
                val v = animation.animatedFraction
                val newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition)
                marker.position = newPosition
                //marker.rotation = computeRotation(v, startRotation, destination.bearing)
                marker.setAnchor(0.5f, 0.5f)
            } catch (ex: java.lang.Exception) {
                // I don't care atm..
            }
        }
        valueAnimator.start()


    }
}

private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
    val normalizeEnd = end - start // rotate start to 0
    val normalizedEndAbs = (normalizeEnd + 360) % 360
    val direction: Float =
        if (normalizedEndAbs > 180) -1f else 1.toFloat() // -1 = anticlockwise, 1 = clockwise
    val rotation: Float = if (direction > 0) {
        normalizedEndAbs
    } else {
        normalizedEndAbs - 360
    }
    val result = fraction * rotation + start
    return (result + 360) % 360
}

private interface LatLngInterpolator {
    fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng

}

class LinearFixed : LatLngInterpolator {


    override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
        val lat = (b.latitude - a.latitude) * fraction + a.latitude
        var lngDelta = b.longitude - a.longitude
        // Take the shortest path across the 180th meridian.
        if (Math.abs(lngDelta) > 180) {
            lngDelta -= Math.signum(lngDelta) * 360
        }
        val lng = lngDelta * fraction + a.longitude
        return LatLng(lat, lng)
    }
}

