package com.ezhire.driver.presentation.main.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ezhire.driver.data.Repository
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.BookingDriverStatus
import com.ezhire.driver.domain.BookingStatusCode
import com.ezhire.driver.domain.DriverActiveStatus
import com.ezhire.driver.domain.DriverLocation
import com.ezhire.driver.presentation.base.BaseViewModel
import com.ezhire.driver.presentation.utils.Constant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeViewModel(val repo : Repository) : BaseViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    fun driverStatus(request : DriverActiveStatus): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.driverStatus(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getDriverStatus(driverID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getDriverStatus(driverID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun updateDriverLocation(param : DriverLocation): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateDriverLocation(param)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getBookings(userID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getBookingList(userID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getBookingByBookingID(bookingID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getBookingByBookingID(bookingID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }


    fun updateNCM(request : BookingDriverStatus): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateNCM(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }
}