package com.ezhire.driver.presentation.login

import android.os.Bundle
import android.util.Log
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.ezhire.driver.R
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.databinding.ActivityLoginBinding
import com.ezhire.driver.presentation.utils.AppData
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.installations.FirebaseInstallations

class LoginActivity : BaseActivity() {

    private lateinit var binding : ActivityLoginBinding

    companion object {
        lateinit var navController : NavController
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_up_with_delay, R.anim.slide_up_with_delay)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

    //  navController = findNavController(R.id.nav_host_login_fragment)

        try {
            FirebaseInstanceId.getInstance().instanceId
                .addOnSuccessListener(this@LoginActivity) {

                    Log.e("FCM TOKEN", it.token)
                    AppData.setFCMToken(it.token)
                }

        } catch (e: Exception) {

        }


    }

    override fun onSupportNavigateUp(): Boolean {
        navController = findNavController(R.id.nav_host_login_fragment)
        return super.onSupportNavigateUp()
    }

}