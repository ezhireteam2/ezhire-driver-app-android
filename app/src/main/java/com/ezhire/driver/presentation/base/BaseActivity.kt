package com.ezhire.driver.presentation.base


import android.os.Bundle
import android.os.PersistableBundle
import android.provider.SyncStateContract
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import com.ezhire.driver.presentation.progress.ProgressDialog
import com.ezhire.driver.presentation.progress.ProgressIndicator
import com.ezhire.driver.presentation.login.LoginActivity
import com.ezhire.driver.presentation.main.MainActivity
import com.ezhire.driver.presentation.main.MainActivity.Companion.binding
import com.ezhire.driver.presentation.utils.ConnectionLiveData
import com.ezhire.driver.presentation.utils.Helper.hideKeyboard
import com.ezhire.driver.presentation.utils.Helper.verifyAvailableNetwork
import io.socket.client.IO
import io.socket.client.Socket
import java.lang.Exception

open class BaseActivity : AppCompatActivity(), ProgressIndicator, BaseView {

    private lateinit var progressBarDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {



    }




    override fun showProgressIndicator() {
        progressBarDialog = ProgressDialog()
        progressBarDialog.showDialog(
            supportFragmentManager,
            BaseActivity::class.java.simpleName
        )
    }

    override fun hideProgressIndicator() {
        if (this::progressBarDialog.isInitialized && progressBarDialog.isAdded ) {
            progressBarDialog.dismiss()
        }
    }

    override fun navigateToFragment(id: Int, args: Bundle?) {
        if (args != null) {
            MainActivity.navController.navigate(id, args)
            return
        }
        MainActivity.navController.navigate(id)
    }

    override fun isInternetAvailable(): Boolean {
        return verifyAvailableNetwork(this)
    }

    override fun hideKeyboard() {
        hideKeyboard(this)

    }

    override fun callDialog(type: String) {
        TODO("Not yet implemented")
    }

    override fun openDrawer() {
            if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
                binding.drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                binding.drawerLayout.openDrawer(GravityCompat.START)
            }
    }


}