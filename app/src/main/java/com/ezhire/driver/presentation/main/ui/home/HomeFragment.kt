package com.ezhire.driver.presentation.main.ui.home

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.location.*
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.LinearInterpolator
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.RecyclerView
import com.birjuvachhani.locus.*
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.data.service.firebase.Config
import com.ezhire.driver.databinding.FragmentHomeBinding
import com.ezhire.driver.domain.*
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.*
import com.google.android.gms.location.*
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.driver_status_bottom_sheet.view.*
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.android.ext.android.inject
import java.util.*


class HomeFragment : BaseFragment() {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
        const val LOCATION_PERMISSION_RC = 501
        const val DEFAULT_ZOOM = 15.0f
        var API_CALL = true

    }

    private val viewModel by inject<HomeViewModel>()
    private lateinit var binding: FragmentHomeBinding
    private lateinit var mGoogleMap: GoogleMap

    private lateinit var bookingList: ArrayList<BookingList>

    private lateinit var mLocalBroadcastManager: LocalBroadcastManager

    val request = BookingDriverStatus()

    private var fuelLevel = arrayOf("1/8", "2/8", "3/8", "4/8", "5/8", "6/8", "7/8", "8/8")

    override fun onDestroy() {
        super.onDestroy()
        //Locus.stopLocationUpdates()
    }

    @SuppressLint("MissingPermission")
    private val callback = OnMapReadyCallback { googleMap ->
        mGoogleMap = googleMap ?: return@OnMapReadyCallback

        mGoogleMap.uiSettings.isMyLocationButtonEnabled = true
        mGoogleMap.uiSettings.isCompassEnabled = true

        val configuration = Configuration()
        configuration.enableBackgroundUpdates = true
        configuration.shouldResolveRequest = false


        Locus.configure {
            request {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                interval = 1000L
                fastestInterval = 1000L
                maxWaitTime = 1000L
                smallestDisplacement = 30f
            }
        }
        Locus.setLogging(true)


        Handler(Looper.getMainLooper()).postDelayed({
            if (isAdded) {

                if (checkPermission()) {
                    Locus.getCurrentLocation(this.requireContext()) {
                        it.location?.let { it1 ->
                            drawMarker(it1)
                            request.LatLng = "${it1.latitude},${it1.longitude}"
                            request.Latitude = "${it1.latitude}"
                            request.Longitude = "${it1.longitude}"
                            startLocationUpdate()
                        }

                        it.error?.let { it2 ->
                            Log.e("Error", it2.message.toString())
                        }
                    }

                } else {
                    val alertBuilder = AlertDialog.Builder(
                        requireContext()
                    )
                    alertBuilder.setCancelable(true)
                    alertBuilder.setTitle("Location Permission required")
                    alertBuilder.setMessage("ezhire Driver app collects location data to enable [\"live tracking of driver\"], [\"customer can track their driver\"] even when the app is closed or not in use")
                    alertBuilder.setPositiveButton(
                        "Allow Access"
                    ) { dialog, which ->
                        Locus.getCurrentLocation(this.requireActivity().applicationContext) {
                            it.location?.let { it1 ->
                                drawMarker(it1)

                                startLocationUpdate()
                            }
                        }
                    }
                    alertBuilder.setNegativeButton(
                        "Not Allow"
                    ) { dialog, which ->
                        requireActivity().finish()
                    }

                    val alert = alertBuilder.create()
                    alert.show()
                }


            }
        }, 3000)

    }

    private fun checkPermission(): Boolean {

        return ((ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_COARSE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED)
                && (ContextCompat.checkSelfPermission(
            requireContext(),
            Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED))
    }

    private fun askPermission() {

        var arrayOfPermission = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            arrayOfPermission = arrayOf(
                android.Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            )
        }
        ActivityCompat.requestPermissions(requireActivity(), arrayOfPermission, 200);
    }


    private fun startLocationUpdate() {

        if (isAdded) {


            Locus.startLocationUpdates(this) { result ->
                result.location?.let { /* Received location update */
                    //Log.e("Location", "" + it.latitude + "," + it.longitude)
                    Log.e("loc", "" + Helper.getLocationTitle(this.requireContext()))


                    drawMarker(it)

                }
                result.error?.let { /* Received error! */
                    Log.e("Location", "" + it.message + "," + it.localizedMessage)
                    when {
                        it.isDenied -> { /* Permission denied */
                        }
                        it.isPermanentlyDenied -> { /* Permission is permanently denied */
                        }
                        it.isFatal -> { /* Something else went wrong! */
                        }
                        it.isSettingsDenied -> { /* Settings resolution denied by the user */
                        }
                        it.isSettingsResolutionFailed -> { /* Settings resolution failed! */
                        }
                    }
                }
            }
        }

    }

    var myMarker: Marker? = null
    private fun drawMarker(location: Location) {

        val latLng = LatLng(location.latitude, location.longitude)

        mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))
        //mGoogleMap?.addMarker(MarkerOptions().position(latLng))


        if (myMarker != null) {
            //driverMarker?.position = driverLoc

            latLng?.let {
                animateMarker(
                    location,
                    myMarker
                )
            }

        } else {
            val icon: BitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.car_marker)

            //val lat = LatLng(driverLoc!!.latitude, driverLoc!!.longitude)
            myMarker = mGoogleMap?.addMarker(MarkerOptions().position(latLng).icon(icon))

        }

        if (binding.switch1.isChecked) {
            updateDriverLocationToServer(location)
        }

    }

    private fun updateDriverLocationToServer(location: Location) {

        val requestParam = DriverLocation()
        requestParam.UserID = AppData.getUserID()
        requestParam.Latitude = location.latitude
        requestParam.Longitude = location.longitude
        requestParam.Latlng = "${requestParam.Latitude},${requestParam.Longitude}"

        viewModel.updateDriverLocation(requestParam).observe(this) {
            when (it) {
                //is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()
                    val response = it.data as CallResponse
                    if (response.Status.equals("success")) {
                        Log.e("Location Updated", "Sucess")
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Info",
                            "${response.Status}",
                            Constant.INFO,
                            null
                        )
                        Log.e("Location Updated", "Error")
                    }
                }
                is Response.Error -> {
                    //hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

//        val myServiceIntent = Intent(this.requireContext(), MyForegroundService::class.java)
//        myServiceIntent.putExtra("DATA", "eZhire is sending location updates")
//        ContextCompat.startForegroundService(this.requireContext(), myServiceIntent)

    }

    override fun onStart() {
        super.onStart()

        Helper.appUpdateAlert(requireContext())

//        if(activity?.intent?.getBooleanExtra("TIP",false) == true){
//            navigateToFragment(R.id.action_nav_home_to_nav_tips)
//        }
//        if(intent.getBooleanExtra("TIP",false)){
//            navigateToFragment(R.id.action_nav_home_to_nav_tips)
//        }

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentHomeBinding.inflate(layoutInflater)

        setUpMap()


        binding.navIcon.setOnClickListener {
            openDrawer()
        }


        getDriverStatus()



        binding.bookingRV.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )
        binding.bookingRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


        getBookings()

        return binding.root
    }

    private fun getDriverStatus() {

        viewModel.getDriverStatus(AppData.getUserID()).observe(requireActivity()) {
            when (it) {
                is Response.Success<*> -> {
                    //hideProgressIndicator()
                    val response = it.data as DriverActiveStatus

                    API_CALL = false
                    binding.switch1.setOnCheckedChangeListener(null)

                    binding.switch1.isChecked = response.Active == 1

                    if(response.Active == 1){
                        binding.switch1.text = "Your status is online"
                    }else{
                        binding.switch1.text = "Your status is offline"
                    }


                    binding.switch1.setOnCheckedChangeListener { buttonView, isChecked ->
                        setDriverStatus(isChecked)
                    }


                }
                is Response.Error -> {
                    //hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }
    }

    private fun setDriverStatus(active: Boolean) {
        val requestParam = DriverActiveStatus()
        if (active) {
            requestParam.Active = 1
        } else {
            requestParam.Active = 0
        }
        requestParam.UserID = AppData.getUserID()

        viewModel.driverStatus(requestParam).observe(this.requireActivity()) {
            when (it) {
                //is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()
                    val response = it.data as CallResponse
                    if (response.Status.contains("success")) {

                        if(switch1.isChecked){
                            binding.switch1.text = "Your status is online"
                        }else{
                            binding.switch1.text = "Your status is offline"
                        }
                    }
                }
                is Response.Error -> {
                   // hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        }
    }

    override fun onPause() {
        super.onPause()

        mLocalBroadcastManager.unregisterReceiver(pushNotificationBroadCastReceiver)
    }

    override fun onResume() {
        super.onResume()

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this.requireContext())
        mLocalBroadcastManager.registerReceiver(
            pushNotificationBroadCastReceiver,
            IntentFilter(Config.PUSH_NOTIFICATION)
        )

    }

    private fun setUpMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment!!.getMapAsync(callback)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out kotlin.String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

    }

    private fun getBookings() {

        binding.loadingProgress.visibility = View.VISIBLE
        binding.bookingRV.visibility = View.INVISIBLE
        viewModel.getBookings(AppData.getUserID()).observe(this.requireActivity()) {
            when (it) {
                //is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()

//                    Handler(Looper.getMainLooper()).postDelayed({
//                        hideProgressIndicator()
//                    }, 500)

                    val data = it.data as BookingResponse

                    if (data.status) {

                        if (data.bookingList?.isNotEmpty() == true) {
                            bookingList = data.bookingList
                            binding.bookingRV.visibility = View.VISIBLE
                            binding.noRecordFoundTV.visibility = View.INVISIBLE
                            binding.bookingRV.adapter =
                                BookingAdapter(this.requireContext(), bookingList, itemClicked)
                            binding.loadingProgress.visibility = View.INVISIBLE

                            val driverActiveStatus = arrayOf(
                                Constant.ON_THE_WAY,
                                Constant.DRIVER_ARIVED,
                                Constant.ON_THE_WAY_BACK,
                                Constant.DRIVER_ARRIVED_COLLECTION,
                                Constant.ON_THE_WAY_COLLECTION
                            )

                            var bookingIDtoLoad = 0L
                            for (booking in bookingList) {
                                val driverActive =
                                    driverActiveStatus.any { it1 -> it1 == booking.Status }
                                if (driverActive) {
                                    bookingIDtoLoad = booking.BookingID
                                    break;
                                }
                            }

                            binding.availableBookingTV.text = "Available Booking (${bookingList.size})"
                            if (bookingIDtoLoad > 0) {
                                getBookingByBookingID(bookingIDtoLoad)
                            }
                        } else {
                            binding.noRecordFoundTV.visibility = View.VISIBLE
                            binding.bookingRV.visibility = View.INVISIBLE
                            binding.loadingProgress.visibility = View.INVISIBLE
                        }

                        if (data.driverStatus != null && data.driverStatus.DriverStatus ?: 0 > 0) {

                            if (data.driverStatus.DriverStatus == Constant.RENTAL_STARTED || data.driverStatus.DriverStatus == Constant.ON_THE_WAY_BACK_COLLECTION ||
                                data.driverStatus.DriverStatus == Constant.ON_THE_WAY_BACK || data.driverStatus.DriverStatus == Constant.RENTAL_END
                            )
                                showBottomSheetDialog(data.driverStatus)
                        }

                    } else {
                        binding.noRecordFoundTV.visibility = View.VISIBLE
                        binding.bookingRV.visibility = View.INVISIBLE
                        binding.loadingProgress.visibility = View.INVISIBLE

                        Helper.showBasicAlert(
                            this.requireContext(),
                            getString(R.string.alert),
                            data.message,
                            Constant.FAILURE, null
                        )
                    }

//                    if(data.status && data.bookingList?.isNotEmpty() == true){
//                        bookingList = data.bookingList
//
//                        if(bookingList.size > 0){
//
//                            binding.bookingRV.visibility = View.VISIBLE
//                            binding.noRecordFoundTV.visibility = View.INVISIBLE
//                            binding.bookingRV.adapter = BookingAdapter(this.requireContext(),bookingList,itemClicked)
//                            binding.loadingProgress.visibility = View.INVISIBLE
//                            val firstBooking = bookingList[0]
//                            if(firstBooking.Status == Constant.ON_THE_WAY
//                                || firstBooking.Status == Constant.DRIVER_ARIVED || firstBooking.Status == Constant.ON_THE_WAY_BACK
//                                || firstBooking.Status == Constant.DRIVER_ARRIVED_COLLECTION || firstBooking.Status == Constant.ON_THE_WAY_COLLECTION){
//                                getBookingByBookingID(firstBooking.BookingID)
//                            }
//
//                        }else{
//                            binding.noRecordFoundTV.visibility = View.VISIBLE
//                            binding.bookingRV.visibility = View.INVISIBLE
//                            binding.loadingProgress.visibility = View.INVISIBLE
//                        }
//                    }else{
//
//                        showBottomSheetDialog()
//                        binding.noRecordFoundTV.visibility = View.VISIBLE
//                        binding.bookingRV.visibility = View.INVISIBLE
//                        binding.loadingProgress.visibility = View.INVISIBLE
//                        if(!data.status){
//                            Helper.showBasicAlert(
//                                this.requireContext(),
//                                getString(R.string.alert),
//                                data.message,
//                                Constant.FAILURE, null
//                            )
//                        }
//                    }


                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        }
    }

    internal var itemClicked: IItemClicked =
        object : IItemClicked {
            override fun itemClicked(postion: Int) {

                getBookingByBookingID(bookingList[postion].BookingID)
//                val bundle = Bundle()
//                bundle.putLong("booking_id", bookingList[postion].BookingID)
//                bundle.putBoolean("marketplace", false)
//                navigateToFragment(R.id.action_nav_home_to_nav_booking, bundle)
//                val intent = Intent(context, BookingHistoryDetailAC::class.java)
//                intent.putExtra("BOOKING", bookingList[postion])
//                startActivity(intent)

            }
        }

    private fun getBookingByBookingID(bookingID: Long) {

        viewModel.getBookingByBookingID(bookingID).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> {
                    showProgressIndicator()
                }

                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val bundle = Bundle()
                    AppData.booking = it.data as Booking
                    bundle.putSerializable("booking", AppData.booking)
                    bundle.putBoolean("marketplace", false)
                    navigateToFragment(R.id.action_nav_home_to_nav_booking, bundle)
                }

                is Response.Error -> {

                }
            }
        }
    }

    private val pushNotificationBroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {

            if(intent.getBooleanExtra("TIP",false)){
                if(dialog != null && dialog.isShowing){
                    dialog.dismiss();
                }
                navigateToFragment(R.id.action_nav_home_to_nav_tips)
                Toast.makeText(requireContext(),"1 Tip nyas",Toast.LENGTH_SHORT).show()
            }else{
                getBookings()
            }
        }
    }
    lateinit var dialog: BottomSheetDialog;
    private fun showBottomSheetDialog(lastStatus: BookingDriverStatus) {


        val view: View = LayoutInflater.from(this.requireContext())
            .inflate(R.layout.driver_status_bottom_sheet, null)
        dialog = BottomSheetDialog(this.requireContext())

        val aa: ArrayAdapter<*> =
            ArrayAdapter<Any?>(
                this.requireContext(),
                android.R.layout.simple_spinner_dropdown_item,
                fuelLevel
            )

        view.fuelLevelET.adapter = aa

        dialog.setCancelable(false)
        dialog.setContentView(view)
        dialog.show()

        if (lastStatus.DriverStatus == Constant.RENTAL_STARTED) {
            view.lastStatusTV.text = "Your last status is Rental Started"
            request.DriverStatus = Constant.ON_THE_WAY_BACK
            request.CarStatus = 0
            request.Odomoter = 0
            request.CarID = 0

            //view.fuelLevelET.setText("0")
            view.odometerET.setText("0")
            view.fuelLevelET.isEnabled = false
            view.odometerET.isEnabled = false
            request.FuelLevel = ""

            view.fuelLevelView.visibility = View.GONE
            view.odometerView.visibility = View.GONE

            view.acceptBtn.text = "On the way back"
        } else if (lastStatus.DriverStatus == Constant.ON_THE_WAY_BACK) {

            view.lastStatusTV.text = "Your last status is On the way to dispatch center"
            view.acceptBtn.text = "Arrived at Dispatch Center"
            request.DriverStatus = Constant.ARRIVED_DISPATCH_CENTER

            //view.fuelLevelET.setText("0")
            view.odometerET.setText("0")
            view.fuelLevelET.isEnabled = false
            view.odometerET.isEnabled = false

            view.fuelLevelView.visibility = View.GONE
            view.odometerView.visibility = View.GONE

            request.Odomoter = 0
            request.CarStatus = 0
            request.DriverStatus = Constant.DRIVER_AVAILABLE
            request.FuelLevel = ""

            request.CarID = lastStatus.CarID
            //request.CarStatus = Constant.ARRIVED_DISPATCH_CENTER

        } else if (lastStatus.DriverStatus == Constant.RENTAL_END) {
            view.lastStatusTV.text = "Your last status is Rental End"
            request.DriverStatus = Constant.ON_THE_WAY_BACK_COLLECTION
            request.CarStatus = Constant.ON_THE_WAY_BACK_COLLECTION
            request.CarID = lastStatus.CarID
            request.FuelLevel = view.fuelLevelET.selectedItem as String


        } else if (lastStatus.DriverStatus == Constant.ON_THE_WAY_BACK_COLLECTION) {
            view.lastStatusTV.text = "Your last status is On the way to dispatch center"
            view.acceptBtn.text = "Arrived at Dispatch Center"
            request.DriverStatus = Constant.DRIVER_AVAILABLE
            request.CarStatus = Constant.ARRIVED_DISPATCH_CENTER
            request.CarID = lastStatus.CarID
            request.FuelLevel = view.fuelLevelET.selectedItem as String

        }

        view.acceptBtn.setOnClickListener {

            if (request.CarID ?: 0 > 0) {

                if (view.odometerET.text.toString().isEmpty()) {
                    view.odometerET.error = "Enter Odometer"
                    return@setOnClickListener
                } else {
                    request.Odomoter = view.odometerET.text.toString().toLong()
                }
            }

            request.IsNCM = false
            request.DriverID = AppData.getUserID()


            request.Action_Date_String = Helper.getCurrentDate("dd/MM/yyyy")
            request.Action_Time_String = Helper.getCurrentTime("HH:mm")



            changeDriverStatus(request, dialog)
            //Toast.makeText(requireContext(),""+request.DriverStatus,Toast.LENGTH_SHORT).show()
        }

        //view.btn.text = ""+buttonText

    }

    private fun changeDriverStatus(driverStatus: BookingDriverStatus, dialog: BottomSheetDialog) {

        viewModel.updateNCM(driverStatus).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> {
                    showProgressIndicator()
                }

                is Response.Success<*> -> {

                    dialog.dismiss()
                    hideProgressIndicator()

                    val response = it.data as BookingResponse

                    val runnable = Runnable {
                        //clearForm();
                        getBookings()
                    }

                    if (response.status) {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Success",
                            "" + response.message,
                            Constant.SUCCESS,
                            runnable
                        )
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Fail",
                            "" + response.message,
                            Constant.FAILURE,
                            runnable
                        )
                    }

                }

                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        }
    }
}

fun animateMarker(destination: Location, marker: Marker?) {
    if (marker != null) {
        val startPosition = marker.position
        val endPosition = LatLng(destination.latitude, destination.longitude)
        val startRotation = marker.rotation
        val latLngInterpolator: LatLngInterpolator = LinearFixed()
        val valueAnimator = ValueAnimator.ofFloat(0f, 1f)
        valueAnimator.duration = 1000 // duration 1 second
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.addUpdateListener { animation ->
            try {
                val v = animation.animatedFraction
                val newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition)
                marker.position = newPosition
                marker.rotation = computeRotation(v, startRotation, destination.bearing)
            } catch (ex: java.lang.Exception) {
                // I don't care atm..
            }
        }
        valueAnimator.start()
    }
}

private fun computeRotation(fraction: Float, start: Float, end: Float): Float {
    val normalizeEnd = end - start // rotate start to 0
    val normalizedEndAbs = (normalizeEnd + 360) % 360
    val direction: Float =
        if (normalizedEndAbs > 180) -1f else 1.toFloat() // -1 = anticlockwise, 1 = clockwise
    val rotation: Float = if (direction > 0) {
        normalizedEndAbs
    } else {
        normalizedEndAbs - 360
    }
    val result = fraction * rotation + start
    return (result + 360) % 360
}

private interface LatLngInterpolator {
    fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng

}

class LinearFixed : LatLngInterpolator {


    override fun interpolate(fraction: Float, a: LatLng, b: LatLng): LatLng {
        val lat = (b.latitude - a.latitude) * fraction + a.latitude
        var lngDelta = b.longitude - a.longitude
        // Take the shortest path across the 180th meridian.
        if (Math.abs(lngDelta) > 180) {
            lngDelta -= Math.signum(lngDelta) * 360
        }
        val lng = lngDelta * fraction + a.longitude
        return LatLng(lat, lng)
    }
}




