package com.ezhire.driver.presentation

import android.content.res.Configuration
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.domain.AddOns
import com.ezhire.driver.domain.Booking
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import kotlinx.android.synthetic.main.activity_charges_detail_ac.*
import java.util.*

class ChargesDetailAC : BaseActivity() {

    //var apiService: ApiInterface? = null

    var AddOnsDetailRV: androidx.recyclerview.widget.RecyclerView? = null
    var summaryAdapter: AddOnsDetailAdapter? = null
    private var SummaryList = ArrayList<AddOns>()

    var booking : Booking? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val locale = Locale("en")
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )

        setContentView(R.layout.activity_charges_detail_ac)


        booking = intent.getSerializableExtra("BOOKING") as Booking?

        //booking = intent.extras?.getParcelable<Booking>("BOOKING")


        //val applicationClass: ApplicationClass = applicationContext as ApplicationClass
        //apiService = applicationClass.apiInterface

        okBtn.setOnClickListener {
            finish()
        }

        nav_menu.setOnClickListener {
            finish()
        }

        bookingIDTV.text = "Booking ID: "+booking?.BookingID

        AddOnsDetailRV = findViewById(R.id.addonsDetailsRV)
        AddOnsDetailRV?.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            this@ChargesDetailAC,
            RecyclerView.VERTICAL,
            false
        )
        AddOnsDetailRV?.addItemDecoration(SpaceItemDecoration(0, 0, 2, 0))


        if(booking?.chargesList?.any { it.ChargeTypeID == Constant.VAT } == true){
            booking?.chargesList?.first { it.ChargeTypeID == Constant.VAT }?.AddonTitle = booking?.chargeType?.TaxLabel?:""

        }


        summaryAdapter = AddOnsDetailAdapter(
            this@ChargesDetailAC,
            booking?.chargesList as ArrayList<AddOns>,
            null,
            false,
            booking?.chargeType?.Currency?:"",
            false
        )
        AddOnsDetailRV?.adapter = summaryAdapter


        newReturnDateTV.text = booking?.DeliveryDateString+ " "+ Helper.formatTime24HrToAmPm(booking?.DeliveryTimeString)
        previousReturnDateTV.text = booking?.ReturnDateString+ " "+ Helper.formatTime24HrToAmPm(booking?.ReturnTimeString)



    }

}
