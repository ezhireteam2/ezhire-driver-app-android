package com.ezhire.driver.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ezhire.driver.data.Repository
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.DriverLocation
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.base.BaseView
import com.ezhire.driver.presentation.base.BaseViewModel
import com.ezhire.driver.presentation.utils.Constant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ChargesDetailViewModel(val repo : Repository) : BaseViewModel() {


    fun getBooking(userID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getBookings(userID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }


    fun getMarketPlaceBooking(bookingID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getMarketPlaceBooking(bookingID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun updateDriverLocation(param : DriverLocation): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateDriverLocation(param)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

}