package com.ezhire.driver.presentation.app

import com.ezhire.driver.data.Repository
import com.ezhire.driver.data.network.ApiClient
import com.ezhire.driver.presentation.login.mobileNoLogin.MobileViewModel
import com.ezhire.driver.presentation.main.MainViewModel
import com.ezhire.driver.presentation.main.ui.booking.BookingViewModel
import com.ezhire.driver.presentation.main.ui.document.DocumentACViewModel
import com.ezhire.driver.presentation.main.ui.feedback.FeedbackViewModel
import com.ezhire.driver.presentation.main.ui.history.BookingHistoryViewModel
import com.ezhire.driver.presentation.main.ui.tips.TipsViewModel
import com.ezhire.driver.presentation.main.ui.home.HomeViewModel
import com.ezhire.driver.presentation.main.ui.ncm.NcmViewModel
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Helper
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dataModule = module {

    // singleton of class
    single { Helper } //

    single { AppData }

    //single { EzhireDatabase.getInstance(androidContext()) }

    single { ApiClient.provideApiService }

    //single { Repository(get(), get()) }

    single { Repository(get()) }


    // create new instance every time
    factory {  }




}

val viewModelsModule = module {
    // ViewModel

    viewModel { MobileViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { BookingViewModel(get()) }
    viewModel { MainViewModel(get()) }
    viewModel { BookingHistoryViewModel(get()) }
    viewModel { FeedbackViewModel(get()) }
    viewModel { TipsViewModel(get()) }
    viewModel { NcmViewModel(get()) }
    viewModel { DocumentACViewModel(get()) }


}

val fragmentModules = module {
//    fragment { HomeFragment() }
//    fragment { DetailsFragment(get()) }
}