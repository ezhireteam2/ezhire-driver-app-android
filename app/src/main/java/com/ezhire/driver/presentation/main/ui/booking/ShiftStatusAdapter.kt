package com.ezhire.driver.presentation.main.ui.booking

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.ezhire.driver.R
import com.ezhire.driver.databinding.ItemBookingStatusBinding
import com.ezhire.driver.domain.BookingStatus
import com.ezhire.driver.domain.ShiftInfo
import com.ezhire.driver.presentation.base.IItemClicked
import java.util.*


class ShiftStatusAdapter(
    private val context: Context,
    itemList: ArrayList<ShiftInfo>
) : androidx.recyclerview.widget.RecyclerView.Adapter<ShiftStatusAdapter.ViewHolder>() {

    private var itemList = ArrayList<ShiftInfo>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val viewHolder = ViewHolder( inflater.inflate(R.layout.item_shift, parent, false))
        return viewHolder
    }

    override fun onBindViewHolder(holder: ShiftStatusAdapter.ViewHolder, position: Int) {

        val item = itemList[position]

        //holder.titleTV.text = booking.StatusTitle

        holder.dateTV.text = item.shift_date
        holder.startTimeTV.text = item.shift_start_time
        holder.endTimeTV.text = item.shift_end_time
        holder.startKM.text = item.start_car_odometer
        holder.endKM.text = item.end_car_odometer
        holder.extraTV.text = item.extra_hrs.toString()


    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }



    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var dateTV: TextView = itemView.findViewById(R.id.dateTV)
        var startTimeTV: TextView = itemView.findViewById(R.id.startTimeTV)
        var endTimeTV: TextView = itemView.findViewById(R.id.endTimeTV)
        var startKM: TextView = itemView.findViewById(R.id.startKM)
        var endKM: TextView = itemView.findViewById(R.id.endKM)
        var extraTV: TextView = itemView.findViewById(R.id.extraTV)

    }


}

//class BookingStatusAdapter(val context: Context) : RecyclerView.Adapter<BookingStatusAdapter.VH>() {
//
//    private lateinit var view : ItemBookingStatusBinding
//
//    private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<BookingStatus>() {
//        override fun areItemsTheSame(oldItem: BookingStatus, newItem: BookingStatus): Boolean {
//            return oldItem.id == newItem.id
//        }
//        override fun areContentsTheSame(oldItem: BookingStatus, newItem: BookingStatus): Boolean {
//            return oldItem == newItem
//        }
//    }
//    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)
//
//    fun setList(list: ArrayList<BookingStatus>){
//        differ.submitList(list)
//    }
//    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
//        view  = ItemBookingStatusBinding.inflate(LayoutInflater.from(context), parent, false)
//        return VH(view.root)
//    }
//    override fun onBindViewHolder(holder: VH, position: Int) {
//        val item  = differ.currentList[position]
//        holder.bindItems(item)
//
//        holder.circleView.tag = position
//        holder.setIsRecyclable(false)
//
//        isSelected(item.isSelected,holder)
//    }
//    override fun getItemViewType(position: Int): Int {
//        return position
//    }
//    override fun getItemCount() : Int {
//        return differ.currentList.size
//    }
//    inner class VH(itemView : View) : RecyclerView.ViewHolder(itemView){
//
//        var title: TextView = view.title
//        var circleView: ImageView = view.circle
//        var line1 = view.line0
//        var line2 = view.line1
//        fun bindItems(item: BookingStatus) {
//            title.text = (item.StatusTitle ?: "N/A")
//        }
//    }
//}
//
//fun BookingStatusAdapter.isSelected(isSelected : Boolean, holder: BookingStatusAdapter.VH){
//
//    if (holder.circleView.tag == 0) {
//        holder.line1.visibility = View.INVISIBLE
//    } else if (holder.circleView.tag == itemCount - 1) {
//        holder.line2.visibility = View.INVISIBLE
//    }else{
//        holder.line1.visibility = View.VISIBLE
//        holder.line2.visibility = View.VISIBLE
//    }
//
//    if(isSelected){
//        holder.circleView.setImageDrawable(ContextCompat.getDrawable(context,R.drawable.ic_check_png))
//        holder.circleView.background = ContextCompat.getDrawable(context,R.drawable.filled_circle_color)
//    }else{
//        holder.circleView.setImageDrawable(null)
//        holder.circleView.background = ContextCompat.getDrawable(context,R.drawable.empty_circl)
//    }
//}
