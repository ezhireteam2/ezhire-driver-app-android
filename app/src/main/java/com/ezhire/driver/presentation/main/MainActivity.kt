package com.ezhire.driver.presentation.main

import android.Manifest
import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.core.content.ContextCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupWithNavController
import com.ezhire.driver.BuildConfig
import com.ezhire.driver.R
import com.ezhire.driver.data.MyForegroundService
import com.ezhire.driver.data.Response
import com.ezhire.driver.data.polyLineData
import com.ezhire.driver.data.service.firebase.Config
import com.ezhire.driver.databinding.ActivityMainBinding
import com.ezhire.driver.databinding.NavHeaderMainBinding
import com.ezhire.driver.domain.*
import com.ezhire.driver.presentation.ChatAC
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.presentation.login.LoginActivity
import com.ezhire.driver.presentation.utils.*
import com.google.android.material.navigation.NavigationView
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.nav_header_main.*
import org.json.JSONObject
import org.koin.android.ext.android.inject


class MainActivity : BaseActivity(),NavigationView.OnNavigationItemSelectedListener {

    companion object {
        lateinit var binding: ActivityMainBinding
        lateinit var navController : NavController

    }


    private lateinit var mLocalBroadcastManager: LocalBroadcastManager

    private lateinit var appBarConfiguration: AppBarConfiguration
    private val viewModel by inject<MainViewModel>()
    val END_SCALE = 0.7f
    private lateinit var contentView: ConstraintLayout

    private lateinit var mSocket : Socket

    private var TIP : Boolean = false

    override fun onStart() {
        super.onStart()

//        if(intent.getBooleanExtra("TIP",false)){
//            TIP = true
//        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val toolbar: Toolbar = findViewById(R.id.toolbar)
//        setSupportActionBar(toolbar)
//

        navController = findNavController(R.id.nav_host_fragment)


        binding.navView.setupWithNavController(navController)

        animateNavigationDrawer(binding.drawerLayout)


        binding.appBarMain.content.also { contentView = it }


        //checkBooking(intent)

        setData()

        getAppVersion()

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this)
        mLocalBroadcastManager.registerReceiver(
            pushNotificationBroadCastReciver,
            IntentFilter(Config.PUSH_NOTIFICATION)
        )


        binding.navView.setNavigationItemSelectedListener(this);

        binding.navView

        //internetObserve()


//        if(checkPermission()){
//
//        }else{
//            val alertBuilder = AlertDialog.Builder(
//                this
//            )
//            alertBuilder.setCancelable(true)
//            alertBuilder.setTitle("Location Permission necessary")
//            alertBuilder.setMessage("ezhire Driver app collects location data to enable [\"live tracking of driver\"], [\"customer can track their driver\"] even when the app is closed or not in use")
//            alertBuilder.setPositiveButton(
//                "Allow Access"
//            ) { dialog, which ->
//                askPermission()
//            }
//            alertBuilder.setNegativeButton(
//                "Not Allow"
//            ) { dialog, which ->
//                finish()
//            }
//
//            val alert = alertBuilder.create()
//            alert.show()
//        }


    }

    var onUpdateChat = Emitter.Listener {

        //Log.e("loc",""+it.get(0))
    }

    private fun getLocationFromSocket() {

        mSocket.on("driver_location", onUpdateChat)
    }

    var count = 100

    private fun sendLocation(){

        val messageJSONO = JSONObject()
        messageJSONO.put("room", "1234")
        messageJSONO.put("location", "24.9210897,67.056386")

        if(mSocket.connected()){
            mSocket.emit("driver_location", messageJSONO);
            //Log.e("Message",messageJSONO.toString())
        }else{
           // Log.e("Socket",""+mSocket.connected())
        }



//        mSocket.emit("1234","24.9210897,67.056386")
        if(count>0){
            Handler(Looper.myLooper()!!).postDelayed(
                {
                    sendLocation()

                }, 3000
            )

            --count
        }

    }

    private fun setData(){
        val headerBinding = NavHeaderMainBinding.bind(binding.navView.getHeaderView(0))

        headerBinding.name.text = AppData.getName()
        headerBinding.email.text = AppData.getEmail()

    }

    private fun internetObserve(){
        val connectionLiveData = ConnectionLiveData(applicationContext)

        connectionLiveData.observe(this, {
            if (it) {
                SnackBarUtils.hideSnackBar()
            } else {
                SnackBarUtils.showProblemSnackBar(binding.root, Constant.NO_INTERNET)
            }
        })
    }


    private fun checkBooking(intent: Intent){


        if(intent.extras != null && !intent.extras?.get("booking_id")?.toString().isNullOrBlank()){
            Log.e("booking", "true")
            Log.e("booking_id", "" + intent.extras?.get("booking_id").toString())
            val bookingID: Long? = intent.extras?.get("booking_id") as Long

            if(bookingID?:0 > 0){
                bookingID.let {

                    viewModel.getMarketPlaceBooking(it ?: 0).observe(this, {
                        when (it) {
                            is Response.Loading -> showProgressIndicator()
                            is Response.Success<*> -> {
                                hideProgressIndicator()
                                val response = it.data as Booking
                                if (response.BookingID > 0 && response.Status == 1) {
                                    val bundle = Bundle()
                                    AppData.booking = response
                                    bundle.putSerializable("booking", response)
                                    bundle.putBoolean("marketplace", true)
                                    navigateToFragment(R.id.action_nav_home_to_nav_booking, bundle)

                                }
                            }
                            is Response.Error -> {
                                hideProgressIndicator()
                                Helper.showBasicAlert(
                                    this,
                                    getString(R.string.alert),
                                    it.exception,
                                    Constant.FAILURE, null
                                )
                            }

                        }

                    })
                }
            }else{
                getVendorBooking()
            }


        }else{
            getVendorBooking()
        }



    }

    private fun getVendorBooking(){
        viewModel.getBooking(AppData.getUserID()).observe(this, {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val response = it.data as Booking


                    if (response.BookingID > 0) {

                        if (response.Status == 9 && response.driver_customer_feedback == 0) {
                            val bundle = Bundle()
                            AppData.booking = response
                            bundle.putSerializable("booking", response)
                            bundle.putBoolean("marketplace", false)
                            navigateToFragment(R.id.action_nav_home_to_nav_feedback, bundle)
                        } else {
                            val bundle = Bundle()
                            AppData.booking = response
                            bundle.putSerializable("booking", response)
                            bundle.putBoolean("marketplace", false)
                            navigateToFragment(R.id.action_nav_home_to_nav_booking, bundle)
                        }


                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this,
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        })
    }

    private fun checkPermission(): Boolean{

        if ((ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED)
            && (ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED)
        ){
            return true
        }else{
            return false
        }
    }

    private fun askPermission(){

        var arrayOfPermission  =arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            arrayOfPermission  =arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION)
        }
        ActivityCompat.requestPermissions(this, arrayOfPermission, 200);
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)



    }

    private fun updateDriverLocationToServer(){

        val requestParam = DriverLocation()
        requestParam.UserID = AppData.getUserID()
        requestParam.Latitude = 24.8616863
        requestParam.Longitude = 67.0345559
        requestParam.Latlng = "${requestParam.Latitude},${requestParam.Longitude}"

        viewModel.updateDriverLocation(requestParam).observe(this, {
            when (it) {
                //is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()
                    val response = it.data as CallResponse
                    if (response.Status.equals("success")) {
                        Log.e("Location Updated", "Sucess")
                    } else {
                        Helper.showBasicAlert(
                            this,
                            "Info",
                            "${response.Status}",
                            Constant.INFO,
                            null
                        )
                        Log.e("Location Updated", "Error")
                    }
                }
                is Response.Error -> {
                    //hideProgressIndicator()
                    Helper.showBasicAlert(
                        this,
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        })
    }

    private fun animateNavigationDrawer(drawerLayout: DrawerLayout) {
        //Add any color or remove it to use the default one!
        //To make it transparent use Color.Transparent in side setScrimColor();
        //drawerLayout.setScrimColor(Color.TRANSPARENT);
        drawerLayout.addDrawerListener(object : DrawerLayout.SimpleDrawerListener() {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

                // Scale the View based on current slide offset
                val diffScaledOffset: Float = slideOffset * (1 - END_SCALE)
                val offsetScale = 1 - diffScaledOffset
                contentView.scaleX = offsetScale
                contentView.scaleY = offsetScale

                // Translate the View, accounting for the scaled width
                val xOffset: Float = drawerView.width * slideOffset
                val xOffsetDiff: Float = contentView.width * diffScaledOffset / 2
                val xTranslation = xOffset - xOffsetDiff
                contentView.translationX = xTranslation
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        val item = menu.findItem(R.id.app_version)
        item.title = BuildConfig.VERSION_NAME
        return true
    }


    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        menu.findItem(R.id.app_version).title = BuildConfig.VERSION_NAME

        return super.onPrepareOptionsMenu(menu)
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        openDrawer()
        when (item.itemId) {

            R.id.nav_booking_history -> {
                if ((navController.currentDestination?.displayName?.contains("nav_booking") == true)) {
                    navigateToFragment(R.id.action_nav_booking_to_nav_booking_history)
                } else {
                    navigateToFragment(R.id.action_nav_home_to_nav_booking_history)
                }
                //navigateToFragment(R.id.action_nav_home_to_nav_booking_history)
            }

            R.id.nav_tips -> {
                if ((navController.currentDestination?.displayName?.contains("nav_booking") == true)) {
                    navigateToFragment(R.id.action_nav_booking_to_nav_tips)
                } else {
                    navigateToFragment(R.id.action_nav_home_to_nav_tips)
                }
            }

            R.id.nav_ncm ->{
                if ((navController.currentDestination?.displayName?.contains("nav_booking") == true)) {
                    navigateToFragment(R.id.action_nav_booking_to_nav_ncm)
                } else {
                    navigateToFragment(R.id.action_nav_home_to_nav_ncm)
                }
            }

            R.id.nav_help -> {
                startActivity(Intent(applicationContext, ChatAC::class.java))
            }

            R.id.nav_logout -> {

                setDriverStatus()


            }

            R.id.nav_home -> {
                navigateToFragment(R.id.nav_home)
            }

            R.id.nav_booking -> {
                if (!(navController.currentDestination?.displayName?.contains("nav_booking") == true)) {
                    navigateToFragment(R.id.nav_booking)
                }

            }
        }

        return  true
    }

    private fun setDriverStatus(){
        val requestParam = DriverActiveStatus()
        requestParam.Active = 0
        requestParam.UserID = AppData.getUserID()

        viewModel.driverStatus(requestParam).observe(this, {
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    hideProgressIndicator()
                    val response = it.data as CallResponse
                    if (response.Status.contains("success")) {
                        stopForgroundService()
                        AppData.clearALLData()
                        polyLineData.clearData(this)
                        startActivity(Intent(applicationContext, LoginActivity::class.java))
                        finish()
                    }
                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this,
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }

        })
    }

    private fun getAppVersion(){
        val param = "DriverAndroidV"+BuildConfig.VERSION_NAME
        Log.e("Req",param)
        viewModel.getAppVersion(param).observe(this,{
            when(it){
                is Response.Success<*> ->{
                    val data = it.data as ArrayList<*>
                    AppData.showUpdate = data.size == 0

                }
            }
        })
    }

    private fun stopForgroundService() {
        //Log.e("Stop Forground", "Stop Service")

        val isServiceRunning = isServiceRunningInForeground(
            this.applicationContext,
            MyForegroundService::class.java
        )

        if(isServiceRunning){
            val stopServiceIntent = Intent(this.applicationContext, MyForegroundService::class.java)
            stopServiceIntent.action = "STOP"
            ContextCompat.startForegroundService(this.applicationContext, stopServiceIntent)
        }
    }

    fun isServiceRunningInForeground(context: Context, serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (service in manager.getRunningServices(Int.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                if (service.foreground) {
                    return true
                }
            }
        }
        return false
    }

    override fun onDestroy() {
        mLocalBroadcastManager.unregisterReceiver(pushNotificationBroadCastReciver)
        super.onDestroy()
    }

    private val pushNotificationBroadCastReciver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            //checkBooking(intent)
        }
    }
}