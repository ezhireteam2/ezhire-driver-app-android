package com.ezhire.driver.presentation

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.ezhire.driver.R
import com.ezhire.driver.databinding.ActivitySplashBinding
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.presentation.login.LoginActivity
import com.ezhire.driver.presentation.main.MainActivity
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Helper


class SplashActivity : BaseActivity() {

    private lateinit var binding : ActivitySplashBinding
    private val TIME_OUT = 3 * 1000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.logo.startAnimation(
            AnimationUtils.loadAnimation(
                this,
                R.anim.zoomin
            )
        )

        Handler(Looper.getMainLooper()).postDelayed({
            changeView(AppData.isLogin())
        }, TIME_OUT.toLong())

        if(intent.extras != null && !intent.extras?.get("msg_type")?.toString().isNullOrBlank()){
            Log.e("promotion","true")
            Log.e("promotion",""+intent.extras?.get("msg_type").toString())

        }else{
            Log.e("No Notification","N/A")
        }
    }



    private fun changeView(login: Boolean) {
        if (login){

            val Intent = Intent(this,MainActivity::class.java)

            if(intent.extras != null && !intent.extras?.get("msg_type")?.toString().isNullOrBlank()){
                //Log.e("booking_id","true")
                Log.e("booking_id",""+intent.extras?.get("booking_id")?.toString()?.toLong())

                if(intent.extras?.get("booking_id") != null){

                    val bookingID = intent.extras?.get("booking_id").toString().toLong()
                    //bookingID = 15144
                    bookingID.let {
                        Intent.putExtra("booking_id",bookingID)
                    }
                }

            }

            startActivity(Intent)
            finish()

            //Helper.changeActivity(this, MainActivity::class.java)
        }else {
            Helper.changeActivity(this, LoginActivity::class.java)
        }
        finish()
    }



}