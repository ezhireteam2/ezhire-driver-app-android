package com.ezhire.driver.presentation.main.ui.tips

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.domain.TipData
import com.ezhire.driver.domain.Tips
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.Helper
import kotlinx.android.synthetic.main.item_tips.view.*
import java.util.*


class TipsAdapter(
    private val context: Context,
    itemList: ArrayList<TipData>,
    private val itemClicked: IItemClicked
) : androidx.recyclerview.widget.RecyclerView.Adapter<TipsAdapter.ViewHolder>() {

    private var itemList = ArrayList<TipData>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_tips_main, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val booking = itemList[position]

        holder.locationTitleTV.text = ""+booking.MonthYear

        var total = 0.0

        for(item in booking.data!!){
                total += item.amount?:0.0
        }

        holder.totalTV.text = "Monthly Total ${booking.data!!.get(0).currency} ${Helper.RoundByString(total,2,2)}"

        holder.tipsRV.layoutManager = LinearLayoutManager(
            holder.tipsRV.context,
            LinearLayoutManager.VERTICAL,
            false
        )
        val adapter = ChildAdapter(booking.data,position)
        holder.tipsRV.adapter = adapter

    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var locationTitleTV: TextView
        var tipsRV: RecyclerView
        var totalTV : TextView


        init {
            locationTitleTV = itemView.findViewById(R.id.locationTitleTV)
            totalTV = itemView.findViewById(R.id.totalTV)
            tipsRV = itemView.findViewById(R.id.tipsRV)

        }
    }
}

class ChildAdapter(private val children : ArrayList<Tips>?,private val parentIndex : Int)
    : RecyclerView.Adapter<ChildAdapter.ViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =  LayoutInflater.from(parent.context)
            .inflate(R.layout.item_tips,parent,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return children?.size?:0
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val child = children?.get(position)
        //holder.imageView.setImageResource(child.image)
        holder.dateTV.text = "Date: "+child?.date_string

        holder.bookingIDTV.text = "Booking ID: "+child?.booking_id
        holder.amountTV.text = "${child?.currency} "+Helper.RoundByString(child?.amount?:0.0,2,2)


    }


    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){

        val dateTV : TextView = itemView.locationTitleTV
        val bookingIDTV : TextView = itemView.locationAddressTV
        val amountTV : TextView = itemView.distanceTV
        //val imageView: ImageView = itemView.child_imageView

    }
}
