package com.ezhire.driver.presentation.main.ui.document

import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.AddOns
import com.ezhire.driver.domain.Document
import com.ezhire.driver.presentation.AddOnsDetailAdapter
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.presentation.main.ui.history.BookingHistoryViewModel
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import kotlinx.android.synthetic.main.activity_document.*
import org.koin.android.ext.android.inject

class DocumentAC : BaseActivity() {

    var documentAdapter: DocumentAdapter? = null
    private var documentlist = ArrayList<Document>()

    private val viewModel by inject<DocumentACViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document)

        documentRV.layoutManager = androidx.recyclerview.widget.GridLayoutManager(this, 2)
        documentRV.addItemDecoration(SpaceItemDecoration(2, 2, 2, 2))

        nav_menu.setOnClickListener {
            finish()
        }

        getDocuments()
    }

    private fun getDocuments(){

        viewModel.getDocuments(AppData.booking.UserID).observe(this,{
            when(it){
                is Response.Loading ->{
                    showProgressIndicator()
                }
                is Response.Success<*> ->{
                    hideProgressIndicator()
                    val dataList = it.data as ArrayList<Document>
                    if(dataList.isNotEmpty()){
                        documentlist =dataList

                        documentRV.adapter = DocumentAdapter(this,documentlist)
                    }
                }

                is Response.Error ->{
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this,
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        })
    }
}