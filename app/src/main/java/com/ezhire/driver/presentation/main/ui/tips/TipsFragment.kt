package com.ezhire.driver.presentation.main.ui.tips

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.databinding.FragmentTipsBinding
import com.ezhire.driver.domain.TipData
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import org.koin.android.ext.android.inject


class TipsFragment : BaseFragment()  {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
        const val LOCATION_PERMISSION_RC = 501
        const val DEFAULT_ZOOM  = 15.0f
        var API_CALL = true

    }

    private val viewModel by inject<TipsViewModel>()
    private lateinit var binding: FragmentTipsBinding

    private lateinit var bookingHistoryList : ArrayList<TipData>

    override fun onDestroy() {
        super.onDestroy()
        //Locus.stopLocationUpdates()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentTipsBinding.inflate(layoutInflater)


        binding.alltipsRV.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false)
        binding.alltipsRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


       binding.navMenu.setOnClickListener {
            this.requireActivity().onBackPressed()
       }

        getTips()


        return binding.root
    }


    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun getTips(){

        viewModel.getTips(AppData.getUserID()).observe(this.requireActivity(),{
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {
                    //hideProgressIndicator()

                    Handler(Looper.getMainLooper()).postDelayed({
                        hideProgressIndicator()
                    }, 1500)

                    val data = it.data as ArrayList<TipData>

                    bookingHistoryList = data as ArrayList<TipData>

                    if(bookingHistoryList.size > 0){

                        binding.alltipsRV.visibility = View.VISIBLE
                        binding.noRecordFoundTV.visibility = View.INVISIBLE
                        binding.alltipsRV.adapter = TipsAdapter(this.requireContext(),bookingHistoryList,itemClicked)

                        var total = 0.0
                        for (item in bookingHistoryList){
                                for(InnerItem in item.data!!){
                                    total += InnerItem.amount?:0.0
                                }
                        }


                        binding.totalTV.text = "Grand Total: ${bookingHistoryList.get(0).data!!.get(0).currency?:""} ${Helper.RoundByString(total,2,2)}"
                        binding.totalTV.visibility = View.VISIBLE

                    }else{
                        binding.alltipsRV.visibility = View.INVISIBLE
                        binding.noRecordFoundTV.visibility = View.VISIBLE
                        binding.totalTV.visibility = View.INVISIBLE
                    }


                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        })
    }

    internal var itemClicked: IItemClicked =
        object : IItemClicked {
            override fun itemClicked(postion: Int) {

//                val intent = Intent(context, BookingHistoryDetailAC::class.java)
//                intent.putExtra("BOOKING",bookingHistoryList!![postion])
//                startActivity(intent)

            }
        }



}




