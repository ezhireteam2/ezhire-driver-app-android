package com.ezhire.driver.presentation.main.ui.ncm

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.ezhire.driver.R
import com.ezhire.driver.domain.BookingDriverStatus
import com.ezhire.driver.domain.BookingHistory
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.main.ui.history.BookingHistoryAdapter
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import java.util.*


class NcmHistoryAdapter(
    private val context: Context,
    itemList: ArrayList<BookingDriverStatus>
) : androidx.recyclerview.widget.RecyclerView.Adapter<NcmHistoryAdapter.ViewHolder>() {

    private var itemList = ArrayList<BookingDriverStatus>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_ncm, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: NcmHistoryAdapter.ViewHolder, position: Int) {

        val ncm = itemList[position]

        holder.vehcileTV.text = ncm.CarPlateNumber

        if(ncm.DriverStatus?:0 == Constant.NCM_OUT){
            holder.statusTV.text = "OUT"
        }else if(ncm.DriverStatus?:0 == Constant.NCM_IN){
            holder.statusTV.text = "IN"
        }else{
            holder.statusTV.text = "N/A"
        }

        holder.dateTimeTV.text = "${ncm.Action_Date_String?:""} ${ncm.Action_Time_String?:""} "

        holder.fuelLevelTV.text = ncm.FuelLevel?:""

        holder.odometerTV.text = ncm.Odomoter.toString()

        holder.itemView.setOnClickListener{

            //itemClicked.itemClicked(position)


        }

    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var vehcileTV: AppCompatTextView = itemView.findViewById(R.id.vehcileTV)
        var statusTV: AppCompatTextView = itemView.findViewById(R.id.statusTV)
        var odometerTV: AppCompatTextView = itemView.findViewById(R.id.odometerTV)
        var fuelLevelTV: AppCompatTextView = itemView.findViewById(R.id.fuelLevelTV)
        var dateTimeTV: AppCompatTextView = itemView.findViewById(R.id.dateTimeTV)

    }
}
