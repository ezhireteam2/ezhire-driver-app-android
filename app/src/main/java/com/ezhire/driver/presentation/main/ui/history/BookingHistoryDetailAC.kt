package com.ezhire.driver.presentation.main.ui.history

import android.app.Dialog
import android.content.Intent
import android.content.res.Configuration
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.recyclerview.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Button
import androidx.recyclerview.widget.LinearLayoutManager
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.AddOns
import com.ezhire.driver.domain.BookingHistory
import com.ezhire.driver.domain.ShiftInfo
import com.ezhire.driver.presentation.AddOnsDetailAdapter
import com.ezhire.driver.presentation.ChatAC
import com.ezhire.driver.presentation.base.BaseActivity
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.main.ui.booking.ShiftStatusAdapter
import com.ezhire.driver.presentation.main.ui.tips.TipsViewModel
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import kotlinx.android.synthetic.main.activity_booking_history_detail_ac.*
import kotlinx.android.synthetic.main.activity_charges_detail_ac.bookingIDTV
import kotlinx.android.synthetic.main.activity_charges_detail_ac.nav_menu
import kotlinx.android.synthetic.main.activity_charges_detail_ac.newReturnDateTV
import kotlinx.android.synthetic.main.activity_charges_detail_ac.okBtn
import kotlinx.android.synthetic.main.activity_charges_detail_ac.previousReturnDateTV
import org.koin.android.ext.android.inject
import java.util.*
import kotlin.collections.ArrayList

class BookingHistoryDetailAC : BaseActivity() {

    //var apiService: ApiInterface? = null

    var AddOnsDetailRV: androidx.recyclerview.widget.RecyclerView? = null
    var summaryAdapter: AddOnsDetailAdapter? = null
    private var SummaryList = ArrayList<AddOns>()

    private val viewModel by inject<BookingHistoryViewModel>()

    var booking = BookingHistory()

    lateinit var shiftStatusAdapter: ShiftStatusAdapter
    var shiftStatusList = ArrayList<ShiftInfo>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val locale = Locale("en")
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(
            config,
            baseContext.resources.displayMetrics
        )

        setContentView(R.layout.activity_booking_history_detail_ac)

        //val applicationClass: ApplicationClass = applicationContext as ApplicationClass
        //apiService = applicationClass.apiInterface

        okBtn.setOnClickListener {
            finish()
        }

        nav_menu.setOnClickListener {
            finish()
        }

        chatIV.setOnClickListener {
            startActivity(Intent(this@BookingHistoryDetailAC,ChatAC::class.java))
        }

        AddOnsDetailRV = findViewById(R.id.addonsDetailsRV)
        AddOnsDetailRV!!.layoutManager =
            androidx.recyclerview.widget.LinearLayoutManager(
                this@BookingHistoryDetailAC,
                RecyclerView.VERTICAL,
                false
            )
        AddOnsDetailRV!!.addItemDecoration(SpaceItemDecoration(0, 0, 2, 0))


        try {
            booking = intent.getSerializableExtra("BOOKING") as BookingHistory

            bookingIDTV.text = "Booking Number: " + booking.id
            newReturnDateTV.text = booking.deliver_date + " " + Helper.formatTime24HrToAmPm(booking.deliver_time_string)
            previousReturnDateTV.text = booking.return_date + " " + Helper.formatTime24HrToAmPm(booking.return_time_string)
            getBookingDetails()
        } catch (e: Exception) {
            Log.e("Ex",e.toString())
        }

        if(booking.rental_type_id == Constant.WITH_DRIVE){
            shiftDetailBtn.visibility = View.VISIBLE
            getShiftDetails()
        }else{
            shiftDetailBtn.visibility = View.INVISIBLE
        }

        shiftDetailBtn.setOnClickListener {
            if(shiftStatusList.size > 0){
                //vibratePhone()
                showShiftDetails()
            }
        }


    }

    private fun showShiftDetails() {

        val alert =
            Dialog(this, android.R.style.Theme_Black_NoTitleBar_Fullscreen)
        alert?.window?.requestFeature(Window.FEATURE_NO_TITLE)
        alert?.setCancelable(true)

        val view: View = LayoutInflater.from(this).inflate(R.layout.dialog_shift_detail, null)


        val okBtn = view.findViewById<Button>(R.id.ok_btn)
        okBtn.setOnClickListener {
            alert.dismiss()
        }
        val shiftRV = view.findViewById<RecyclerView>(R.id.bookingHistoryRV)

        shiftRV.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL,
            false
        )
        shiftRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


        shiftStatusAdapter =
            ShiftStatusAdapter(
                this, shiftStatusList
            )
        shiftRV.adapter = shiftStatusAdapter

        alert?.setContentView(view)
        alert?.window?.setWindowAnimations(R.style.Animation_Design_BottomSheetDialog)
        alert?.show()

        alert?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    internal var itemClicked: IItemClicked =
        object : IItemClicked {
            override fun itemClicked(postion: Int) {

            }
        }

    private fun getShiftDetails(){

            viewModel.getShiftDetails(AppData.getUserID(),booking.id).observe(this,{
                when (it) {
                    is Response.Success<*> -> {
                        //hideProgressIndicator()

                        val dataList = it.data as ArrayList<*>
                        if(dataList.isNotEmpty()){
                            shiftDetailBtn.visibility = View.VISIBLE
                            shiftStatusList = dataList as ArrayList<ShiftInfo>
                            //shiftStatusAdapter.notifyDataSetChanged()
                        }
                    }
                    is Response.Error -> {
                        //hideProgressIndicator()
                        Helper.showBasicAlert(
                            this,
                            getString(R.string.alert),
                            it.exception,
                            Constant.FAILURE, null
                        )
                    }
                }
            })



    }

    private fun getBookingDetails() {

        viewModel.getBookingHistoryDetails(bookingID = booking.id).observe(this,{
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {



                    Handler(Looper.getMainLooper()).postDelayed({
                        hideProgressIndicator()
                    }, 1500)

                    val data = it.data as ArrayList<AddOns>

                    SummaryList = data

                    summaryAdapter = AddOnsDetailAdapter(
                        this@BookingHistoryDetailAC,
                        SummaryList,
                        null,
                        false,
                        booking.currency,
                        false
                    )
                    AddOnsDetailRV!!.adapter = summaryAdapter


                }
                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this,
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }

            }
        })
//        doAsync {
//            val call = apiService!!.getBookingCharges(booking.id).execute()
//
//            if (call.isSuccessful) {
//
//                uiThread {
//                    SummaryList = call.body() as ArrayList<AddOns>
//
//                    summaryAdapter = AddOnsDetailAdapter(
//                        this@BookingHistoryDetailAC,
//                        SummaryList,
//                        null,
//                        false,
//                        booking.currency,
//                        false
//                    )
//                    AddOnsDetailRV!!.adapter = summaryAdapter
//                }
//            }else{
//
//                val appClass = applicationContext as ApplicationClass
//                appClass.logError(
//                    call.errorBody()!!.string()+" | "+call.message(),
//                    "Booking History - Get BOOKING DETAIL - Api ("+ Constants.GET_BOOKING_DETAIL+")",
//                    ""
//                )
//
//            }
//        }
    }


}
