package com.ezhire.driver.presentation.utils

import android.content.Context
import android.content.SharedPreferences
import com.ezhire.driver.domain.Booking
import com.ezhire.driver.presentation.app.Application
import com.google.gson.GsonBuilder

object AppData {

    //Shared Preference field used to save and retrieve JSON string
    lateinit var preferences: SharedPreferences
    private val AppToken = "app_token"
    private val userID = "user_id"
    private val login = "user_login"
    private val email = "user_email"
    private val name = "user_name"
    private val phone = "phone"
    private val FCM_TOKEN = "fcm_token"
    val POLY_LINE = "RecordedPoints"
    lateinit var booking : Booking

    private val notifyCustomer = "notifyCustomer"


    var showUpdate : Boolean = false

    fun with(application: Application) {
        preferences = application.getSharedPreferences(
            Constant.PREFERENCES_FILE_NAME, Context.MODE_PRIVATE
        )
    }

    fun setFCMToken(token: String?){
        val editor = preferences.edit()
        editor.putString(FCM_TOKEN, token)
        editor.apply()
    }

    fun getFCMToken(): String? {
        return preferences.getString(FCM_TOKEN, "")
    }

    fun getAppToken(): String? {
        return preferences.getString(AppToken, "")
    }

    fun setAppToken(token: String?) {
        val editor = preferences.edit()
        editor.putString(AppToken, token)
        editor.apply()
    }


    fun getUserID(): Long {
        return preferences.getLong(userID, 0)
    }

    fun setUserID(token: Long) {
        val editor = preferences.edit()
        editor.putLong(userID, token)
        editor.apply()
    }




    fun isLogin(): Boolean {
        return preferences.getBoolean(login, false)
    }

    fun setLogin(ischeck: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(login, ischeck)
        editor.apply()
    }


    fun getName(): String? {
        return preferences.getString(name, "")
    }

    fun setName(ischeck: String) {
        val editor = preferences.edit()
        editor.putString(name, ischeck)
        editor.apply()
    }


    fun getEmail(): String? {
        return preferences.getString(email, "")
    }

    fun setEmail(ischeck: String) {
        val editor = preferences.edit()
        editor.putString(email, ischeck)
        editor.apply()
    }

    fun getPhone(): String? {
        return preferences.getString(phone, "")
    }

    fun getNotifyCustomerStatus(): Boolean{
        return preferences.getBoolean(notifyCustomer,true)
    }

    fun setNotifyCustomer(value : Boolean){
        preferences.edit().putBoolean(notifyCustomer,value).apply()
    }

    fun setPhone(phoneNumber: String) {
        val editor = preferences.edit()
        editor.putString(phone, phoneNumber)
        editor.apply()
    }

    fun setTime(time : Long){
        preferences.edit().putLong("TIME",time).apply()
    }

    fun getTime() : Long{
        return preferences.getLong("TIME",0L)
    }



    fun <T> put(`object`: T, key: String) {
        //Convert object to JSON String.
        val jsonString = GsonBuilder().create().toJson(`object`)
        //Save that String in SharedPreferences
        preferences.edit().putString(key, jsonString).apply()
    }


    inline fun <reified T> get(key: String): T? {
        // We read JSON String which was saved.
        val value = preferences.getString(key, null)
        // JSON String was found which means object can be read.
        // We convert this JSON String to model object. Parameter "c" (of
        // type Class < T >" is used to cast.
        return GsonBuilder().create().fromJson(value, T::class.java)
    }

    private fun setPolylineData(){

    }

    private fun clearRoute(){

    }

    fun clearALLData() {
        setLogin(false)
        setEmail("")
        setName("")
        setUserID(0)
        setAppToken("")
        setNotifyCustomer(true)
        clearRoute()
        preferences.edit().clear().apply()
        //preferences.edit().clear().apply()
    }

    fun clearBookingData(){

        preferences.edit().putBoolean("FIRST_POINT",true).apply()
        preferences.edit().putBoolean("SECOND_POINT",true).apply()
    }

}
