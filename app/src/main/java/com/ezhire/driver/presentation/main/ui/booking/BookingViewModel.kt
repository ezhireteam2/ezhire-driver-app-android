package com.ezhire.driver.presentation.main.ui.booking

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ezhire.driver.data.Repository
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.*
import com.ezhire.driver.presentation.base.BaseViewModel
import com.ezhire.driver.presentation.utils.Constant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class BookingViewModel(val repo : Repository) : BaseViewModel() {

    fun getBookingByBookingID(bookingID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getBookingByBookingID(bookingID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getBooking(userID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getBookings(userID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun updateBookingStatus(request : BookingStatusCode): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateBookingStatus(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun acceptBooking(request : BookingAccept): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.acceptBooking(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun rejectBooking(request : BookingAccept): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.rejectBooking(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun saveCarKM(request : ShiftInfo): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.saveCarKM(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun updateBooking(request : UpdateBooking): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateBooking(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun addCash(request : SaveCash): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.addCash(request)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getShiftInfo(driverID: Long, booking_id : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getLastShiftInfo(driverID,booking_id)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getAmountToCollect(booking_id : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getAmountToCollect(booking_id)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun notifyCustomer(userID : DriverLocation): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.notifyCustomer(userID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun getAllShiftInfo(driverID: Long, booking_id : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getAllShiftInfo(driverID,booking_id)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

    fun updateDriverLocation(param : DriverLocation): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.updateDriverLocation(param)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }
}