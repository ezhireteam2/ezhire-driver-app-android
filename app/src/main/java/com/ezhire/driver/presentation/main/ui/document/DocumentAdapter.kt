package com.ezhire.driver.presentation.main.ui.document

import android.content.Context
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.ezhire.driver.R
import com.ezhire.driver.data.network.HttpEndPoints
import com.ezhire.driver.domain.Document
import com.ezhire.driver.presentation.base.IItemClicked
import android.content.Intent
import android.net.Uri


class DocumentAdapter(
    private val context: Context,
    itemList: ArrayList<Document>
) : androidx.recyclerview.widget.RecyclerView.Adapter<DocumentAdapter.ViewHolder>() {

    private var itemList: ArrayList<Document> = ArrayList<Document>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_document, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val document = itemList[position]

        holder.title.text = document.title

        Glide.with(context).load(HttpEndPoints.BASE_URL+"/"+document.documents).placeholder(R.drawable.ic_document).into(holder.image)

        holder.uploadIcon.visibility = View.INVISIBLE

        holder.itemView.setOnClickListener {
            val intent = Intent()
            intent.action = Intent.ACTION_VIEW
            intent.setDataAndType(
                Uri.parse(HttpEndPoints.BASE_URL+"/"+document.documents), "image/*")
            context.startActivity(intent)
        }

    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var title: TextView
        var requiredTV : TextView
        var image: ImageView
        var deleteIcon: ImageView
        var uploadIcon: ImageView

        init {
            requiredTV = itemView.findViewById(R.id.requiredTV)
            title = itemView.findViewById(R.id.title)
            image = itemView.findViewById(R.id.picture1)
            deleteIcon = itemView.findViewById(R.id.picture_delete1)
            uploadIcon = itemView.findViewById(R.id.file_upload1)
        }
    }
}
