package com.ezhire.driver.presentation

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ezhire.driver.R
import com.ezhire.driver.domain.AddOns
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import java.util.*


class AddOnsDetailAdapter(
    private val context: Context,
    itemList: ArrayList<AddOns>,
    private val itemClicked: IItemClicked?,
    var newBooking: Boolean,
    val currency: String,
    val collection: Boolean
) : RecyclerView.Adapter<AddOnsDetailAdapter.ViewHolder>() {

    private var itemList = ArrayList<AddOns>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val inflater = LayoutInflater.from(parent.context)
        val viewHolder = ViewHolder(inflater.inflate(R.layout.item_addone_detail, parent, false))
        return viewHolder
    }

    override fun onBindViewHolder(holder: AddOnsDetailAdapter.ViewHolder, position: Int) {

        val addOns = itemList[position]

        holder.addOnAmount.text = currency + " " + Helper.RoundByString(addOns.TotalCharge, 2, 2)

        if(addOns.ChargeTypeID == Constant.BABY_SEATER || addOns.ChargeTypeID == Constant.GPS ||
            addOns.ChargeTypeID == Constant.BOSTER_SEAT || addOns.ChargeTypeID == Constant.ADD_DRIVER){
            holder.title.text = Helper.RoundByString(addOns.Frequency,0,0) + " x " + addOns.AddonTitle.capitalize()
        }else{
            holder.title.text = addOns.AddonTitle.capitalize()
        }


        if (addOns.ChargeTypeID == Constant.RENT || addOns.ChargeTypeID == Constant.EXTRA_HOUR) {

            holder.title.text = "${addOns.AddonTitle.capitalize()}"

            holder.days.visibility = View.VISIBLE

            holder.days.text = "${addOns.Charge} | ${Helper.RoundByString(addOns.Frequency,0,0)} day(s)"
            if(addOns.Frequency < 1 && addOns.Frequency != 0.0 && !collection){
                holder.days.text = "${addOns.Charge} | ${Helper.RoundByString((addOns.Frequency*10),0,0)} hours"
            }

            if(addOns.ChargeTypeID == Constant.EXTRA_HOUR){
                holder.days.text = "${addOns.Charge} | ${Math.abs(addOns.Frequency)} hours"
            }


        } else {
            holder.days.visibility = View.INVISIBLE
        }


        if(addOns.AddonTitle.contains("Total") ){
            holder.title.setTypeface(holder.title.typeface, Typeface.BOLD)
            holder.addOnAmount.setTypeface(holder.title.typeface, Typeface.BOLD)
        }else{
            holder.title.typeface = Typeface.DEFAULT
            holder.addOnAmount.typeface = Typeface.DEFAULT
        }

        if(addOns.AddonTitle.contains("Discount",true)){
            holder.title.text = "Discount (-)"
            holder.title.setTextColor(ContextCompat.getColor(context,R.color.green_shade))
            holder.addOnAmount.setTextColor(ContextCompat.getColor(context,R.color.green_shade))
        }else{
            holder.title.setTextColor(ContextCompat.getColor(context, R.color.text_dark))
            holder.addOnAmount.setTextColor(ContextCompat.getColor(context,R.color.text_dark))
        }

        if(addOns.TotalCharge == 0.0 && !addOns.showAddonInSummary && newBooking){
            holder.itemView.visibility = View.GONE
            holder.itemView.layoutParams = RecyclerView.LayoutParams(0, 0)
        }else{
            holder.itemView.layoutParams = RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            holder.itemView.visibility = View.VISIBLE

        }


    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var title: TextView
        var addOnAmount: TextView
        var days: TextView

        init {
            title = itemView.findViewById(R.id.title)

            addOnAmount = itemView.findViewById(R.id.amount)
            days = itemView.findViewById(R.id.days)


        }
    }


}
