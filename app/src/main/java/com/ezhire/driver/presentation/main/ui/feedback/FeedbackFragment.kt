package com.ezhire.driver.presentation.main.ui.feedback

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.databinding.FragmentFeedbackBinding
import com.ezhire.driver.domain.DeliveryFeedBack
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import org.koin.android.ext.android.inject


class FeedbackFragment : BaseFragment()  {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
        const val LOCATION_PERMISSION_RC = 501
        const val DEFAULT_ZOOM  = 15.0f
        var API_CALL = true

    }

    private val viewModel by inject<FeedbackViewModel>()
    private lateinit var binding: FragmentFeedbackBinding

    override fun onDestroy() {
        super.onDestroy()
        //Locus.stopLocationUpdates()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentFeedbackBinding.inflate(layoutInflater)


        binding.doneBtn.setOnClickListener {
            if (binding.ratingBar.rating.toInt() == 0) {
                Toast.makeText(this.requireContext(), "Please rate first", Toast.LENGTH_SHORT).show()
            }else{
                customerFeedback()
            }
        }

        return binding.root
    }


    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()

    }

    private fun customerFeedback(){

        val param = DeliveryFeedBack()
        param.Booking_id = AppData.booking.BookingID
        param.user_id = AppData.getUserID()
        param.comments = binding.commentET.text.toString()
        param.rate = binding.ratingBar.rating.toInt()

        viewModel.customerFeedback(param).observe(this.requireActivity(),{
            when (it) {
                is Response.Loading -> showProgressIndicator()
                is Response.Success<*> -> {

                    val runnable = Runnable {
                        navigateToFragment(R.id.action_nav_feedback_to_nav_home)
                    }

                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.sucess),
                        "Feedback submit successfully",
                        Constant.SUCCESS, runnable
                    )
                }

                is Response.Error ->{
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        })
    }

}




