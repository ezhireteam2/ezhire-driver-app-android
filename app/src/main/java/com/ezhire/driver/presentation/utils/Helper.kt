package com.ezhire.driver.presentation.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.location.Location
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.DrawableCompat
import com.ezhire.driver.R
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import java.text.DateFormat
import java.text.NumberFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern


object Helper {

    fun formatedCount(count: Int): String {

        return "($count)"

    }


    fun getAppVersion(context: Context): String? {
        return context.packageManager
            .getPackageInfo(context.packageName, 0).versionName

    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentDate(format : String): String?{

        val dateFormat: DateFormat = SimpleDateFormat(format)
        val cal = Calendar.getInstance()
        return dateFormat.format(cal.time)
    }

    @SuppressLint("SimpleDateFormat")
    fun getCurrentTime(format: String): String?{
        val timeFormat: DateFormat = SimpleDateFormat(format)
        val timecal = Calendar.getInstance()
        return  timeFormat.format(timecal.time)
    }

    fun showBasicAlert(
        context: Context?, title: String?,
        message: String?, type: String, runnable: Runnable?
    ) {
        val factory = LayoutInflater.from(context)
        val deleteDialogView: View = factory.inflate(R.layout.dialog_layout, null)
        val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
        alertDialog.setView(deleteDialogView)
        alertDialog.setCancelable(false)

        if (alertDialog != null && alertDialog.isShowing) {
            alertDialog.dismiss()
        }

        val image = deleteDialogView.findViewById<ImageView>(R.id.dialog_icon_iv)
        val heading = deleteDialogView.findViewById<TextView>(R.id.dialog_heading_tv)
        val text = deleteDialogView.findViewById<TextView>(R.id.dialog_desc_tv)


        when(type){
            Constant.SUCCESS -> {
                image.setImageResource(R.drawable.success)
            }

            Constant.FAILURE -> {
                image.setImageResource(R.drawable.delete_icon)
            }

            Constant.INFO -> {
                image.setImageResource(R.drawable.info)
            }
        }

        heading.text = title
        text.text = message


        deleteDialogView.findViewById<Button>(R.id.ok_btn).setOnClickListener {
            alertDialog.dismiss()

            runnable?.run()
        }

        alertDialog.show()
        alertDialog.window!!.attributes.windowAnimations = R.style.DialogAnimation //style id
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

    }


    fun sum(a: Int, b: Int){

    }

    fun hideKeyboard(activity: Activity) {
        val imm: InputMethodManager =
            activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        //Find the currently focused view, so we can grab the correct window token from it.
        var view = activity.currentFocus
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }


    fun verifyAvailableNetwork(activityContext: Context): Boolean {
        val connectivityManager =
            activityContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }


    fun getDeviceId(context: Context): String? {
        return Settings.Secure.getString(
            context.contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }

    fun validateEmailAddress(str: EditText): Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        val p = Pattern.compile(regex)
        return p.matcher(str.text.trim().toString()).matches()
    }

    fun changeActivity(current: Activity, destination: Class<*>){
        val intent = Intent(current.applicationContext, destination)

        current.startActivity(intent)
    }

    fun formatTime24HrToAmPm(inputTime: String?): String? {
        return try {
            if (inputTime != null && !inputTime.contains("M")) {
                val c = Calendar.getInstance()
                val arr = inputTime.split(":").toTypedArray()
                c[Calendar.HOUR_OF_DAY] = arr[0].toInt()
                c[Calendar.MINUTE] = arr[1].toInt()
                SimpleDateFormat("hh:mm aa", Locale.US).format(c.time)
            } else {
                inputTime
            }
        } catch (e: Exception) {
            ""
        }
    }

    fun dateFormatter(inputDate: String?, inputFormat: String?, outputFormat: String?): String? {
        var parsedDate = ""
        try {
            var format = SimpleDateFormat(inputFormat, Locale.ENGLISH)
            val newDate = format.parse(inputDate)
            format = SimpleDateFormat(outputFormat, Locale.ENGLISH)
            parsedDate = format.format(newDate)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return parsedDate
    }



    /**
     * Returns the `location` object as a human readable string.
     * @param location  The [Location].
     */
    fun getLocationText(location: Location?): String? {
        return if (location == null) "Unknown location" else "(" + location.getLatitude()
            .toString() + ", " + location.getLongitude().toString() + ")"
    }

    fun getLocationTitle(context: Context): String? {
        return context.getString(
            R.string.location_updated,
            DateFormat.getDateTimeInstance().format(Date())
        )
    }

    fun getAuthorizationHeader(): String {
        var Token: String = AppData.getAppToken()?:""
        if (Token.length > 0) {
            Token = "Token $Token"
        }
        Log.e("Auth", "" + Token);
        return Token
    }

    fun RoundByString(dblVal: Double, intRoundBy: Int, intDecimalPointToShow: Int): String? {
        var intRoundBy = intRoundBy
        var strValue = NumberFormat.getNumberInstance(Locale.US).format(dblVal).replace(",", "")
        //String strValue = String.valueOf(dblVal);
        var strDecimalValue: String
        if (strValue.indexOf(".") >= 0) {
            strDecimalValue = strValue.substring(strValue.indexOf("."))
            strValue = strValue.substring(0, strValue.indexOf("."))
        } else {
            strDecimalValue = ""
        }


        //Removing Point From Decimal Value
        if (strDecimalValue.length > 0) {
            strDecimalValue = strDecimalValue.substring(1)
        }

        //When RoundBy is smaller than decimalpointtoshow then use decimalpointtoshow value
        if (intRoundBy > intDecimalPointToShow) intRoundBy = intDecimalPointToShow

        //Checking Decimal Value Length Greater than RoundBy number
        if (strDecimalValue.length > intRoundBy && intRoundBy >= 0) {
            val intLastDigit = strDecimalValue.substring(intRoundBy, intRoundBy + 1).toInt()
            strDecimalValue = strDecimalValue.substring(0, intRoundBy)
            if (strDecimalValue == "") {
                strDecimalValue = "0"
            }
            if (intLastDigit >= 5) {
                var lengthCounter = 0
                if (strDecimalValue.startsWith("0")) {
                    strDecimalValue = "1$strDecimalValue"
                    lengthCounter = -1
                }
                if ((strDecimalValue.toInt() + 1).toString().length + lengthCounter > intRoundBy) {
                    strValue = (strValue.toLong() + 1).toString()
                    strDecimalValue = ""
                    strDecimalValue = PadRight(
                        strDecimalValue,
                        "0",
                        intDecimalPointToShow
                    )
                } else {
                    strDecimalValue = (strDecimalValue.toLong() + 1).toString()
                    if (lengthCounter == -1) {
                        strDecimalValue = strDecimalValue.substring(1)
                    }
                }
            }
        }
        strDecimalValue =
            if (strDecimalValue.length > intDecimalPointToShow) strDecimalValue.substring(
                0,
                intDecimalPointToShow
            ) else PadRight(
                strDecimalValue,
                "0",
                intDecimalPointToShow
            )
        if (intDecimalPointToShow > 0) strValue = "$strValue.$strDecimalValue"
        return strValue
    }

    private fun PadRight(strValue: String, strPadChar: String, intLength: Int): String {
        var strValue = strValue
        if (intLength > strValue.length) {
            val intTotalLen = intLength - strValue.length
            for (x in 0 until intTotalLen) {
                strValue += strPadChar
            }
        }
        return strValue
    }

    /**
     * Demonstrates converting a [Drawable] to a [BitmapDescriptor],
     * for use as a marker icon.
     */
    fun vectorToBitmap(id: Drawable?, @ColorInt color: Int?): BitmapDescriptor? {
        val vectorDrawable = id
        val bitmap = Bitmap.createBitmap(
            vectorDrawable!!.intrinsicWidth,
            vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
        DrawableCompat.setTint(vectorDrawable, color!!)
        vectorDrawable.draw(canvas)
        return BitmapDescriptorFactory.fromBitmap(bitmap)
    }

    fun appUpdateAlert(context: Context){

        val runnable = Runnable {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("market://details?id=${context.packageName}")
            context.startActivity(intent)
        }

        Handler(Looper.getMainLooper()).postDelayed({
            if(AppData.showUpdate){
                Helper.showBasicAlert(context,"Update Available","A new version of eZhire App is available. Please update the app from playstore",Constant.INFO,runnable)
            }
        },10000)

    }
}