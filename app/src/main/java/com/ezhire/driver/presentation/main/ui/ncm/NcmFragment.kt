package com.ezhire.driver.presentation.main.ui.ncm

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.recyclerview.widget.RecyclerView
import com.birjuvachhani.locus.Locus
import com.ezhire.driver.R
import com.ezhire.driver.data.Response
import com.ezhire.driver.databinding.FragmentNcmBinding
import com.ezhire.driver.domain.BookingDriverStatus
import com.ezhire.driver.domain.BookingResponse
import com.ezhire.driver.domain.NCMStatusResponse
import com.ezhire.driver.presentation.base.BaseFragment
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper
import com.ezhire.driver.presentation.utils.SpaceItemDecoration
import org.koin.android.ext.android.inject
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class NcmFragment : BaseFragment() {

    companion object {
        const val REQUEST_CHECK_SETTINGS = 43
        const val LOCATION_PERMISSION_RC = 501
        const val DEFAULT_ZOOM = 15.0f
        var API_CALL = true

    }

    private val viewModel by inject<NcmViewModel>()
    private lateinit var binding: FragmentNcmBinding

    var actionDateString: String? = null
    var actionTimeString: String? = null

    private lateinit var ncmDataList: ArrayList<BookingDriverStatus>

    val request = BookingDriverStatus()

    var listIsVisible = false
    // private lateinit var bookingHistoryList : ArrayList<TipData>

    private var fuelLevel = arrayOf("1/8","2/8","3/8","4/8","5/8","6/8","7/8","8/8")

    override fun onDestroy() {
        super.onDestroy()
        //Locus.stopLocationUpdates()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Locus.getCurrentLocation(this.requireContext()) {
            it.location.let {
                request.Latitude = it?.latitude.toString()
                request.Longitude = it?.longitude.toString()
                request.LatLng = "${request.Latitude},${request.Longitude}"
            }
        }

    }

    override fun onStart() {
        super.onStart()

    }

    @SuppressLint("SimpleDateFormat")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentNcmBinding.inflate(layoutInflater)


        binding.navMenu.setOnClickListener {
            Helper.hideKeyboard(this.requireActivity())
            this.requireActivity().onBackPressed()
        }

        val timeFormat: DateFormat = SimpleDateFormat("HH:mm")
        val timecal = Calendar.getInstance()

        val dateFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy")
        val cal = Calendar.getInstance()

        actionDateString = dateFormat.format(cal.time)
        actionTimeString = timeFormat.format(timecal.time)

        val aa: ArrayAdapter<*> =
            ArrayAdapter<Any?>(this.requireContext(), android.R.layout.simple_spinner_dropdown_item, fuelLevel)

        binding.fuelLevelET.adapter = aa



        binding.dateTimeET.setText("$actionDateString $actionTimeString")

        binding.statusSwitch.setOnCheckedChangeListener { compoundButton, b ->
            if (b) {
                binding.statusSwitch.text = "NCM OUT"
            } else {
                binding.statusSwitch.text = "NCM IN"
            }
        }

        binding.addIV.setOnClickListener {
            Helper.hideKeyboard(this.requireActivity())
            if(listIsVisible){
                listIsVisible = false
                getNCMStatus()
                binding.ncmListView.visibility = View.VISIBLE
                binding.cashCollectView.visibility = View.GONE
                binding.addIV.setImageDrawable(ContextCompat.getDrawable(this.requireContext(),R.drawable.ic_add))
            }else{
                listIsVisible = true

                binding.ncmListView.visibility = View.GONE
                binding.cashCollectView.visibility = View.VISIBLE
                binding.addIV.setImageDrawable(ContextCompat.getDrawable(this.requireContext(),R.drawable.ic_baseline_list_alt_24))
            }
        }


        binding.okBtn.setOnClickListener {
            Helper.hideKeyboard(this.requireActivity())
            saveNCM()
        }

        binding.okBtn.isEnabled = false
        binding.okBtn.alpha = 0.5f

        binding.ncmRV.layoutManager = androidx.recyclerview.widget.LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        )
        binding.ncmRV.addItemDecoration(SpaceItemDecoration(0, 0, 0, 2))


        getNCMStatus()

        return binding.root
    }

    private fun saveNCM() {

        val vehcile = binding.vehcileET.text.toString()
        val odometerET = binding.odometerET.text.toString()
        val fuelLevelET = binding.fuelLevelET.selectedItem as String

        if (vehcile.isEmpty()) {
            binding.vehcileET.error = "Enter vehicle number"
            return
        }

        if (odometerET.isEmpty()) {
            binding.odometerET.error = "Enter odometer"
            return
        }

//        if (fuelLevelET.isEmpty()) {
//            //binding.fuelLevelET.error = "Enter fuel level"
//            return
//        }

        request.Action_Date_String = actionDateString
        request.Action_Time_String = actionTimeString

        request.Odomoter = odometerET.toLong()
        request.FuelLevel = fuelLevelET.toString()
        request.CarPlateNumber = vehcile
        request.DriverID = AppData.getUserID()

        request.IsNCM = true

        if (binding.statusSwitch.isChecked) {
            request.CarStatus = Constant.NCM_OUT
            request.DriverStatus = Constant.NCM_OUT
        } else {
            request.CarStatus = Constant.NCM_IN
            request.DriverStatus = Constant.NCM_IN
        }


        viewModel.updateNCM(request).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> {
                    showProgressIndicator()
                }

                is Response.Success<*> -> {

                    hideProgressIndicator()

                    val response = it.data as BookingResponse

                    val runnable = Runnable {
                        clearForm();
                        //getNCMStatus()
                    }

                    if (response.status) {
                        Helper.hideKeyboard(this.requireActivity())
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Success",
                            "" + response.message,
                            Constant.SUCCESS,
                            runnable
                        )
                    } else {
                        Helper.showBasicAlert(
                            this.requireContext(),
                            "Fail",
                            "" + response.message,
                            Constant.FAILURE,
                            runnable
                        )
                    }

                }

                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        }

    }

    private fun getNCMStatus() {
        viewModel.getNCMStatus(AppData.getUserID()).observe(this.requireActivity()) {
            when (it) {
                is Response.Loading -> {
                    showProgressIndicator()
                }

                is Response.Success<*> -> {

                    hideProgressIndicator()

                    val response = it.data as NCMStatusResponse

                    if (response.status) {

                        ncmDataList = response.bookingDriverStatus ?: ArrayList()

                        binding.ncmRV.adapter =
                            NcmHistoryAdapter(this.requireContext(), ncmDataList)

                        if (ncmDataList.size > 0) {
                            binding.ncmRV.visibility = View.VISIBLE
                        }


                        binding.statusSwitch.isChecked =
                            response.bookingDriverStatus?.get(0)?.DriverStatus != Constant.NCM_OUT

                        if (!binding.statusSwitch.isChecked) {
                            binding.vehcileET.setText("" + response.bookingDriverStatus?.get(0)?.CarPlateNumber)
                            binding.statusSwitch.isEnabled = false
                            binding.statusSwitch.isClickable = false

                        } else {
                            binding.statusSwitch.isEnabled = false
                            binding.statusSwitch.isClickable = false
                        }
                    } else {
                        binding.statusSwitch.isEnabled = false
                        binding.statusSwitch.isClickable = false
                        binding.ncmRV.visibility = View.GONE
                        binding.noRecordFoundTV.visibility = View.VISIBLE
                    }

                    binding.okBtn.isEnabled = true
                    binding.okBtn.alpha = 1.0f

                }

                is Response.Error -> {
                    hideProgressIndicator()
                    Helper.showBasicAlert(
                        this.requireContext(),
                        getString(R.string.alert),
                        it.exception,
                        Constant.FAILURE, null
                    )
                }
            }
        }
    }

    private fun clearForm() {
        binding.odometerET.setText("")
        //binding.fuelLevelET.setText("")
        binding.vehcileET.setText("")
        binding.addIV.performClick()
    }


    override fun onPause() {
        super.onPause()
    }

    override fun onResume() {
        super.onResume()

    }


}




