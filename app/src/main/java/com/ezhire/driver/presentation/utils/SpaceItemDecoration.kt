package com.ezhire.driver.presentation.utils

import android.content.Context
import android.graphics.Rect
import androidx.recyclerview.widget.RecyclerView
import android.view.View

/**
 * Created by yasir on 11/9/2016.
 */

class SpaceItemDecoration(
    private val left: Int,
    private val right: Int,
    private val top: Int,
    private val bottom: Int
) : androidx.recyclerview.widget.RecyclerView.ItemDecoration() {
    internal var context: Context? = null

    override fun getItemOffsets(
        outRect: Rect, view: View,
        parent: androidx.recyclerview.widget.RecyclerView, state: androidx.recyclerview.widget.RecyclerView.State
    ) {
        outRect.top = top
        outRect.left = left
        outRect.right = right
        outRect.bottom = bottom


        /*if (parent.getChildLayoutPosition(view) == 1) {
            outRect.top = space;
        } else {
            outRect.top = 0;
        }*/
    }
}
