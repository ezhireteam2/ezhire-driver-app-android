package com.ezhire.driver.presentation.base


import android.os.Bundle
import androidx.fragment.app.Fragment
import com.ezhire.driver.presentation.progress.ProgressIndicator


open class BaseFragment : Fragment(), BaseView, ProgressIndicator {

    override fun navigateToFragment(id: Int, args: Bundle?) {
        if (activity != null) (activity as BaseActivity).navigateToFragment(id,args)
    }

    override fun isInternetAvailable(): Boolean {
        TODO("Not yet implemented")
    }

    override fun hideKeyboard() {

        if (activity != null) (activity as BaseActivity).hideKeyboard()

    }

    fun permissionRecieved() : Boolean {

       return true

    }

    override fun callDialog(type: String) {
        TODO("Not yet implemented")
    }

    override fun openDrawer() {
        if (activity != null) (activity as BaseActivity).openDrawer()
    }

    override fun showProgressIndicator() {
        if (activity != null) (activity as BaseActivity).showProgressIndicator()

    }

    override fun hideProgressIndicator() {
        if (activity != null) (activity as BaseActivity).hideProgressIndicator()
    }



}
