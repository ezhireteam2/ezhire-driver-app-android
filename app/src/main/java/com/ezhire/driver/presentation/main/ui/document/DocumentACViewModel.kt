package com.ezhire.driver.presentation.main.ui.document

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ezhire.driver.data.Repository
import com.ezhire.driver.data.Response
import com.ezhire.driver.domain.DeliveryFeedBack
import com.ezhire.driver.presentation.base.BaseViewModel
import com.ezhire.driver.presentation.utils.Constant
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class DocumentACViewModel(val repo : Repository) : BaseViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    fun getDocuments(userID : Long): MutableLiveData<Response> {
        val data = MutableLiveData<Response>()
        data.postValue(Response.Loading)
        viewModelScope.launch {
            try {
                if (isInternetAvailable()){
                    val response = repo.getDocument(userID)

                    withContext(Dispatchers.Main) {
                        data.postValue(Response.Success(response))
                    }

                }else {
                    data.postValue(Response.Error(Constant.NO_INTERNET))
                }
            }catch (ex : Exception){
                withContext(Dispatchers.Main) {
                    data.postValue(Response.Error(getException(ex)))
                }
            }
        }

        return data
    }

}