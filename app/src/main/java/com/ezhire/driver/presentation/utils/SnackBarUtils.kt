package com.ezhire.driver.presentation.utils

import androidx.drawerlayout.widget.DrawerLayout
import com.ezhire.driver.R
import com.google.android.material.snackbar.Snackbar


object SnackBarUtils {


    private lateinit var mSnackBar: Snackbar


    fun hideSnackBar() {
        if (this::mSnackBar.isInitialized) mSnackBar.dismiss()
    }

    fun showProblemSnackBar(activity: DrawerLayout, message: String?) {
        mSnackBar = Snackbar.make(
            activity,
            message!!, Snackbar.LENGTH_INDEFINITE
        )

        mSnackBar.show()
    }
}