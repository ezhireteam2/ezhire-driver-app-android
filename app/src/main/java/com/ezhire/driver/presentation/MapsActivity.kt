package com.ezhire.driver.presentation

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.birjuvachhani.locus.Locus
import com.directions.route.*
import com.ezhire.driver.R
import com.ezhire.driver.data.polyLineData
import com.ezhire.driver.data.service.firebase.Config
import com.ezhire.driver.domain.Booking
import com.ezhire.driver.presentation.main.ui.booking.animateMarker
import com.ezhire.driver.presentation.utils.AppData
import com.ezhire.driver.presentation.utils.Constant
import com.ezhire.driver.presentation.utils.Helper

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import com.google.maps.android.SphericalUtil
import com.maps.route.extensions.getColor
import kotlinx.android.synthetic.main.activity_maps.*
import java.math.RoundingMode
import java.text.DecimalFormat
import java.util.ArrayList
import kotlin.math.ceil

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, RoutingListener, GoogleMap.CancelableCallback {

    private lateinit var map: GoogleMap

    var driverLoc: Location? = null

    lateinit var booking : Booking

    var driverMarker: Marker? = null
    var deliveryLocationMarker: Marker? = null
    private var line: Polyline? = null

    internal lateinit var mLocalBroadcastManager: LocalBroadcastManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        booking = intent.getSerializableExtra("BOOKING") as Booking

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this)
        mLocalBroadcastManager.registerReceiver(
            pushNotificationBroadCastReciver,
            IntentFilter(Config.LOCATION_UPDATE)
        )

        closeBtn.setOnClickListener {
            finish()
        }

    }

    private val pushNotificationBroadCastReciver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val newLocation: Location? = intent.getParcelableExtra<Location>("location")
            Log.e("NewLocation","${newLocation?.latitude},${newLocation?.longitude}")
            newLocation?.let { drawPath(it) }



        }
    }

    override fun onDestroy() {
        mLocalBroadcastManager.unregisterReceiver(pushNotificationBroadCastReciver)
        super.onDestroy()

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap

        if (isPathSavedInDevice()) {
            drawExisitingPath()
        }
        //startLocationUpdate()
    }

    private fun isPathSavedInDevice(): Boolean {

        return (polyLineData.getData(this).size > 0)
    }

    private fun drawExisitingPath() {

        Log.e("Exisiting Path", "TRUE")
        val polyLine = polyLineData.getData(this@MapsActivity)
        if (polyLine.size > 0) {

            val driverLocation = polyLine.get(0).points.get(0)


            for (item in polyLine){
                item.color(ContextCompat.getColor(this,R.color.green_shade))
            }
            //polyLine.get(0).color(ContextCompat.getColor(this.requireContext(),R.color.green_shade))

            map?.moveCamera(CameraUpdateFactory.newLatLngZoom(driverLocation, 18f))

            map?.run {

                if (driverMarker != null) {


                    driverLoc?.let { animateMarker(it, driverMarker) }

                } else {
//
                    val icon: BitmapDescriptor =
                        BitmapDescriptorFactory.fromResource(R.drawable.car_marker)
                    driverMarker = map?.addMarker(
                        MarkerOptions().position(driverLocation).icon(
                            icon
                        ).anchor(0.5f, 0.5f)
                    )
                }

                if (deliveryLocationMarker != null) {
                    deliveryLocationMarker?.position = LatLng(
                        booking.DeliveryLat,
                        booking.DeliveryLong
                    )
                } else {
                    deliveryLocationMarker = map?.addMarker(
                        MarkerOptions().position(
                            LatLng(
                                booking.DeliveryLat,
                                booking.DeliveryLong
                            )
                        ).icon(Helper.vectorToBitmap(ContextCompat.getDrawable(this@MapsActivity,R.drawable.ic_location_pin),resources.getColor(R.color.dark_shade)
                        )))
                }

            }


            if (line != null) {
                line?.remove();
            }

            line = map?.addPolyline(polyLine.get(0))

            val routeInfo : Route? = AppData.get<Route>("ROUTE_INFO")
            if(routeInfo != null){
                routeInfo.distanceText
                //selfPickupTV.text = routeInfo.durationText + " | " + routeInfo.distanceText + " away"
            }

        }
    }

    private fun startLocationUpdate(){

        Locus.getCurrentLocation(this) { it ->
            it.location?.let {
                Log.e("Current loc", "${it.latitude}," + it.longitude)
                val source = LatLng(it.latitude, it.longitude)
                map?.moveCamera(CameraUpdateFactory.newLatLngZoom(source, 18f))
                drawPath(it)
            }
            it.error?.let {
                Log.e("Current loc", "err: ${it.message} " + it.localizedMessage)
            }
        }

    }

    private fun drawPath(currentLoc: Location) {


        val source = LatLng(currentLoc.latitude, currentLoc.longitude)
        driverLoc = currentLoc
        val destination = LatLng(booking.DeliveryLat, booking.DeliveryLong)

        if (booking.BookingID > 0 && booking.Status != Constant.RENTAL_STARTED) {

            val startPoint = currentLoc

            val endPoint = Location("locationA")
            endPoint.latitude = booking.DeliveryLat
            endPoint.longitude = booking.DeliveryLong


            if (line != null && line?.points?.size ?: 0 > 0) {
                val tolerance = 50.0 // meters
                val isLocationOnPath =
                    PolyUtil.isLocationOnPath(source, line?.points, true, tolerance)

                if (isLocationOnPath) {

                    val distance = endPoint.distanceTo(startPoint).toDouble()



                    //Log.e("Driver","Same route")

                    currentLoc?.let {

                        map.animateCamera(CameraUpdateFactory.newCameraPosition(CameraPosition(source,map.cameraPosition.zoom,map.cameraPosition.tilt,currentLoc.bearing)),
                            2000, this)
                        animateMarker(currentLoc, driverMarker)

                        //map.animateCamera(CameraUpdateFactory.newLatLng(source))


                        val df = DecimalFormat("#.####")
                        df.roundingMode = RoundingMode.CEILING

                        var index = 0
                        for ((position,item) in line?.points!!.withIndex()){
                            val pathLat = df.format(item.latitude)
                            val pathLong = df.format(item.longitude)
                            val currentLaent = df.format(currentLoc.latitude)
                            val currentLong = df.format(currentLoc.longitude)
                            if (currentLaent == pathLat && currentLong == pathLong){
                                Log.e("Same","Point same")
                                index = position
                                break  //Breaking the loop when the index found
                            }
                        }
                        //Creating new path from the current location to the destination
                        val listPoints: List<LatLng> = line?.points!!.subList(index,(line?.points?.size!!))

                        val distance = SphericalUtil.computeLength(listPoints)

                        val redrawPath = distanceFormula((distance.toLong()))


                        //binding.selfPickupTV.text = "${Helper.RoundByString((distance/1000),2,2)}KM away "

                        val options =
                            PolylineOptions().width(5f).color(getColor(R.color.green_shade)).geodesic(
                                true
                            )
                        val iterator = listPoints.iterator()
                        while (iterator.hasNext()) {
                            val data = iterator.next()

                            options.add(data)
                        }


                        line?.remove()
                        line = map?.addPolyline(options)

                        if(redrawPath){
                            line?.remove()
                            line == null
                        }

                    }
                } else {

                    Log.e("Google", "Routing Call")
                    val routing = Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(
                            this
                        ).waypoints(
                            source,
                            destination
                        ).key(getString(R.string.google_api_key)).build()

                    routing.execute()
                }
            } else {
                Log.e("Google", "Routing Call")
                firstTimeLoading = true
                val routing =
                    Routing.Builder().travelMode(AbstractRouting.TravelMode.DRIVING).withListener(
                        this
                    ).waypoints(
                        source,
                        destination
                    ).key(getString(R.string.google_api_key)).build()

                routing.execute()
            }


        } else {

//            binding.imageView7.visibility = View.GONE
//            binding.selfPickupTV.visibility = View.GONE
//            binding.navigateBtn.visibility = View.GONE
//            binding.contactVendor.visibility = View.GONE
//            binding.divider3.visibility = View.GONE
//            binding.fullScreenView.visibility = View.GONE

            map?.animateCamera(CameraUpdateFactory.newLatLngZoom(source, 15f))

            if (driverMarker != null) {
                //driverMarker?.position = driverLoc

                driverLoc?.let { animateMarker(it, driverMarker) }


            } else {
//                val height = 45
//                val width = 65
//                val bitmapdraw = resources.getDrawable(R.drawable.carmarker2) as BitmapDrawable
//                val b = bitmapdraw.bitmap
//                val smallMarker = Bitmap.createScaledBitmap(b, width, height, false)
                val icon: BitmapDescriptor =
                    BitmapDescriptorFactory.fromResource(R.drawable.car_marker)

                val lat = LatLng(driverLoc!!.latitude, driverLoc!!.longitude)
                driverMarker = map?.addMarker(MarkerOptions().position(lat).icon(icon))

            }

        }


    }

    var firstTimeLoading = false

    private fun distanceFormula(currentDistance : Long) : Boolean{


        val initalTotalDistance = AppData.get<Double>("DISTANCE") //M

        if(initalTotalDistance != null && initalTotalDistance > 0){

            var distanceBy3 = initalTotalDistance.div(3)

            if((distanceBy3 % 1) == 0.0){
                distanceBy3 -= -1.0;
            }else{
                distanceBy3 = ceil(distanceBy3)
            }

            val point1 : Long = (distanceBy3.times(2)).toLong()
            val point2 = distanceBy3.toLong()
            Log.e("Value","${currentDistance} ${point1}")

            if(currentDistance == point1 || ((currentDistance >= (point1)) && (currentDistance <= (point1+20)))){
                Log.e("Point 1","${currentDistance} ${point1}")
                return true
            }

            if(currentDistance == point2 || ((currentDistance >= (point2)) && (currentDistance <= (point2+20)))){
                Log.e("Point 2","${currentDistance} ${point2}")
                return true
            }
            return false
        }

        return false

    }

    override fun onRoutingFailure(p0: RouteException?) {

    }

    override fun onRoutingStart() {
    }

    override fun onRoutingSuccess(list: ArrayList<Route>, p1: Int) {



            try {
                //Get all points and plot the polyLine route.
                //Log.e("NewDestination", "Request Receive")
                val listPoints: List<LatLng> = list.get(0).getPoints()
                val options =
                    PolylineOptions().width(5f).color(getColor(R.color.com_facebook_blue)).geodesic(
                        true
                    )
                val iterator = listPoints.iterator()
                while (iterator.hasNext()) {
                    val data = iterator.next()

                    options.add(data)
                }

                val driverLatLng = LatLng(driverLoc!!.latitude, driverLoc!!.longitude)

                val driverPositionByPath = list.get(0).points[0]

                map?.run {

                    if (driverMarker != null) {
                        //driverMarker?.position = driverLoc
                        val loc = Location("")
                        loc.latitude = driverPositionByPath.latitude
                        loc.longitude = driverPositionByPath.longitude
                        loc.bearing = (driverLoc?.bearing ?: 0.0) as Float
                        driverLoc?.let { animateMarker(it, driverMarker) }

                    } else {
//
                        val icon: BitmapDescriptor =
                            BitmapDescriptorFactory.fromResource(R.drawable.car_marker)
                        driverMarker = map?.addMarker(
                            MarkerOptions().position(driverPositionByPath).icon(
                                icon
                            ).anchor(0.5f, 0.5f)
                        )
                    }

                    if (deliveryLocationMarker != null) {
                        deliveryLocationMarker?.position = LatLng(
                            booking.DeliveryLat,
                            booking.DeliveryLong
                        )
                    } else {
                        deliveryLocationMarker = map?.addMarker(
                            MarkerOptions().position(
                                LatLng(
                                    booking.DeliveryLat,
                                    booking.DeliveryLong
                                )
                            ).icon(Helper.vectorToBitmap(ContextCompat.getDrawable(this@MapsActivity,R.drawable.ic_location_pin),resources.getColor(R.color.dark_shade)
                            )))
                    }

                }


                if (line != null) {
                    line?.remove();
                }

                line = map?.addPolyline(options)

                //binding.selfPickupTV.text = list[0].distanceText + " away | "+list[0].durationText

                AppData.put(list[0],"ROUTE_INFO")


                if(firstTimeLoading){

                    val endPoint = Location("locationA")
                    endPoint.latitude = booking.DeliveryLat
                    endPoint.longitude = booking.DeliveryLong

                    val loc = Location("")
                    loc.latitude = driverPositionByPath.latitude
                    loc.longitude = driverPositionByPath.longitude

                    val distance = endPoint.distanceTo(loc).toDouble()

                    AppData.put(distance,"DISTANCE")

                }
//                if (list[0].distanceValue < 400) {
//                    notifyCustomer()
//                }


                val builder = LatLngBounds.Builder()
                val latLng = LatLng(booking.DeliveryLat, booking.DeliveryLong)
                builder.include(latLng)
                builder.include(LatLng(driverLoc!!.latitude, driverLoc!!.longitude))
                val bounds = builder.build()
                //map.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50))


                if(firstTimeLoading){
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(driverLatLng,15f))
                }else{
                    map.animateCamera(CameraUpdateFactory.newLatLng(driverLatLng))
                }

                val data = polyLineData(options)
                polyLineData.writeData(this, data)

            } catch (e: java.lang.Exception) {
                Toast.makeText(
                    this,
                    "EXCEPTION: Cannot parse routing response",
                    Toast.LENGTH_SHORT
                ).show()
            }


    }

    override fun onRoutingCancelled() {

    }

    override fun onFinish() {

    }

    override fun onCancel() {

    }
}