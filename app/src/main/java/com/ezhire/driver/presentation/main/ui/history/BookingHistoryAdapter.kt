package com.ezhire.driver.presentation.main.ui.history

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.ezhire.driver.R
import com.ezhire.driver.domain.BookingHistory
import com.ezhire.driver.presentation.base.IItemClicked
import com.ezhire.driver.presentation.utils.Helper
import java.util.*


class BookingHistoryAdapter(
    private val context: Context,
    itemList: ArrayList<BookingHistory>,
    private val itemClicked: IItemClicked
) : androidx.recyclerview.widget.RecyclerView.Adapter<BookingHistoryAdapter.ViewHolder>() {

    private var itemList = ArrayList<BookingHistory>()

    init {
        this.itemList = itemList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {


        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.item_booking_history, parent, false)

        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: BookingHistoryAdapter.ViewHolder, position: Int) {

        val booking = itemList[position]

        holder.locationTitleTV.text = "Booking # "+booking.id
        holder.locationAddressTV.text = booking.deliver_date + " at " + Helper.formatTime24HrToAmPm(booking.deliver_time_string) + " to "+booking.return_date+" at "+Helper.formatTime24HrToAmPm(booking.return_time_string)
        holder.distanceTV.text = booking.currency +". "+ Helper.RoundByString(booking.amount,2,2)

        holder.itemView.setOnClickListener{

            //itemClicked.itemClicked(position)


        }

    }


    override fun getItemCount(): Int {
        return itemList.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    inner class ViewHolder(itemView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(itemView) {

        var locationTitleTV: TextView
        var locationAddressTV: TextView
        var distanceTV: TextView

        init {
            locationTitleTV = itemView.findViewById(R.id.locationTitleTV)
            locationAddressTV = itemView.findViewById(R.id.locationAddressTV)
            distanceTV = itemView.findViewById(R.id.distanceTV)
        }
    }
}
