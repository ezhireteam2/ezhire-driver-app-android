package com.ezhire.driver.presentation.base

import android.os.Bundle
import androidx.fragment.app.DialogFragment
import com.ezhire.driver.presentation.progress.ProgressIndicator

open class BaseDialogFragment : DialogFragment(), BaseView, ProgressIndicator {
    override fun navigateToFragment(id: Int, args: Bundle?) {
        TODO("Not yet implemented")
    }

    override fun isInternetAvailable(): Boolean {
        TODO("Not yet implemented")
    }

    override fun hideKeyboard() {
        TODO("Not yet implemented")
    }

    override fun callDialog(type: String) {
        TODO("Not yet implemented")
    }

    override fun openDrawer() {
        TODO("Not yet implemented")
    }

    override fun showProgressIndicator() {
        TODO("Not yet implemented")
    }

    override fun hideProgressIndicator() {
        TODO("Not yet implemented")
    }

}