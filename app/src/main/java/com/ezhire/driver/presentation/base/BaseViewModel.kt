package com.ezhire.driver.presentation.base

import androidx.lifecycle.ViewModel
import com.ezhire.driver.presentation.utils.Constant
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.net.InetAddress

open class BaseViewModel : ViewModel() {


    fun isInternetAvailable() : Boolean {
        return try {
            val ipAdar: InetAddress = InetAddress.getByName("google.com")
            !ipAdar.equals("")
        } catch (e: Exception) {
            println("Exceptions::$e")
            true
        }
    }



    fun getException(exception: Exception): String {
        return when (exception){
            is IOException -> Constant.NETWORK_ERROR
            is HttpException -> getErrorMessage(exception.response()!!)
            else -> exception.localizedMessage!!
        }
    }


    private fun getErrorMessage(json: Response<*>) : String {
        return when (json.code()) {
            404, 500 -> "Server is temporary unavailable"
            else -> {
                val obj = JSONObject(json.errorBody()?.string())
                val error: String
                if (obj.has("message") && !obj.isNull("message")){
                    error = obj.get("message").toString()
                }else {
                    error = when {

                        obj.has("status") -> {
                            obj.getString("status")
                        }
                        obj.has("detail") -> {
                            obj.getString("detail")
                        }
                        else -> {
                            "Something went wrong"
                        }
                    }

                }
                error
            }
        }
    }
}