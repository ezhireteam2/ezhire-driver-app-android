package com.ezhire.driver.presentation.progress

interface ProgressIndicator {
    fun showProgressIndicator()
    fun hideProgressIndicator()
}