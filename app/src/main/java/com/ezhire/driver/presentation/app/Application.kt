package com.ezhire.driver.presentation.app

import android.app.Application
import com.ezhire.driver.presentation.utils.AppData
import im.crisp.sdk.Crisp
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin


open class Application : Application() {

    override fun onCreate() {
        super.onCreate()

        // SharedPrefManager
        AppData.with(this)

        //EzhireDatabase.getInstance(applicationContext)

        //start Koin
        startKoin {
            androidContext(this@Application)
            modules(listOf(dataModule, viewModelsModule, fragmentModules))
        }

        Crisp.initialize(this)

        if (AppData.isLogin()) {
            Crisp.getInstance().websiteId = "25a6fdcf-c0e1-408a-9284-82b93c0a3a9d"
            Crisp.getInstance().locale = "en"
            Crisp.User.setNickname(AppData.getName())
            Crisp.User.setEmail(AppData.getEmail())
            Crisp.User.setPhone(AppData.getPhone())

            Crisp.Session.setData("Platform", "Android")
        }
    }


}