package com.ezhire.driver.data;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public class polyLineData implements Serializable {

    PolylineOptions polylineOptions;

    public polyLineData(){;}

    public polyLineData(PolylineOptions polylineOptions) {
        this.polylineOptions = polylineOptions;
    }


    public static void writeData(Context c, polyLineData pd)
    {
        Gson gson=new Gson();
        SharedPreferences.Editor   spEditor=c.getSharedPreferences("RecordedPoints",Context.MODE_PRIVATE).edit();
        spEditor.clear();
        String uniqueID = UUID.randomUUID().toString();
        spEditor.putString(uniqueID,gson.toJson(pd)).apply();
    }

    public static void clearData(Context c){
        SharedPreferences.Editor   spEditor=c.getSharedPreferences("RecordedPoints",Context.MODE_PRIVATE).edit();
        spEditor.clear().apply();
    }


    public static ArrayList<PolylineOptions> getData(Context c)
    {
        Gson gson=new Gson();
        ArrayList<PolylineOptions> data=new ArrayList<>();
        SharedPreferences   sp=c.getSharedPreferences("RecordedPoints",Context.MODE_PRIVATE);
        Map<String,?> mp=sp.getAll();

        for(Map.Entry<String,?> entry : mp.entrySet()){
            String json=entry.getValue().toString();
            polyLineData pd=gson.fromJson(json,polyLineData.class);
            data.add(pd.polylineOptions);
        }

        return data;

    }

}
