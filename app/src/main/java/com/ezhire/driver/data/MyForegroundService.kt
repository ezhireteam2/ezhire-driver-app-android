package com.ezhire.driver.data

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.ServiceInfo.FOREGROUND_SERVICE_TYPE_LOCATION
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.birjuvachhani.locus.Locus
import com.directions.route.Route
import com.directions.route.RouteException
import com.directions.route.RoutingListener
import com.ezhire.driver.R
import com.ezhire.driver.data.network.HttpEndPoints
import com.ezhire.driver.data.service.firebase.Config
import com.ezhire.driver.domain.DriverLocation
import com.ezhire.driver.presentation.main.MainActivity
import com.ezhire.driver.presentation.main.ui.booking.BookingViewModel
import com.ezhire.driver.presentation.utils.AppData
import com.google.android.gms.location.LocationRequest
import io.socket.client.IO
import io.socket.client.Manager
import io.socket.client.Socket
import io.socket.emitter.Emitter
import io.socket.engineio.client.Transport
import org.json.JSONObject
import org.koin.android.ext.android.inject
import java.util.*


public class MyForegroundService : LifecycleService(), RoutingListener {


    private lateinit var mSocket: Socket

    var location: Location? = null

    private val viewModel by inject<BookingViewModel>()

    var mLocalBroadcastManager: LocalBroadcastManager? = null

    var lastDistance = 0

    override fun onCreate() {
        super.onCreate()

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this)

        try {
            val opts: IO.Options = IO.Options()
            //opts.port = 80
            mSocket = IO.socket(HttpEndPoints.WEBSOCKET_SERVER_URL)
            //mSocket.connect()

            mSocket.io().on(Manager.EVENT_TRANSPORT) { args ->
                val transport: Transport = args[0] as Transport
                transport.on(Transport.EVENT_ERROR, Emitter.Listener { args ->
                    val e = args[0] as Exception
                    Log.e("Socket", "Transport error $e")
                    e.printStackTrace()
                    e.cause!!.printStackTrace()
                })
            }

        } catch (e: Exception) {
            Log.e("Socket", " " + e.toString())
        }

    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        when (intent?.action) {
            "START" -> {
                val input = intent?.getStringExtra("DATA")

                val notificationIntent = Intent(this, MainActivity::class.java)
                val pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent,
                    PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT)

                val channelId =
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        createNotificationChannel("my_service", "My Background Service")
                    } else {
                        ""
                    }

                val notification = NotificationCompat.Builder(this, channelId)
                    .setContentTitle("Location update")
                    .setContentText(input)
                    .setSmallIcon(R.drawable.ic_ezhire_logo)
                    .setContentIntent(pendingIntent)
                    .build()

                //
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                    startForeground(1, notification, FOREGROUND_SERVICE_TYPE_LOCATION)
                } else {
                    startForeground(1, notification)
                }

                Log.e("Service", "Socket -> ${mSocket.isActive} : ${mSocket.connected()}")
                if (!mSocket.connected()) {
                    Log.e("Connect", "Connect")
                    mSocket.connect()
                    mSocket.on(Socket.EVENT_CONNECT, onConnect)
                    mSocket.on(Socket.EVENT_CONNECT_ERROR) {
                        Log.e("Socket", "Connect Error")
                    }
                }
            }

            "STOP" -> {
                mSocket.disconnect()
                stopForeground(true)
                stopSelfResult(startId)
            }

            "ON-THE-WAY" -> {
                Log.e("Send Location", "OntheWay")
                Locus.getCurrentLocation(this) {
                    it.location?.let { it1 -> sendLocationToSocket(it1) }
                }
            }
        }



        Locus.configure {


            request {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                interval = 100L
                fastestInterval = 100L
                maxWaitTime = 100L
                smallestDisplacement = 10f
            }

            this.enableBackgroundUpdates = true
            this.forceBackgroundUpdates = true
        }

        Locus.setLogging(true)

        Locus.startLocationUpdates(this) {

            //Log.e("LocationUpdateError", "${it.error?.message}")

            it.location?.let { it -> /* Received location update */
                Log.e("LocationUpdate", "" + it.latitude + "," + it.longitude)

                location = it

                val startPoint = Location("locationA")
                startPoint.latitude = it.latitude
                startPoint.longitude = it.longitude



                if (AppData.booking.DeliveryLat != 0.0) {
                    val endPoint = Location("locationA")

                    endPoint.latitude = AppData.booking.DeliveryLat
                    endPoint.longitude = AppData.booking.DeliveryLong

                    if (AppData.booking.IS_COLLECTION) {
                        endPoint.latitude = AppData.booking.ReturnLat
                        endPoint.longitude = AppData.booking.ReturnLong
                    }

                    val distance = endPoint.distanceTo(startPoint).toDouble()
                    Log.e("Distance", "" + distance)
                    if (distance <= 500 && AppData.getNotifyCustomerStatus()) {
                        networkCall()
                    }
                }

                // app is in foreground, broadcast the push message
                val pushNotification = Intent(Config.LOCATION_UPDATE)
                pushNotification.putExtra("location", it)
                mLocalBroadcastManager?.sendBroadcast(pushNotification)

                sendLocationToSocket(it)


            }
        }

        return super.onStartCommand(intent, flags, startId)
    }

    var onConnect = Emitter.Listener {
        Log.e("Socket", "ID: ${mSocket.id()}")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("Service", "OnDestroy")
    }

    override fun onBind(intent: Intent): IBinder? {

        Log.e("Service", "OnBind")
        return super.onBind(intent)
    }

    override fun onUnbind(intent: Intent?): Boolean {
        Log.e("Service", "OnUnbind")
        return super.onUnbind(intent)

    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        Log.e("Service", "OnTaskRemoved")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_NONE
        )
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }


    public fun sendLocationToSocket(location: Location) {

        updateDriverLastLocation(location)
        if (mSocket.connected()) {

            var roomID: String? = AppData.booking.BookingID.toString()
            //roomID = "1122"
            if (roomID?.isNotEmpty() == true) {
                //Log.e("Room ID",""+roomID)

                val messageJSONO = JSONObject()
                messageJSONO.put("room", "" + roomID)
                messageJSONO.put("location", "${location.latitude},${location.longitude}")
                messageJSONO.put("bearing", "${location.bearing}")

                mSocket.let {
                    mSocket.emit("driver_location", messageJSONO)
                    //Log.e("Location Sent", messageJSONO.toString())
                }

                mSocket.let {
                    mSocket.emit("driver_location_new", messageJSONO)
                    //Log.e("Location Sent", messageJSONO.toString())
                }

            } else {
                Log.e("Room ID", "No Room")
            }
            //Log.e("Socket","Connected")
        } else {
            Log.e("Socket", "Not Connected")
        }
    }

    override fun onRoutingFailure(p0: RouteException?) {
        Log.e("onRoutingFailure", "onRoutingFailure")
    }

    override fun onRoutingStart() {
        Log.e("onRoutingStart", "onRoutingStart")
    }

    override fun onRoutingSuccess(p0: ArrayList<Route>, p1: Int) {
        Log.e("onRoutingSuccess", "onRoutingSuccess")

//        val pushNotification = Intent(Config.LOCATION_UPDATE)
//        pushNotification.putExtra("route", p0)
//        pushNotification.putExtra("location", location)
//
//        Log.e("Distance R",""+ p0[0].distanceValue)
//
//        mLocalBroadcastManager?.sendBroadcast(pushNotification)

        //networkCall()
    }

    override fun onRoutingCancelled() {
        Log.e("onRoutingCancelled", "onRoutingCancelled")
    }

    private fun updateDriverLastLocation(location: Location) {


        val currentTime = Calendar.getInstance().timeInMillis


        val lastSaveTime = AppData.getTime() + 30000

        if (lastSaveTime == 0L) {
            AppData.setTime(Calendar.getInstance().timeInMillis)
        }

        if (currentTime > lastSaveTime) {

            AppData.setTime(Calendar.getInstance().timeInMillis)

            val param = DriverLocation()
            param.UserID = AppData.getUserID()
            param.Latitude = location.latitude
            param.Longitude = location.longitude
            param.Latlng = "${location.latitude},${location.longitude}"

            viewModel.updateDriverLocation(param).observe(this, {
                when (it) {
                    is Response.Success<*> -> {
                        //AppData.setNotifyCustomer(false)
                        Log.e("Api Success", "updateDriverLocation")
                    }

                    is Response.Error -> {
                        Log.e("Api Failed", "updateDriverLocation: ${it.exception}")
                    }

                }
            })
        } else {
            //Donot send
            //Log.e("No Api","No Api Call")
        }


    }

    private fun networkCall() {

        AppData.setNotifyCustomer(false)
        //val repository = Repository(ApiClient.provideApiService)

        //val response = repository.notifyCustomer(AppData.getUserID())
        val request = DriverLocation()
        request.UserID = AppData.booking.UserID
        viewModel.notifyCustomer(request).observe(this, {
            when (it) {
                is Response.Success<*> -> {

                    Log.e("Api Success", "Notify Customer")
                }

                is Response.Error -> {
                    Log.e("Api Failed", "Error: ${it.exception}")
                }

            }
        })

    }

}


