package com.ezhire.driver.data.network

import android.provider.Settings
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.ezhire.driver.BuildConfig
import com.ezhire.driver.presentation.utils.Helper
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiClient {

    private val logger: HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(BuildConfig.INTERCEPTOR)


        private val gson : Gson by lazy {
            GsonBuilder()
                .setLenient()
                .create()
        }


        private val provideOkHttpClient : OkHttpClient by lazy {

            val httpClient = OkHttpClient().newBuilder()
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(45, TimeUnit.SECONDS)


            httpClient.addInterceptor(logger)

            // log using OkHttp
            httpClient.build()
        }

    internal val okHttpClient = OkHttpClient().newBuilder().addInterceptor { chain ->

        val originalRequest = chain.request()
        val headerData = Helper.getAuthorizationHeader().replace("\n", "")
        val builder = originalRequest.newBuilder().addHeader("Authorization",headerData)


        val newRequest = builder.build()

        chain.proceed(newRequest)
    }.connectTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(30, TimeUnit.SECONDS)
        .readTimeout(30, TimeUnit.SECONDS).addInterceptor(logger).build()


        private val provideRetrofit : Retrofit by lazy {
            Retrofit.Builder()
                .baseUrl(HttpEndPoints.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .build()
        }


        val provideApiService : ApiServices by lazy {
            provideRetrofit.create(
                ApiServices::class.java
            )
        }
}