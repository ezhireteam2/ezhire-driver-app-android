package com.ezhire.driver.data.network


import com.ezhire.driver.domain.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


interface ApiServices {


//    @POST(HttpEndPoints.LOGIN_API)
//    suspend fun loginUser(@Body userCredentials: User): Users

    @POST(HttpEndPoints.FCM_REGISTRATION)
    suspend fun registerFCM(@Body request: FcmUser): FcmUser

    @GET(HttpEndPoints.FIREBASE_TOKEN_AUTH)
    suspend fun fireBaseAuth(@Query("token") token: String): List<User>

    @GET(HttpEndPoints.BOOKING)
    suspend fun getBooking(@Query("user_id") token: Long): Booking

    @POST(HttpEndPoints.CHANGE_BOOKING_STATUS)
    suspend fun updateBookingStatus(@Body param: BookingStatusCode): BookingResponse

    @POST(HttpEndPoints.DRIVER_STATUS)
    suspend fun driverStatus(@Body param: DriverActiveStatus): CallResponse

    @GET(HttpEndPoints.GET_DRIVER_STATUS)
    suspend fun getDriverStatus(@Query("driver_id") driverID: Long): DriverActiveStatus

    @POST(HttpEndPoints.CAR_KILOMETER)
    suspend fun saveCarKilometer(@Body param: ShiftInfo): ShiftInfo

    @GET(HttpEndPoints.GET_MARKETPLACE_BOOKING)
    suspend fun getMarketPlaceBooking(@Query("booking_id") id: Long): Booking

    @POST(HttpEndPoints.ACCEPT_BOOKING)
    suspend fun acceptBooking(@Body param: BookingAccept): BookingAccept

    @POST(HttpEndPoints.REJECT_BOOKING)
    suspend fun rejectBooking(@Body param: BookingAccept): CallResponse

    @POST(HttpEndPoints.UPDATE_DRIVER_LOCATION)
    suspend fun updateDriverLocation(@Body param: DriverLocation): CallResponse

    @GET(HttpEndPoints.CAR_KILOMETER)
    suspend fun getLastShiftInfo(
        @Query("driver_id") driverID: Long,
        @Query("booking_id") bookingID: Long
    ): ShiftInfo

    @GET(HttpEndPoints.GET_SHIFT_DETAILS)
    suspend fun getAllShiftInfo(
        @Query("driver_id") driverID: Long,
        @Query("booking_id") bookingID: Long
    ): List<ShiftInfo>

    @POST(HttpEndPoints.UPDATE_BOOKING)
    suspend fun updateBooking(@Body request: UpdateBooking): CallResponse

    @POST(HttpEndPoints.ADD_CASH)
    suspend fun addCash(@Body request: SaveCash): CallResponse

    @GET(HttpEndPoints.GET_BOOKING_AMOUNT)
    suspend fun getAmountToCollect(@Query("booking_id") id: Long): CallResponse

    @POST(HttpEndPoints.DRIVER_IS_NEARBY)
    suspend fun notifyCustomer(@Body request: DriverLocation): CallResponse

    @POST(HttpEndPoints.CUSTOMER_FEEDBACK)
    suspend fun customerFeedback(@Body param: DeliveryFeedBack): CallResponse

    @GET(HttpEndPoints.GET_BOOKING_HISTORY)
    suspend fun bookingHistory(@Query("user_id") id: Long): ArrayList<BookingHistory>

    @GET(HttpEndPoints.GET_TIPS)
    suspend fun getTips(@Query("driver_id") id: Long): ArrayList<TipData>

    @GET(HttpEndPoints.BOOKING_HISTORY_DETAIL)
    suspend fun bookingHistoryDetail(@Query("booking_id") id: Long): ArrayList<AddOns>

    @GET(HttpEndPoints.BOOKING_HISTORY_DETAIL)
    suspend fun shiftDetails(
        @Query("driverID") DriverID: Long,
        @Query("booking_id") id: Long
    ): ArrayList<ShiftInfo>

    @GET(HttpEndPoints.GET_VERSION)
    suspend fun getAppVersion(@Query("version") ver: String): ArrayList<AppVersion>

    @GET(HttpEndPoints.GET_BOOKINGS)
    suspend fun getBookings(@Query("user_id") token: Long): BookingResponse

    @GET(HttpEndPoints.GET_BOOKING_BY_ID)
    suspend fun getBookingByBookingID(@Query("booking_id") token: Long): Booking

    @POST(HttpEndPoints.UPDATE_NCM)
    suspend fun updateNCM(@Body request: BookingDriverStatus): BookingResponse

    @GET(HttpEndPoints.UPDATE_NCM)
    suspend fun getNCMStatus(@Query("user_id") token: Long): NCMStatusResponse

    @GET(HttpEndPoints.USER_DOCUMENTS)
    suspend fun getDocuments(@Query("user_id") token: Long): ArrayList<Document>
}