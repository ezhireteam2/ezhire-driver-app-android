//package com.ezhire.driver.data.room
//
//import androidx.lifecycle.LiveData
//import androidx.room.*
//import androidx.room.Dao
//import com.ezhire.driver.data.room.entity.Users
//
////where room operations get defined!
//@Dao
//interface Dao {
//
//
//    @Insert(onConflict = OnConflictStrategy.REPLACE)
//    suspend fun insertUser(
//        user: Users
//    )
//
//    @Delete
//    suspend fun dltUser(
//        user: Users
//    )
//
//
//
//    @Query("select * from Users")
//    fun fetchUser(): LiveData<List<Users>>
//
//
//    @Query("select * from Users where status_name = :name")
//    fun fetchSingleUser(name  : String): LiveData<List<Users>>
//
//
//
//
//}