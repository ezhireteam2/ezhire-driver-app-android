package com.ezhire.driver.data

import com.ezhire.driver.data.network.ApiServices
import com.ezhire.driver.domain.*

@Suppress("UNCHECKED_CAST")
class Repository(private val client: ApiServices) {

    //suspend fun loginAPI(user : User) = client.loginUser(user)

    suspend fun registerFCMToken(request: FcmUser) = client.registerFCM(request)

    suspend fun userExistAPI(token: String) = client.fireBaseAuth(token)

    suspend fun getBookings(userID: Long) = client.getBooking(userID)

    suspend fun updateBookingStatus(request: BookingStatusCode) =
        client.updateBookingStatus(request)

    suspend fun driverStatus(request: DriverActiveStatus) = client.driverStatus(request)

    suspend fun getDriverStatus(driverID: Long) = client.getDriverStatus(driverID)

    suspend fun saveCarKM(request: ShiftInfo) = client.saveCarKilometer(request)

    suspend fun getMarketPlaceBooking(bookingID: Long) = client.getMarketPlaceBooking(bookingID)

    suspend fun acceptBooking(request: BookingAccept) = client.acceptBooking(request)

    suspend fun rejectBooking(request: BookingAccept) = client.rejectBooking(request)

    suspend fun updateDriverLocation(request: DriverLocation) = client.updateDriverLocation(request)

    suspend fun getLastShiftInfo(driverID: Long, booking_id: Long) =
        client.getLastShiftInfo(driverID, booking_id)

    suspend fun getAllShiftInfo(driverID: Long, booking_id: Long) =
        client.getAllShiftInfo(driverID, booking_id)

    suspend fun updateBooking(request: UpdateBooking) = client.updateBooking(request)

    suspend fun addCash(request: SaveCash) = client.addCash(request)

    suspend fun getAmountToCollect(bookingID: Long) = client.getAmountToCollect(bookingID)

    //suspend fun notifyCustomer(userID : Long) = client.notifyCustomer(userID)

    suspend fun notifyCustomer(userID: DriverLocation) = client.notifyCustomer(userID)

    suspend fun customerFeedback(request: DeliveryFeedBack) = client.customerFeedback(request)

    suspend fun bookingHistory(userID: Long) = client.bookingHistory(userID)

    suspend fun bookingHistoryDetail(BookingID: Long) = client.bookingHistoryDetail(BookingID)

    suspend fun getTips(userID: Long) = client.getTips(userID)

    suspend fun shiftDetail(driverID: Long, BookingID: Long) =
        client.bookingHistoryDetail(BookingID)

    suspend fun getAppVersion(version: String) = client.getAppVersion(version)

    suspend fun getBookingList(userID: Long) = client.getBookings(userID)

    suspend fun getBookingByBookingID(bookingID: Long) = client.getBookingByBookingID(bookingID)

    suspend fun updateNCM(request: BookingDriverStatus) = client.updateNCM(request)

    suspend fun getNCMStatus(bookingID: Long) = client.getNCMStatus(bookingID)


    suspend fun getDocument(userID: Long) = client.getDocuments(userID)
}


