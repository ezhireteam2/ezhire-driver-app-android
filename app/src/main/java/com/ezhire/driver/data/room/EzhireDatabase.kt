//package com.ezhire.driver.data.room
//
//import android.content.Context
//import androidx.room.Database
//import androidx.room.Room
//import androidx.room.RoomDatabase
//import androidx.room.migration.Migration
//import androidx.sqlite.db.SupportSQLiteDatabase
//import com.ezhire.driver.data.room.entity.Users
//import com.ezhire.driver.presentation.utils.Constant
//
//
//@Database(version = 1, exportSchema = true, entities = [Users::class])
//
//abstract class EzhireDatabase : RoomDatabase() {
//
//    abstract fun appDao() : Dao
//
//    companion object {
//        private var INSTANCE: EzhireDatabase? = null
//
//        /**
//         * Create SingleTon class with activityContext
//         */
//        fun getInstance(activityContext: Context): EzhireDatabase = INSTANCE ?: synchronized(this) {
//            INSTANCE ?: buildDatabaseInstance(activityContext.applicationContext).also { INSTANCE = it }
//        }
//
//        /**
//         * Create Database here
//         */
//        private fun buildDatabaseInstance(context: Context): EzhireDatabase {
//            return Room.databaseBuilder(context, EzhireDatabase::class.java, Constant.DATABASE)
//                //  .allowMainThreadQueries()
//                .fallbackToDestructiveMigration()
//               // .addMigrations(MIGRATION_1_2)
//                .build()
//
//        }
//
//
//        val MIGRATION_1_2: Migration = object : Migration(1, 2) {
//            override fun migrate(database: SupportSQLiteDatabase) {
//
//            }
//        }
//
//
//    }
//}