package com.ezhire.driver.data.network

import com.ezhire.driver.BuildConfig

object HttpEndPoints {

    var BASE_URL = BuildConfig.BASE_URL

    const val LOGIN_API = "/rental/login/"

    const val FCM_REGISTRATION = "/rental/GCM/"

    const val FIREBASE_TOKEN_AUTH = "/rental/mobile-auth-driver/"

    const val SOCIAL_AUTH = "/rental/social_auth/"

    const val BOOKING = "/rental/get-driver-booking/"

    const val GET_BOOKINGS = "/rental/get-driver-bookings/"

    const val GET_BOOKING_BY_ID = "/rental/get-driver-bookingV2/"

    const val CHANGE_BOOKING_STATUS = "/rental/change-booking-statusV3/"

    const val DRIVER_STATUS = "/rental/update-driver-status/"

    const val GET_DRIVER_STATUS = "/rental/get-driver-status/"

    const val UPDATE_DRIVER_LOCATION = "/rental/driver-location/"

    const val CAR_KILOMETER = "/rental/booking-kilometers/"

    const val GET_MARKETPLACE_BOOKING = "/rental/get-market-place-booking/"

    const val ACCEPT_BOOKING = "/rental/accept-booking/"

    const val REJECT_BOOKING = "/rental/booking-reject/"

    const val GET_SHIFT_DETAILS = "/rental/get-shift-kilometers/"

    const val UPDATE_BOOKING = "/rental/booking-update/"

    const val ADD_CASH = "/rental/save-collected-cash/"

    const val GET_BOOKING_AMOUNT = "/rental/amount-to-collect/"

    const val DRIVER_IS_NEARBY = "/rental/notify-customer/"

    const val CUSTOMER_FEEDBACK = "/rental/rate_customer/"

    const val GET_BOOKING_HISTORY = "/rental/BookingHistoryDriver/"

    const val BOOKING_HISTORY_DETAIL = "/rental/BookingDetails/"

    const val GET_TIPS = "/rental/get-driver-tip/"

    const val GET_VERSION = "/rental/AppVersion/"

    const val UPDATE_NCM = "/rental/driver-ncm/"

    const val USER_DOCUMENTS = "/rental/get_user_documents/"

    const val WEBSOCKET_SERVER_URL = "https://goezhire.com/"

}