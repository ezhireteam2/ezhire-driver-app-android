package com.ezhire.driver.data.service.firebase;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.ezhire.driver.R;
import com.ezhire.driver.presentation.main.MainActivity;
import com.ezhire.driver.presentation.utils.AppData;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;

import java.util.Map;
import java.util.Objects;

/**
 * Created by yasir on 10/03/2017.
 */

public class FCMService extends FirebaseMessagingService {

    private static final String TAG = FCMService.class.getSimpleName();
    private NotificationUtils notificationUtils;
    LocalBroadcastManager mLocalBroadcastManager;

    private SharedPreferences prefs;

    private final String NOTIFICATION_TYPE = "NotificationType";
    private final String NEWBOOKING_NOTIFICATION = "NEWBOOKING";


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        String refreshedToken = "";
        try {
            refreshedToken = token;
            if (!refreshedToken.equals("")) {
                storeRegIdInPref(refreshedToken);
            }
        } catch (Exception ex) {
            Log.e("FCM Ex", ex.toString());
        }

        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
    }

    private void storeRegIdInPref(String token) {
        AppData.INSTANCE.setFCMToken(token);
        Log.d(TAG, "Refreshed added: " + token);

    }

    @Override
    public void onCreate() {
        super.onCreate();

        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        //prefs = Global.getSharedPrefs(this);
    }

    public FCMService() {
        super();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "Notification: " + remoteMessage.getNotification());
        Log.e(TAG, "From: " + remoteMessage.toString());
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        Log.e(TAG, "getMessageType: " + remoteMessage.getMessageType());
        Log.e(TAG, "Data: " + remoteMessage.getData());
        Log.e(TAG, "Data size: " + remoteMessage.getData().size());


        if (AppData.INSTANCE.isLogin() && remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());
            try {

                handleDataMessage(remoteMessage.getData(),remoteMessage.getNotification());
            } catch (Exception e) {
                //Crashlytics.logException(e);
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }

    }


    private void handleDataMessage(Map<String, String> json, RemoteMessage.Notification notification) {

        try {
            //String date = new SimpleDateFormat("dd-MM-yyy", Locale.US).format(new Date());
            //String time = new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date());

            String title = json.get("title");
            String message = json.get("message");
            String msg_type = json.get("msg_type");
            Long booking_id = 0L;
            if(json.get("booking_id") != null){
                booking_id = Long.valueOf(Objects.requireNonNull(json.get("booking_id")));
            }
            //String body = notification.getBody().

            Log.e("JSON",""+json.toString());

            //New Booking Invitation



            if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                // app is in foreground, broadcast the push message
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);



                pushNotification.putExtra("message", message);
                pushNotification.putExtra("msg_type", msg_type);
                pushNotification.putExtra("booking_id",booking_id);
                pushNotification.putExtra(NOTIFICATION_TYPE, NEWBOOKING_NOTIFICATION);
                if(msg_type != null && msg_type.contentEquals("TIP")){
                    pushNotification.putExtra("TIP",true);
                }

                mLocalBroadcastManager.sendBroadcast(pushNotification);


                Intent resultIntent;
                resultIntent = new Intent(getApplicationContext(), MainActivity.class);

                if(msg_type != null && msg_type.contentEquals("TIP")){
                    resultIntent.putExtra("TIP",true);
                }


                resultIntent.putExtra("message", message);
                resultIntent.putExtra("TranId", 0);
                resultIntent.putExtra("booking_id",booking_id);
                resultIntent.putExtra("msg_type",msg_type);
                resultIntent.putExtra(NOTIFICATION_TYPE, NEWBOOKING_NOTIFICATION);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resultIntent.setAction(Intent.ACTION_MAIN);
                resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

                showNotificationMessage(getApplicationContext(), title, message, "", resultIntent);
            } else {
                // app is in background, show the notification in notification tray
                Intent resultIntent;
                resultIntent = new Intent(getApplicationContext(), MainActivity.class);
                Log.e("App is Background", "Background");
                resultIntent.putExtra("message", message);
                resultIntent.putExtra("message_type",msg_type);
                resultIntent.putExtra("TranId", 0);
                resultIntent.putExtra("booking_id",booking_id);
                resultIntent.putExtra("msg_type",msg_type);
                if(msg_type != null && msg_type.contentEquals("TIP")){
                    resultIntent.putExtra("TIP",true);
                }
                resultIntent.putExtra(NOTIFICATION_TYPE, NEWBOOKING_NOTIFICATION);
                resultIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                resultIntent.setAction(Intent.ACTION_MAIN);
                resultIntent.addCategory(Intent.CATEGORY_LAUNCHER);

                showNotificationMessage(getApplicationContext(), title, message, "", resultIntent);

            }
        } catch (Exception e) {
            //Crashlytics.logException(e);
            Log.e(TAG, "Exception: " + e.toString());
        }
    }

    /**
     * Showing notification with text only
     */
    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
        notificationUtils = new NotificationUtils(context);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent);
    }

    /**
     * Showing notification with text and image
     */
    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
        notificationUtils = new NotificationUtils(context);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        notificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
    }

    private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ic_launcher_background : R.drawable.ic_launcher_background;
    }

    private void handleNotification(String message) {
        if (!NotificationUtils.isAppIsInBackground(getApplicationContext())) {
            // app is in foreground, broadcast the push message
            Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
            pushNotification.putExtra("message", message);
            mLocalBroadcastManager.sendBroadcast(pushNotification);

            // play notification sound
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            //notificationUtils.playNotificationSound();
        } else {
            // If the app is in background, firebase itself handles the notification
        }
    }


}
